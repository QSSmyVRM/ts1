﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_SearchBridgeConference.SearchBridgeConference" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<% if (Session["isExpressUser"].ToString() == "1")
   {%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}
   else
   {%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%>--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MCU Conference List</title>

    <script type="text/javascript" src="inc/functions.js"></script>

    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />

    <script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script>

    <script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>

    <script type="text/javascript" src="script/CallMonitorJquery/MonitorMCU.js"></script>

    <script type="text/javascript" src="script/CallMonitorJquery/json2.js"></script>

    <script type="text/javascript">
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
        document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>

    <script type="text/javascript">

        function DataLoading(val) {
            if (val == "1")
                document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
            else
                document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
        }

        function viewconf(cid) {
            url = "ManageConference.aspx?t=hf&confid=" + cid;
            confdetail = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
            confdetail.focus();
        }

        //ZD 100369
        function PopupWindow() {
            var popwin = window.showModalDialog("BridgeList.aspx", document, "resizable: yes; scrollbar: no; help: yes; status:yes; dialogHeight:580px; dialogWidth:510px");
        }

        function ChangeSortingOrder(arg) {
            DataLoading('1');
            document.getElementById("hdnSortingOrder").value = "1";
        }
        function SelectOneDefault(obj) {
            var i = 1;
            if (navigator.userAgent.indexOf('Trident') > -1)
                i = 0;

            if (obj.tagName == "INPUT" && obj.type == "radio" && obj.checked) {
                var elements = document.getElementsByTagName('input');

                for (i = 0; i < elements.length; i++)
                    if ((elements.item(i).type == "radio")) {
                    if (elements.item(i).id != obj.id) {
                        elements.item(i).checked = false;
                    }
                }
            }
        }

        function CheckSelectedPage() {
            var chkPage = document.getElementById("chkSelectPage");
            var ird;

            var gridCount = document.getElementById('<%=dgConferenceList.ClientID%>').rows.length;

            for (var k = 1; k <= gridCount; k++) {
                if (k <= 9)
                    ird = "dgConferenceList_ctl0" + k + "_chkChangeMCU"; //ZD 100369-Bug Fixing
                else
                    ird = "dgConferenceList_ctl" + k + "_chkChangeMCU";
                    
                if (document.getElementById(ird) == null)
                    continue;
                if (chkPage.checked) {
                    document.getElementById(ird).checked = true;
                }
                else {
                    document.getElementById(ird).checked = false;
                }
            }
        }

        function CheckMCUSelection(obj) {
            var chkId = document.getElementById(obj.id);
            if (chkId != null && chkId.checked == false) {
                document.getElementById("chkSelectPage").checked = false;
            }
        }
	
    </script>

</head>
<body>
    <form id="frmMCUConferenceList" runat="server" method="post">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" id="txtSortBy" runat="server" />
    <input type="hidden" id="hdnAscendingOrder" runat="server" />
    <input type="hidden" id="hdnSortingOrder" runat="server" />
    <input type="hidden" id="hdnShowpopup" runat="server" />
    <div>
        <table width="95%" align="center" border="0">
            <tr>
                <td>
                    <h3>
                        <asp:Label ID="Label3" runat="server" Text="<%$ Resources:WebResources, ConferenceList_lblHeader%>"></asp:Label></h3>
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV"  name="dataLoadingDIV" align="center" style="display:none">
                        <img border='0' src='image/wait1.gif' alt='Loading..' />
                    </div><%--ZD 100678 End--%>
                </td>
            </tr>
            <tr align="right" class="blackblodtext" id="trPages" runat="server">
                <td class="blackblodtext">
                    <asp:LinkButton ID="btnChangeMCU" Text="<%$ Resources:WebResources, ChangeMCU%>" runat="server" OnClientClick="javascript:return fnInvokePopup();"></asp:LinkButton>&nbsp; &nbsp;
                    <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, SelectPage%>" runat="server"></asp:Literal>
                    <asp:CheckBox ID="chkSelectPage" runat="server" onclick="javascript:CheckSelectedPage();" />
                    &nbsp; &nbsp; <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, ALL%>" runat="server"></asp:Literal>
                    <asp:CheckBox ID="chkAllPages" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DataGrid ID="dgConferenceList" AllowSorting="true" runat="server" AutoGenerateColumns="False"
                        Font-Names="Verdana" Font-Size="Small" ShowFooter="true" Width="100%" Style="border-collapse: separate"
                        BorderStyle="None" BorderWidth="0px" GridLines="None" OnItemDataBound="InterpretRole"
                        OnItemCreated="BindRowsDeleteMessage" OnEditCommand="EditConference" OnCancelCommand="DeleteConference"
                        OnDeleteCommand="ManageConference" OnUpdateCommand="CloneConference">
                        <ItemStyle CssClass="tableBody" Height="15" VerticalAlign="Top" />
                        <HeaderStyle CssClass="tableHeader" Height="30" />
                        <SelectedItemStyle CssClass="tableBody" />
                        <Columns>
                            <asp:BoundColumn DataField="ConferenceID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="IsHost" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="IsParticipant" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ConferenceDuration" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ConferenceStatus" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="IsRecur" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="OpenForRegistration" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Conference_Id" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ConferenceDateTime" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="organizationName" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="IsSynchronous" Visible="false"></asp:BoundColumn> <%--ZD 100036--%>
                            <asp:BoundColumn DataField="IsPublicConference" Visible="false"></asp:BoundColumn> <%--ZD 101233 CellNo:24--%>
                            <asp:TemplateColumn ItemStyle-Width="100%" HeaderStyle-Width="100%" FooterStyle-HorizontalAlign="Right">
                                <HeaderTemplate>
                                    <table width="100%">
                                        <tr class="tableHeader">
                                            <td width="5%" runat="server" id="tdID" align="left" class="tableHeader">
                                                <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ConferenceList_tdID%>" runat="server"></asp:Literal><asp:LinkButton ID="btnSortID" runat="server" CommandArgument="1" OnCommand="SortGrid"
                                                    OnClientClick="ChangeSortingOrder('1');">
                                                    <%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                                                      { %>
                                                    <asp:Label ID="spnAscendingIDIE" runat="server" Style="font-family: Wingdings; font-weight: bolder;">&#233;</asp:Label><asp:Label
                                                        ID="spnDescndingIDIE" runat="server" Style="font-family: Wingdings; font-weight: bolder;">&#234;</asp:Label><% }
                                                      else
                                                      { %><asp:Label ID="spnAscendingID" runat="server" Style="font-family: Wingdings;
                                                          font-weight: bolder; font-size: x-large;">&#8593;</asp:Label><asp:Label ID="spnDescendingID"
                                                              runat="server" Style="font-family: Wingdings; font-weight: bolder; font-size: x-large;">&#8595;</asp:Label><% } %></asp:LinkButton>
                                            </td>
                                            <td width="10%" runat="server" id="tdName" align="left" class="tableHeader">
                                                <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ConferenceList_tdName%>" runat="server"></asp:Literal><asp:LinkButton ID="btnSortName" runat="server" CommandArgument="2"
                                                    OnCommand="SortGrid" OnClientClick="ChangeSortingOrder('2');">
                                                    <%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                                                      {%><asp:Label ID="spnAscendingConfNameIE" runat="server" Style="font-family: Wingdings;
                                                          font-weight: bolder;">&#233;</asp:Label><asp:Label ID="spnDescndingConfNameIE" runat="server"
                                                              Style="font-family: Wingdings; font-weight: bolder;">&#234;</asp:Label><% }
                                                      else
                                                      { %><asp:Label ID="spnAscendingConfName" runat="server" Style="font-family: Wingdings;
                                                          font-weight: bolder; font-size: x-large;">&#8593;</asp:Label><asp:Label ID="spnDescendingConfName"
                                                              runat="server" Style="font-family: Wingdings; font-weight: bolder; font-size: x-large;">&#8595;</asp:Label><% } %></asp:LinkButton>
                                            </td>
                                            <td width="10%" runat="server" id="tdOrgName" align="left" class="tableHeader">
                                                <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ConferenceList_tdOrgName%>" runat="server"></asp:Literal><asp:LinkButton ID="btnSortorgName" runat="server" CommandArgument="5" OnCommand="SortGrid"
                                                    OnClientClick="ChangeSortingOrder('3');">
                                                    <%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                                                      { %><asp:Label ID="spnAscendingSiloNameIE" runat="server" Style="font-family: Wingdings;
                                                          font-weight: bolder;">&#233;</asp:Label><asp:Label ID="spnDescndingSiloNameIE" runat="server"
                                                              Style="font-family: Wingdings; font-weight: bolder;">&#234;</asp:Label><% }
                                                      else
                                                      { %><asp:Label ID="spnAscendingSiloName" runat="server" Style="font-family: Wingdings;
                                                          font-weight: bolder; font-size: x-large;">&#8593;</asp:Label><asp:Label ID="spnDescendingSiloName"
                                                              runat="server" Style="font-family: Wingdings; font-weight: bolder; font-size: x-large;">&#8595;</asp:Label><% } %></asp:LinkButton>
                                            </td>
                                            <td width="15%" runat="server" id="tdDateTime" style="text-decoration: underline"
                                                align="left" class="tableHeader">
                                                <asp:Label ID="lblDtTimeHeader" runat="server" Text="<%$ Resources:WebResources, ConferenceList_lblDtTimeHeader%>"></asp:Label>
                                                <asp:LinkButton ID="btnSortDateTime" runat="server" CommandArgument="3" OnCommand="SortGrid"
                                                    OnClientClick="ChangeSortingOrder('4');">
                                                    <%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                                                      { %>
                                                    <asp:Label ID="spnAscendingConfDateIE" runat="server" Style="font-family: Wingdings;
                                                        font-weight: bolder;">&#233;</asp:Label><asp:Label ID="spnDescndingConfDateIE" runat="server"
                                                            Style="font-family: Wingdings; font-weight: bolder;">&#234;</asp:Label>
                                                    <% }
                                                      else
                                                      { %><asp:Label ID="spnAscendingConfDate" runat="server" Style="font-family: Wingdings;
                                                          font-weight: bolder; font-size: x-large;">&#8593;</asp:Label><asp:Label ID="spnDescendingConfDate"
                                                              runat="server" Style="font-family: Wingdings; font-weight: bolder; font-size: x-large;">&#8595;</asp:Label><% } %></asp:LinkButton>
                                            </td>
                                            <td width="1%" class="tableHeader" align="left">
                                                <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ConferenceList_Type%>" runat="server"></asp:Literal>
                                            </td>
                                            <td id="td7head" width="27%" class="tableHeader" align="center" runat="server">
                                                <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, Actions%>" runat="server"></asp:Literal>
                                            </td>
                                            <td id="tdChangeMCU" width="10%" class="tableHeader" align="center" runat="server">
                                                <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, ChangeMCU%>" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table width="100%" class="tableBody">
                                        <tr style='background-color: <%#(DataBinder.Eval(Container, "DataItem.isVMR").ToString().Equals("1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1") && !(DataBinder.Eval(Container, "DataItem.RemPublicVMRCount").ToString().Equals("0")))?"#F0E68C":"Transparent"%>'>
                                            <td width="5%" align="left" valign="top">
                                                <asp:Label ID="lblUniqueID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceUniqueID") %>'></asp:Label>
                                            </td>
                                            <td width="10%" align="left" valign="top">
                                                <asp:Label ID="lblConfName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceName") %>'></asp:Label>
                                            </td>
                                            <td width="10%" align="left" valign="top">
                                                <asp:Label ID="lblOrgName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.organizationName") %>'></asp:Label>
                                            </td>
                                            <td width="15%" align="left" valign="top">
                                                <asp:Label ID="lblDateTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceDateTime")%>'
                                                    Visible='<%# !DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>'></asp:Label>
                                                <asp:LinkButton ID="btnGetInstances" Text="<%$ Resources:WebResources, ConferenceList_btnGetInstances%>" runat="server"
                                                    CommandName="Edit" Visible='<%# DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>'
                                                    OnClientClick="javascript:DataLoading('1');"></asp:LinkButton>
                                            </td>
                                            <td width="13%" align="left" valign="top">
                                                <asp:Label ID="lblConferenceType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceTypeDescription")%>'></asp:Label>
                                            </td>
                                            <td id="Td7" width="27%" runat="server" valign="top">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <asp:LinkButton ID="btnViewDetails" runat="server" Text="<%$ Resources:WebResources, View%>"></asp:LinkButton>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:LinkButton Text="<%$ Resources:WebResources, ConferenceList_btnClone%>" runat="server" ID="btnClone" CommandName="Update" Visible='<%# ( (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) > 0))) %>'
                                                                OnClientClick="javascript:DataLoading('1');"></asp:LinkButton>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:LinkButton Text="<%$ Resources:WebResources, Delete%>" runat="server" ID="btnDelete" CommandName="Cancel"
                                                                Visible='<%# ( DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 1)) %>'
                                                                OnClientClick="javascript:DataLoading('1');"></asp:LinkButton>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:LinkButton Text="<%$ Resources:WebResources, Edit%>" runat="server" ID="btnEdit" CommandName="Edit" Visible='<%# (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 1)) %>'
                                                                OnClientClick="javascript:DataLoading('1');"></asp:LinkButton>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:LinkButton Text="<%$ Resources:WebResources, Manage%>" runat="server" ID="btnManage" CommandArgument="1" CommandName="Delete"
                                                                Visible='<%# (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || Int32.Parse(Session["admin"].ToString()) >= 1) %>'
                                                                OnClientClick="javascript:DataLoading('1');"></asp:LinkButton>
                                                        </td>
                                                        <td width="20%">
                                                            <asp:CheckBox ID="chkChangeMCU" runat="server" onclick="javascript:CheckMCUSelection(this);" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:Label ID="Label1" Text="<%$ Resources:WebResources, TotalRecords%>" runat="server" Font-Bold="true" CssClass="subtitleblueblodtext"></asp:Label>
                                    <asp:Label runat="server" ID="lblTotalRecords" Text='<%# DataBinder.Eval(Container, "DataItem.TotalRecords")%>'></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr align="center">
                <td align="center">
                    <asp:Label ID="lblNoConferences" runat="server" Text="<%$ Resources:WebResources, ConferenceList_lblNoConferences%>" Visible="False"
                        CssClass="lblError"></asp:Label>&nbsp;
                </td>
            </tr>
            <tr align="right">
                <td>
                    <button id="btnCancel" runat="server" class="altMedium0BlueButtonFormat" onserverclick="btnCancel_Click">
                        <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, ConferenceList_btnGoBack%>" runat="server"></asp:Literal></button>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="tc1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue"
                                CssClass="subtitleblueblodtext" runat="server"><asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, ManageUser_Pages%>" runat="server"></asp:Literal> </asp:TableCell>
                            <asp:TableCell ID="tc2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:Button ID="CustomTrigger" runat="server" Text="<%$ Resources:WebResources, Open%>" Style="display: none" />
                    <ajax:ModalPopupExtender ID="CustomPopUp" runat="server" TargetControlID="CustomTrigger"
                        PopupControlID="PopupCustomPanel" DropShadow="false" Drag="true" BackgroundCssClass="modalBackground"
                        CancelControlID="ClosePUp" BehaviorID="CustomTrigger">
                    </ajax:ModalPopupExtender>
                    <asp:Panel ID="PopupCustomPanel" Width="75%" runat="server" HorizontalAlign="Center"
                        CssClass="treeSelectedNode" Height="600px" ScrollBars="Auto">
                        <div>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        <table width="95%" border="0">
                                            <tr>
                                                <td align="center">
                                                    <h3>
                                                        <asp:Label ID="lblHeader" Text="<%$ Resources:WebResources, SearchBridgeConference_MCUList%>" runat="server"></asp:Label>
                                                    </h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="Label2" runat="server" Text="" CssClass="lblError"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:DataGrid ID="dgMCUs" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                        GridLines="None" BorderColor="blue" Style="border-collapse: separate" BorderStyle="solid"
                                                        BorderWidth="1" ShowFooter="true" Width="100%" Visible="true">
                                                        <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                                        <EditItemStyle CssClass="tableBody" />
                                                        <AlternatingItemStyle CssClass="tableBody" />
                                                        <ItemStyle CssClass="tableBody" />
                                                        <FooterStyle CssClass="tableBody" />
                                                        <HeaderStyle CssClass="tableHeader" />
                                                        <Columns>
                                                            <asp:BoundColumn DataField="ID" Visible="false">
                                                                <HeaderStyle CssClass="tableHeader" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="name" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, SearchBridgeConference_Name%>" ItemStyle-CssClass="tableBody">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="interfaceType" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, InterfaceType%>" ItemStyle-CssClass="tableBody">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="administrator" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Administrator%>" ItemStyle-CssClass="tableBody">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="exist" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, ExistingVirtual%>" ItemStyle-CssClass="tableBody">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="status" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Status%>" ItemStyle-CssClass="tableBody">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="PollStatus" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, PollStatus%>" ItemStyle-Font-Bold="true"
                                                                ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="order" Visible="false"></asp:BoundColumn>
                                                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Select%>" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="15%">
                                                                <HeaderStyle CssClass="tableHeader" />
                                                                <ItemTemplate>
                                                                    <asp:RadioButton ID="rdSelectMCU" onclick="javascript:SelectOneDefault(this);" GroupName="Default"
                                                                        runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                    <asp:Table runat="server" ID="tblNoMCUs" Visible="false" Width="90%">
                                                        <asp:TableRow CssClass="lblError">
                                                            <asp:TableCell CssClass="lblError">
                                                                <asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, NoMCUsfound%>" runat="server"></asp:Literal>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </td>
                                            </tr>
                                            <tr align="right">
                                                <td>
                                                    <asp:Button ID="ClosePUp" class="altMedium0BlueButtonFormat" runat="server" Text="<%$ Resources:WebResources, Close%>" />
                                                    <input name="btnClose" validationgroup="Uploadd" id="btnClose" type="button" class="altMedium0BlueButtonFormat"
                                                        runat="server" value="<%$ Resources:WebResources, Submit%>" onserverclick="SubmitMCUSelection" />
                                                </td>
                                            </tr>
                                            <tr style="display: none">
                                                <td align="center">
                                                    <asp:DropDownList ID="lstBridgeType" DataTextField="name" DataValueField="ID" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="lstBridgeStatus" DataTextField="name" DataValueField="ID" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>

    <script type="text/javascript" src="inc/softedge.js"></script>

    <script type="text/javascript">
        function fnInvokePopup() {
            document.getElementById('CustomTrigger').click();
            return false;
        }

        document.onkeydown = function(evt) {
            evt = evt || window.event;
            var keyCode = evt.keyCode;
            if (keyCode == 8) {
                if (document.getElementById("btnCancel") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnCancel").click();
                        return false;
                    }
                }
            }
            fnOnKeyDown(evt);
        };
    </script>

</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
