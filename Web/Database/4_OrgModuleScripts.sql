/*   **** UpdateScriptforOrgModule.sql - start  **** */

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Org_List_D]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Org_List_D](
	[orgId] [int] IDENTITY(11,1) NOT NULL,
	[orgname] [nvarchar](50) NOT NULL,
	[address1] [nvarchar](50) NULL,
	[address2] [nvarchar](50) NULL,
	[city] [nvarchar](50) NULL,
	[state] [int] NULL,
	[country] [int] NULL,
	[zipcode] [nvarchar](50) NULL,
	[phone] [nvarchar](50) NULL,
	[fax] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[website] [nvarchar](50) NULL,
	[deleted] [int] NULL CONSTRAINT [DF_Org_List_D_deleted]  DEFAULT ((0)),
	[disabled] [int] NULL CONSTRAINT [DF_Org_List_D_disabled]  DEFAULT ((0))
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Org_Settings_D]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Org_Settings_D](
	[LastModified] [datetime] NULL,
	[LastModifiedUser] [int] NULL,
	[tzsystemid] [int] NULL,
	[Logo] [nvarchar](256) NULL,
	[CompanyTel] [nvarchar](20) NULL,
	[CompanyEmail] [nvarchar](512) NULL,
	[CompanyURL] [nvarchar](512) NULL,
	[Connect2] [smallint] NOT NULL,
	[DialOut] [smallint] NULL,
	[AccountingLogic] [smallint] NULL,
	[BillPoint2Point] [smallint] NULL,
	[AllLocation] [smallint] NULL,
	[RealtimeStatus] [int] NOT NULL,
	[BillRealTime] [smallint] NOT NULL,
	[MultipleDepartments] [int] NULL,
	[overAllocation] [smallint] NULL,
	[Threshold] [int] NULL,
	[autoApproveImmediate] [smallint] NOT NULL,
	[adminEmail1] [int] NULL,
	[adminEmail2] [int] NULL,
	[adminEmail3] [int] NULL,
	[AutoAcceptModConf] [smallint] NULL,
	[recurEnabled] [smallint] NULL,
	[responsetime] [int] NULL,
	[responsemessage] [nvarchar](4000) NULL,
	[SystemStartTime] [datetime] NULL,
	[SystemEndTime] [datetime] NULL,
	[Open24hrs] [smallint] NULL,
	[Offdays] [nvarchar](50) NULL,
	[ISDNLineCost] [decimal](2, 0) NOT NULL,
	[ISDNPortCost] [decimal](2, 0) NOT NULL,
	[IPLineCost] [decimal](2, 0) NOT NULL,
	[IPPortCost] [decimal](2, 0) NOT NULL,
	[ISDNTimeFrame] [smallint] NULL,
	[dynamicinviteenabled] [smallint] NULL,
	[doublebookingenabled] [smallint] NULL,
	[IMEnabled] [smallint] NULL CONSTRAINT [DF_Org_Settings_D_IMEnabled]  DEFAULT ((0)),
	[IMRefreshConn] [float] NULL,
	[IMMaxUnitConn] [int] NULL,
	[IMMaxSysConn] [int] NULL,
	[DefaultToPublic] [smallint] NOT NULL CONSTRAINT [DF_Org_Settings_D_DefaultToPublic]  DEFAULT ((0)),
	[DefaultConferenceType] [smallint] NULL CONSTRAINT [DF_Org_Settings_D_DefaultConferenceType]  DEFAULT ((2)),
	[EnableRoomConference] [smallint] NULL CONSTRAINT [DF_Org_Settings_D_EnableRoomConference]  DEFAULT ((1)),
	[EnableAudioVideoConference] [smallint] NULL CONSTRAINT [DF_Org_Settings_D_EnableAudioVideoConference]  DEFAULT ((1)),
	[EnableAudioOnlyConference] [smallint] NULL CONSTRAINT [DF_Org_Settings_D_EnableAudioOnlyConference]  DEFAULT ((1)),
	[DefaultCalendarToOfficeHours] [smallint] NULL CONSTRAINT [DF_Org_Settings_D_DefaultCalendarToOfficeHours]  DEFAULT ((1)),
	[RoomTreeExpandLevel] [varchar](20) NULL CONSTRAINT [DF_Org_Settings_D_RoomTreeExpandLevel]  DEFAULT ((3)),
	[EnableBufferZone] [smallint] NULL,
	[EnableCustomOption] [smallint] NULL,
	[RoomLimit] [int] NULL CONSTRAINT [DF_Org_Settings_D_RoomLimit]  DEFAULT ((0)),
	[MCULimit] [int] NULL CONSTRAINT [DF_Org_Settings_D_MCULimit]  DEFAULT ((0)),
	[UserLimit] [int] NULL CONSTRAINT [DF_Org_Settings_D_UserLimit]  DEFAULT ((0)),
	[OrgId] [int] NOT NULL,
	[MaxNonVideoRooms] [int] NULL,
	[MaxVideoRooms] [int] NULL,
	[MaxEndpoints] [int] NULL,
	[MaxCDRs] [int] NULL,
	[EnableFacilities] [int] NULL,
	[EnableCatering] [int] NULL,
	[EnableHousekeeping] [int] NULL,
	[EnableAPI] [int] NULL,
	[Language] [int] NULL,
	[ExchangeUserLimit] [int] NULL,
	[DominoUserLimit] [int] NULL,
 CONSTRAINT [PK_Org_Settings_D] PRIMARY KEY CLUSTERED 
(
	[OrgId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Org_Diagnostics_D]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Org_Diagnostics_D](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[PurgeType] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[TimeofDay] [datetime] NULL,
	[DayofWeek] [int] NULL,
	[ServiceStatus] [int] NULL,
	[PurgeData] [int] NULL,
	[OrgId] [int] NULL,
 CONSTRAINT [PK_Org_Diagnostics_D] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END

/* Sys_Settings_D */
 
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D
	DROP CONSTRAINT DF_Sys_Settings_D_IMEnabled
GO
ALTER TABLE dbo.Sys_Settings_D
	DROP CONSTRAINT DF_Sys_Settings_D_DefaultToPublic
GO
ALTER TABLE dbo.Sys_Settings_D
	DROP CONSTRAINT DF_Sys_Settings_D_DefaultConferenceType
GO
ALTER TABLE dbo.Sys_Settings_D
	DROP CONSTRAINT DF_Sys_Settings_D_EnableRoomConference
GO
ALTER TABLE dbo.Sys_Settings_D
	DROP CONSTRAINT DF_Sys_Settings_D_EnableAudioVideoConference
GO
ALTER TABLE dbo.Sys_Settings_D
	DROP CONSTRAINT DF_Sys_Settings_D_EnableAudioOnlyConference
GO
ALTER TABLE dbo.Sys_Settings_D
	DROP CONSTRAINT DF_Sys_Settings_D_DefaultCalendarToOfficeHours
GO
ALTER TABLE dbo.Sys_Settings_D
	DROP CONSTRAINT DF_Sys_Settings_D_RoomTreeExpandLevel
GO

ALTER TABLE dbo.Sys_Settings_D
	DROP COLUMN tzsystemid, Logo, CompanyTel, CompanyEmail, CompanyURL, Connect2, DialOut, AccountingLogic, BillPoint2Point, AllLocation, RealtimeStatus, BillRealTime, MultipleDepartments, overAllocation, Threshold, autoApproveImmediate, adminEmail1, adminEmail2, adminEmail3, AutoAcceptModConf, recurEnabled, responsetime, responsemessage, SystemStartTime, SystemEndTime, Open24hrs, Offdays, ISDNLineCost, ISDNPortCost, IPLineCost, IPPortCost, ISDNTimeFrame, dynamicinviteenabled, doublebookingenabled, IMEnabled, IMRefreshConn, IMMaxUnitConn, IMMaxSysConn, DefaultToPublic, DefaultConferenceType, EnableRoomConference, EnableAudioVideoConference, EnableAudioOnlyConference, DefaultCalendarToOfficeHours, RoomTreeExpandLevel
GO

ALTER TABLE dbo.Sys_Settings_D ADD LastModifiedUser int NULL CONSTRAINT [DF_Sys_Settings_D_LastModifiedUser] DEFAULT ((11)) 


COMMIT


/* Add orgId to master and transaction tables */
ALTER TABLE [dbo].[Conf_Conference_D] ADD [orgId] [int] NULL CONSTRAINT [DF_Conf_Conference_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Dept_CustomAttr_D] ADD [orgId] [int] NOT NULL CONSTRAINT [DF_Dept_CustomAttr_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Dept_List_D] ADD [orgId] [int] NULL CONSTRAINT [DF_Dept_List_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Ept_List_D] ADD [orgId] [int] NULL CONSTRAINT [DF_Ept_List_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Grp_Detail_D] ADD [orgId] [int] NULL CONSTRAINT [DF_Grp_Detail_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Loc_Room_D] ADD [orgId] [int] NULL CONSTRAINT [DF_Loc_Room_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Loc_Tier2_D] ADD [orgId] [int] NULL CONSTRAINT [DF_Loc_Tier2_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Loc_Tier3_D] ADD [orgId] [int] NULL CONSTRAINT [DF_Loc_Tier3_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Mcu_List_D] ADD [orgId] [int] NULL CONSTRAINT [DF_MCU_List_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Sys_Approver_D] ADD [orgId] [int]  NULL CONSTRAINT [DF_Sys_Approver_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Sys_TechSupport_D] ADD [orgId] [int]  NOT NULL CONSTRAINT [DF_Sys_TechSupport_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Tmp_List_D] ADD [orgId] [int] NULL CONSTRAINT [DF_Tmp_List_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Usr_Accord_D] ADD [orgId] [int] NULL CONSTRAINT [DF_Usr_Accord_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Usr_Roles_D] ADD [crossaccess] [int] NULL CONSTRAINT [DF_Usr_Roles_D_crossaccess] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Usr_SearchTemplates_D] ADD[orgId] [int] NULL CONSTRAINT [DF_Usr_SearchTemplates_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Usr_Templates_D] ADD [companyId] [int] NULL CONSTRAINT [DF_Usr_Templates_D_orgId] DEFAULT ((11))
GO
ALTER TABLE dbo.Inv_Category_D ADD orgId int NULL CONSTRAINT [DF_Inv_Category_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Inv_ItemCharge_D] ADD [orgId] [int] NULL CONSTRAINT [DF_Inv_ItemCharge_D_orgId] DEFAULT ((11))
GO
ALTER TABLE [dbo].[Inv_List_D] ADD [orgId] [int] NULL CONSTRAINT [DF_Inv_List_D_orgId] DEFAULT ((11))
GO
ALTER TABLE dbo.Inv_Workorder_D ADD orgId int NULL CONSTRAINT [DF_Inv_workorder_D_orgId] DEFAULT ((11)) 
GO
ALTER TABLE dbo.Acc_Balance_D ADD orgId int NULL CONSTRAINT [DF_Acc_Balance_D_orgId] DEFAULT ((11)) 
GO


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Approver_D
	DROP CONSTRAINT DF_Sys_Approver_D_orgId
GO
CREATE TABLE dbo.Tmp_Sys_Approver_D
	(
	approverid int NOT NULL,
	orgId int NOT NULL,
	UID int NOT NULL IDENTITY (1, 1)
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Sys_Approver_D ADD CONSTRAINT
	DF_Sys_Approver_D_orgId DEFAULT ((11)) FOR orgId
GO
SET IDENTITY_INSERT dbo.Tmp_Sys_Approver_D OFF
GO
IF EXISTS(SELECT * FROM dbo.Sys_Approver_D)
	 EXEC('INSERT INTO dbo.Tmp_Sys_Approver_D (approverid, orgId)
		SELECT approverid, orgId FROM dbo.Sys_Approver_D WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Sys_Approver_D
GO
EXECUTE sp_rename N'dbo.Tmp_Sys_Approver_D', N'Sys_Approver_D', 'OBJECT' 
GO
ALTER TABLE dbo.Sys_Approver_D ADD CONSTRAINT
	PK_Sys_Approver_D PRIMARY KEY CLUSTERED 
	(
	UID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_TechSupport_D
	DROP CONSTRAINT DF_Sys_TechSupport_D_orgId
GO
CREATE TABLE dbo.Tmp_Sys_TechSupport_D
	(
	name nvarchar(256) NOT NULL,
	email nvarchar(512) NULL,
	phone nvarchar(50) NULL,
	info nvarchar(2000) NULL,
	feedback nvarchar(2000) NULL,
	id int NOT NULL IDENTITY (1, 1),
	orgId int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Sys_TechSupport_D ADD CONSTRAINT
	DF_Sys_TechSupport_D_orgId DEFAULT ((11)) FOR orgId
GO
SET IDENTITY_INSERT dbo.Tmp_Sys_TechSupport_D ON
GO
IF EXISTS(SELECT * FROM dbo.Sys_TechSupport_D)
	 EXEC('INSERT INTO dbo.Tmp_Sys_TechSupport_D (name, email, phone, info, feedback, id, orgId)
		SELECT name, email, phone, info, feedback, id, orgId FROM dbo.Sys_TechSupport_D WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Sys_TechSupport_D OFF
GO
DROP TABLE dbo.Sys_TechSupport_D
GO
EXECUTE sp_rename N'dbo.Tmp_Sys_TechSupport_D', N'Sys_TechSupport_D', 'OBJECT' 
GO
COMMIT


/*   **** UpdateScriptforOrgModule.sql - end  **** */


/*   **** UpdateOrgData.sql - start  **** */

Insert into [Org_List_D]
([orgname],[address1],[address2],[city],[state],[country],[zipcode],[phone],[fax],[email],[website],[deleted],[disabled])
values
('Base Company','','','',34,225,'12345','','','basecompany@myvrm.com','www.myvrm.com',0,0)
Go

INSERT INTO [dbo].[Org_Settings_D]
(
[LastModified], [LastModifiedUser], [Logo], [CompanyTel], [CompanyEmail], 
[CompanyURL], [Connect2], [DialOut], [AccountingLogic], [BillPoint2Point], 
[AllLocation], [tzsystemid], [RealtimeStatus], [BillRealTime], 
[MultipleDepartments], [overAllocation], [Threshold], [autoApproveImmediate], 
[adminEmail1], [adminEmail2],[adminEmail3], [AutoAcceptModConf], [recurEnabled], 
[responsetime], [responsemessage],[SystemStartTime], [SystemEndTime], [Open24hrs], 
[Offdays], [ISDNLineCost], [ISDNPortCost], [IPLineCost], [IPPortCost], 
[ISDNTimeFrame], [dynamicinviteenabled], 
[doublebookingenabled], [IMEnabled], [IMRefreshConn], [IMMaxUnitConn], 
[IMMaxSysConn], [DefaultToPublic], [DefaultConferenceType], [EnableRoomConference],
[EnableAudioVideoConference], [EnableAudioOnlyConference], [DefaultCalendarToOfficeHours], 
[RoomTreeExpandLevel], EnableBufferZone,EnableCustomOption,RoomLimit,
MCULimit,UserLimit,OrgId ,[MaxNonVideoRooms]
,[MaxVideoRooms],[MaxEndpoints],[MaxCDRs],[EnableFacilities]
,[EnableCatering],[EnableHousekeeping],[EnableAPI],[Language]
,[ExchangeUserLimit],[DominoUserLimit]
)
VALUES
('20071010 16:21:56.420',11, N'ABC', NULL, N'vrmadmin@myVrm.com', 
N'', 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, NULL, 
0, 0, 0, 0, 1, 1, 60, N'', '19000101 00:00:00.000', 
'19000101 23:59:00.000', 1, N'1,7', 2, 1, 33, 3, 
1, 1, 0, 0, 30, 3, 30, 0,7,1, 1, 1, 1, 3,1,1,1,1,1,11,0,1,1,0,0,0,0,0,0,0,0)
GO

/* Insert default data into diagnostics table for base company */
Insert into [dbo].[Org_Diagnostics_D]
(PurgeType,UpdatedDate,TimeofDay,DayofWeek,ServiceStatus,PurgeData,OrgId)
values
(0,getdate(),getdate(),5,0,1,11)
Go


update Sys_TechSupport_D set orgid = 11
Go
update Sys_Approver_D set orgid = 11
Go
update Conf_Conference_D set orgid = 11
Go
update Usr_SearchTemplates_D set orgid = 11
Go
update Usr_Templates_D set companyId = 11
Go
update Loc_Room_D set orgid = 11
Go
update usr_list_d set companyid = 11
Go
update usr_inactive_d set companyid = 11
Go
update MCU_List_D set orgid = 11
Go
update Dept_List_D set orgid = 11
Go
update Grp_Detail_D set orgid = 11
Go
update Loc_Tier3_D set orgid = 11
Go
update Loc_Tier2_D set orgid = 11
Go
update Tmp_list_d set orgid = 11
Go
Update Dept_CustomAttr_D set orgid=11
Go
update Inv_Category_D set orgid =11
GO
update Inv_Workorder_D set orgid =11
GO
update Acc_Balance_D set orgid =11
GO
update Inv_List_D set orgid =11
GO
update Inv_ItemCharge_D set orgid =11
GO
update Usr_Accord_D set orgid =11
GO
update Usr_GuestList_D set companyId =11
GO
Update Ept_List_D set orgid=11
GO

/*   **** UpdateOrgData.sql - end  **** */

