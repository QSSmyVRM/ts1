﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100866 End--%>

<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_MCUPopupList" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<script type="text/javascript" src="script/wincheck.js"></script>
<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>");

    function grid_SelectionChanged(s, e) {
        s.GetSelectedFieldValues("ifrmDetails", GetSelectedFieldValuesCallback);
    }

    function GetSelectedFieldValuesCallback(values) {
        var mcuinfo = "";
        
        for (var i = 0; i < values.length; i++) {
            mcuinfo += Addparticipants(values[i]);
        }

        parent.opener.document.getElementById("MCUsInfo").value = mcuinfo        
        parent.opener.ifrmMemberlist.location.reload(true);
    }

    function Addparticipants(mcuinfo) {

        mcuary = mcuinfo.split("|");
        mcuid = mcuary[0]; mcuNm = mcuary[1]; mcuAdmin = mcuary[2]; mcuTzn = mcuary[3];

        var mcusinfo = "", loadBal = "0", overFlow ="0", leadMCU = "0";

        var ctrlMCUsInfo = parent.opener.document.getElementById("MCUsInfo");
        if (ctrlMCUsInfo.value != "") {
            mcusary = ctrlMCUsInfo.value.split("||");
             for (var i = 0; i < mcusary.length - 1; i++) {
                 if (mcusary[i] == "")
                     continue;
                 var mcus1ary = mcusary[i].split("!!");

                 if (mcuid == mcus1ary[0]) {

                     loadBal = mcus1ary[4];
                     overFlow = mcus1ary[5];
                     leadMCU = mcus1ary[6];
                 }
             }
         }

         loadBal = 1

        mcusinfo = mcuid + "!!" + mcuNm + "!!" + mcuAdmin + "!!" + mcuTzn + "!!"+ loadBal + "!!"+ overFlow +"!!"+ leadMCU + "||" ;

        return mcusinfo;
    }

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MCU Members</title><%--ZD 100040--%>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
        </Scripts>
    </asp:ScriptManager>
    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <input type="hidden" id="partysValue" name="partysValue" value="" />
                    <asp:Panel ID="switchOrgPnl" runat="server" HorizontalAlign="Center" Width="100%"
                        CssClass="treeSelectedNode">
                        <table width="100%" align="center" border="0">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <dxwgv:ASPxGridView AllowSort="true" ID="grid" ClientInstanceName="grid" runat="server"
                                        KeyFieldName="id" Width="100%" EnableRowsCache="True" OnDataBound="ASPxGridView1_DataBound">
                                        <Columns>
                                            <dxwgv:GridViewDataTextColumn FieldName="name" Caption="<%$ Resources:WebResources, approvalstatus_MCU%>"
                                                VisibleIndex="0" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" />
                                            <dxwgv:GridViewDataTextColumn FieldName="administrator" Caption="<%$ Resources:WebResources, Administrator%>"
                                                VisibleIndex="1" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" />
                                            <dxwgv:GridViewDataTextColumn FieldName="timeZone" Caption="<%$ Resources:WebResources, TimeZone%>"
                                                VisibleIndex="2" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center"/>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" VisibleIndex="3" Caption="<%$ Resources:WebResources, Select%>">
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="id" Visible="False" />
                                            <dxwgv:GridViewDataColumn FieldName="ifrmDetails" Visible="False" />
                                        </Columns>
                                        <Styles>
                                            <CommandColumn Paddings-Padding="1" />
                                        </Styles>                                      
                                        <SettingsPager Mode="ShowPager" PageSize="5" AlwaysShowPager="true" Position="Bottom"><%--ZD 100040--%>
                                        </SettingsPager>
                                        <ClientSideEvents SelectionChanged="grid_SelectionChanged" />
                                    </dxwgv:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <input align="middle" type="button" runat="server" style="cursor: pointer" id="ClosePUp"
                                        value=" <%$ Resources:WebResources, Close%> " onclick="javascript:window.close();"
                                        class="altMedium0BlueButtonFormat" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
    }
</script>
<%--ZD 100428 END--%>
