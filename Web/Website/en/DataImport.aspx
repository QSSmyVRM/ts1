<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DataImport.aspx.cs" Inherits="ns_DataImport.DataImport" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --> <%--FB 2779--%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<!-- ZD 101022 -->

<script type="text/javascript" language="JavaScript" src="inc/functions.js"></script><%--ZD 102364--%> <%-- ZD 102723 --%><%--ZD 103895--%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<script language="javascript">
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
function DataLoading()
{
    var obj = document.getElementById("tblDataImport");
    if (obj != null)
        obj.style.display="";
}
</script>
<%--ZD 103895 Start--%>
<style type="text/css">
    
        .file_input_textbox {
            height:20px; 
            width:200px; 
            float:left; 
            }

        .file_input_div {
            position: relative;
            width:80px; 
            height:30px;
            overflow: hidden;
            float:left; }

        .file_input_button1 {
            width: 80px;            
            top:0px; 
            border:1px solid #A7958B;
            padding:2px 8px 2px 8px; 
            font-weight: normal;
            height:24px;
            margin:0px; 
            margin-right:5px; 
            vertical-align:bottom; 
            background-color:#D4D0C8; 
            }

        .file_input_hidden {
            font-size :45px;
            position:absolute;            
            right:0px;top:0px;
            cursor:pointer; 
            opacity:0; 
            filter:alpha(opacity=0); 
            -ms-filter:"alpha(opacity=0)";
            -khtml-opacity:0;
            -moz-opacity:0;
            }        
        </style>
        <%--ZD 103895 End--%>
    <title>Database Import</title>
</head>
<body>
    <form id="frmDataImport" runat="server">
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
        <h3 style="text-align: center">
            <asp:Label id="lblHeader" runat="server" text="<%$ Resources:WebResources, DataImport_DataImportTool%>"></asp:Label> </h3>
            <br />
            <center>
                <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
            </center>
            <br />
            <center>
            <asp:GridView ID="newErrGrid" runat ="server" Width="65%" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField HeaderStyle-Width="12%" DataField="Row No" HeaderText="<%$ Resources:WebResources, DataImport_RowNo%>" />
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="Reason" HeaderText="<%$ Resources:WebResources, DataImport_Reason%>"/>
            </Columns>
            </asp:GridView>
            </center> <%--ZD 104091--%>
            <center>
                <asp:Label ID="lblinfo" runat="server" CssClass="lblError"></asp:Label>
            </center>
            <br />
            <center>
            <asp:GridView ID="InfoGrid" runat ="server" Width="65%" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField HeaderStyle-Width="12%" DataField="Row No" HeaderText="<%$ Resources:WebResources, DataImport_RowNo%>" />
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="Reason" HeaderText="<%$ Resources:WebResources, DataImport_Reason%>"/>
            </Columns>
            </asp:GridView>
            </center>
            <br /><br />
            <table id="tblDataImport" width="100%">
            <tr>
                 <td align="center">
                    <b><img border="0" src="image/wait1.gif" alt="Loading.." ><%-- FB 2742 --%> <%--ZD 100419--%>
               </td>
            </tr>
           </table>  <%--ZD 101730 start--%>      
            <table width="100%" bgcolor="white" cellpadding="1" cellspacing="0" border="0">
                <tr align="center">
                    <td width="35%" align="left" nowrap="nowrap" > <%--ZD 100456--%>
                        <div id="tdDBType">
                        <b><asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, HolidayImport_ExternalDataba%>" runat="server"></asp:Literal></b>&nbsp;
                        <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstDatabaseType" runat="server" style="width:140px" >
                            <asp:ListItem Selected="True" Text="Rendezvous" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                        </div>
                    </td>
                    <td nowrap="nowrap" id="tdFile" style="width:60%"> 
                        <input type="text" class="file_input_textbox" readonly="readonly" id="txtFile"  value='<%$ Resources:WebResources, Nofileselected%>' runat="server" style="width:290px"/>
                        <div class="file_input_div"><input type="button" runat="server" value="<%$ Resources:WebResources, Browse%>" class="file_input_button1"  onclick="document.getElementById('fleMasterCSV').click();return false;"  /><%--ZD 103895--%>
                            <asp:FileUpload ID="fleMasterCSV" EnableViewState="true" TabIndex="-1"  runat="server" class="file_input_hidden" OnChange="getfilename(this)" /><%--ZD 103895--%>
                        </div>
                     <asp:Button ID="btnGetDataTable" ValidationGroup="Submit1" runat="server" OnClick="GenerateDataTable" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, Upload%>" class="file_input_hidden" style="margin-left:-95px" OnClientClick="return fnUpload(document.getElementById('fleMasterCSV').value, this.id, 'Submit1');" /><%--ZD 102277--%> <%--ZD 102364--%>
                    
                    </td> <%--ZD 101730 End--%>      
                </tr><%--ZD 102277--%>
                <tr align="center">
                <td width="35%" align="left">
                </td>
                <td style="width:60%" align="left">
                <asp:RegularExpressionValidator ID="regFNVal1" ControlToValidate="fleMasterCSV"
                                                    Display="dynamic" runat="server" ValidationGroup="Submit1" SetFocusOnError="true"
                                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters46%>"
                                                    ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                </td><%--ZD 102277--%>
                </tr>
            </table>
            <br /><br />
            <table width="100%" bgcolor="white" cellpadding="1" cellspacing="0" border="1" >
                <tr id="trTier1" runat="server"><%--ZD 102029--%>
                    <td style="text-align: center">
                        1</td>
                    <td>
                        <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, DataImport_ImportTier1%>" runat="server"></asp:Literal></td>
                    <td width="20%" align="center">
                        <asp:Button ID="btnImportTier1" runat="server" Width="60pt"
                            Text="<%$ Resources:WebResources, UITextChange_btnSubmit%>" OnClick="ImportTier1s" OnClientClick="javascript:DataLoading()" /></td> <%--FB 2519--%>
                </tr>
                <tr bgcolor=Gainsboro id="trTier2" runat="server"><%--ZD 102029--%>
                    <td style="text-align: center">
                        2</td>
                    <td>
                       <asp:Literal  Text="<%$ Resources:WebResources, DataImport_ImportTier2%>" runat="server"></asp:Literal></td>
                    <td align="center">
                        <asp:Button ID="btnImportTier2" runat="server" Width="60pt"
                            Text="<%$ Resources:WebResources, UITextChange_btnSubmit%>" OnClick="ImportTier2s" OnClientClick="javascript:DataLoading()" /></td><%--FB 2519--%>
                </tr>
                <tr id="trDept" runat="server"><%--ZD 102029--%>
                    <td style="text-align: center">
                        3</td>
                    <td>
                        <asp:Literal  Text="<%$ Resources:WebResources, DataImport_ImportDepartment%>" runat="server"></asp:Literal></td>
                    <td align="center">
                        <asp:Button ID="btnImportDepartment" runat="server"  Width="60pt"
                            Text="<%$ Resources:WebResources, UITextChange_btnSubmit%>" OnClick="ImportDepartments"  OnClientClick="javascript:DataLoading()" /></td> <%--FB 2519--%>
                </tr>
                
                <tr bgcolor=Gainsboro>
                    <td style="text-align: center">
                        <asp:Label ID="lblMCuNo" runat="server">4</asp:Label><%--ZD 102029--%>
                        </td>
                    <td>
                        <asp:Literal  Text="<%$ Resources:WebResources, DataImport_ImportMCU%>" runat="server"></asp:Literal></td>
                    <td align="center">
                        <asp:Button ID="btnImportmcu" runat="server"  Width="60pt"
                            Text="<%$ Resources:WebResources, UITextChange_btnSubmit%>" OnClick="Importmcu" OnClientClick="javascript:DataLoading()"  /></td> <%--FB 2519--%>
                </tr>
                
                <tr>
                    <td style="text-align: center">
                        <asp:Label ID="lblEptNo" runat="server">5</asp:Label><%--ZD 102029--%>
                        
                        </td>
                    <td>
                        <asp:Literal  Text="<%$ Resources:WebResources, DataImport_ImportEndpoint%>" runat="server"></asp:Literal></td>
                    <td align="center">
                        <asp:Button ID="btnImportEndpoints" runat="server" Width="60pt"
                            Text="<%$ Resources:WebResources, UITextChange_btnSubmit%>" OnClick="ImportEndpoints" OnClientClick="javascript:DataLoading()"  /></td> <%--FB 2519--%>
                </tr>
                <tr bgcolor=Gainsboro>
                    <td style="text-align: center">
                        <asp:Label ID="lblRmNo" runat="server">6</asp:Label><%--ZD 102029--%>
                        </td>
                    <td>
                        <asp:Literal  Text="<%$ Resources:WebResources, DataImport_ImportRoom%>" runat="server"></asp:Literal></td>
                    <td align="center">
                        <asp:Button ID="btnImportRooms" runat="server" Width="60pt"
                          Text="<%$ Resources:WebResources, UITextChange_btnSubmit%>" OnClick="ImportRooms" OnClientClick="javascript:DataLoading()" /></td> <%--FB 2519--%>
                </tr>
                <tr>
                    <td style="text-align: center">
                        <asp:Label ID="lblUsrNo" runat="server">7</asp:Label><%--ZD 102029--%>
                    </td>
                    <td>
                        <asp:Literal   Text="<%$ Resources:WebResources, DataImport_ImportUser%>" runat="server"></asp:Literal> </td>
                    <td align="center">
                        <asp:Button ID="btnImportUsers" runat="server" Width="60pt"
                            Text="<%$ Resources:WebResources, UITextChange_btnSubmit%>" OnClick="ImportUsers" OnClientClick="javascript:DataLoading()" /></td> <%--FB 2519--%>
                </tr>
                <tr bgcolor=Gainsboro id="trConf" runat="server"> <%--ZD 102029--%>
                    <td style="text-align: center">
                        <asp:Label ID="lblConfNo" runat="server">8</asp:Label>
                        </td>
                    <td>
                        <asp:Literal   Text="<%$ Resources:WebResources, DataImport_ImportConference%>" runat="server"></asp:Literal></td>
                    <td align="center">
                        <asp:Button ID="btnImportConferences" runat="server"  Text="<%$ Resources:WebResources, UITextChange_btnSubmit%>" OnClick="ImportConferences" OnClientClick="javascript:DataLoading()" Width="60pt" /></td> <%--FB 2519--%>
                </tr>
                 <tr  id="trCSS" runat="server"><%--ZD 102029--%>
                    
                    <td style="text-align: center">
                        9</td>
                    <td nowrap="nowrap">
                        <table width="100%">
                            <tr>
                                <td width="10%" nowrap="nowrap">
                                    <asp:Literal ID="Literal6"  Text="<%$ Resources:WebResources, DataImport_ImportDefaultCSS%>" runat="server"></asp:Literal>
                                </td>
                                <td nowrap="nowrap">
                                    <input type="text" class="file_input_textbox" id="txtFileinput" readonly="readonly" runat="server" value='<%$ Resources:WebResources, Nofileselected%>' style="width:290px"/> <%--ZD 101730--%>
                                    <div class="file_input_div"><input type="button" id="btnUploadConf" runat="server" value="<%$ Resources:WebResources, Browse%>" class="file_input_button"  onclick="document.getElementById('cssXMLFileUpload').click();return false;"  /> <%--ZD 100745--%>
                                    <asp:FileUpload ID="cssXMLFileUpload" EnableViewState="true" runat="server" CssClass="altText" OnChange="getfilename(this)"  /> </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="center">
                        <asp:Button ID="btnImportDefaultCSSXML" runat="server" OnClick="ImportDefaultCSSXML" Text="<%$ Resources:WebResources, DefaultLicense_btnSubmit%>" OnClientClick="javascript:DataLoading()"  Width="60pt"/> <%--FB 2519--%>
                   </td>
                </tr>
            </table>
        <br />
        <table>
            <tr id="trNote">
                <td align="left">
                    <b><asp:Literal ID="Literal3"  Text="<%$ Resources:WebResources, HolidayImport_NOTE%>" runat="server"></asp:Literal> </b> <asp:Literal ID="Literal2"  Text="<%$ Resources:WebResources, HolidayImport_Thissectionsh%>" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
        <%--ZD 100456--%>
         <table border="0" width="100%">
          <tr align="center">
                <td align="center"> 
                  <button type="button" id="btnCancel" class="altLongYellowButtonFormat" onclick="fnCancel();" style="width:100px">
                  <asp:Literal ID="Literal4"  Text="<%$ Resources:WebResources, ManageDepartment_btnCancel%>" runat="server"></asp:Literal> </button>
                </td> 
             </tr>
         </table>
        <%--<table id="tblDataImport">
            <tr>
                <td>
                    <b><font color="#FF00FF" size="2">Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border="0" src="image/wait1.gif" width="100" height="12">
                </td>
            </tr>
        </table>--%>
    </form>
<br />
<br />
<p>&nbsp;</p>
<p>&nbsp;</p>
<script language="javascript">
    document.getElementById("tblDataImport").style.display = "none";

    var strRequest = "";
    strRequest = "<%=strRequest %>";
    if (strRequest == "") {
        if (document.getElementById("btnCancel")) {
            document.getElementById("btnCancel").style.display = "none";
        }
    }
    else {
        if(document.getElementById("tdDBType"))
            document.getElementById("tdDBType").style.display = "none";

        if (document.getElementById("trNote"))
            document.getElementById("trNote").style.display = "none";

        document.onkeydown = function(evt) {
            evt = evt || window.event;
            var keyCode = evt.keyCode;
            if (keyCode == 8) {
                if (document.getElementById("btnCancel") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnCancel").click();
                        return false;
                    }
                }
            }
            fnOnKeyDown(evt);
        };
    }
    
    function fnCancel() //ZD 100456
    {
        window.location.href = 'ManageDataImport.aspx';
        return false;
    }

//ZD 101344
    function getfilename(obj) {
        var fileInputVal = obj.value;
        fileInputVal = fileInputVal.replace("C:\\fakepath\\", "");
        if (navigator.userAgent.indexOf("MSIE") > -1)
            obj.parentNode.parentNode.childNodes[0].value = fileInputVal;
        else
            obj.parentNode.parentNode.childNodes[1].value = fileInputVal;
    }

    //ZD 103895 - Start
    function fnUpload(obj, btnid, grpname) {

        if (!Page_ClientValidate(grpname))
            return Page_IsValid;

        if (obj == null || obj == undefined || obj == "") {
            alert(WithoutImage);
            return false;
        }
        else {
            document.getElementById(btnid).click();            
        }
    }
    


    //ZD 100745
    /*if (document.getElementById('fleMasterCSV') != null)
        document.getElementById('fleMasterCSV').setAttribute("onblur", "document.getElementById('btnGetDataTable').focus(); document.getElementById('btnGetDataTable').setAttribute('onfocus', '');");

    if (document.getElementById('btnGetDataTable') != null) {
        if (strRequest == "")
            document.getElementById('btnGetDataTable').setAttribute("onblur", "document.getElementById('btnImportTier1').focus(); document.getElementById('btnImportTier1').setAttribute('onfocus', '');");
        else
            document.getElementById('btnGetDataTable').setAttribute("onblur", "document.getElementById('btnImportmcu').focus(); document.getElementById('btnImportmcu').setAttribute('onfocus', '');");
    }*/
    //ZD 103895 - End
    if (document.getElementById('btnImportTier1') != null)
        document.getElementById('btnImportTier1').setAttribute("onblur", "document.getElementById('btnImportTier2').focus(); document.getElementById('btnImportTier2').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportTier2') != null)
        document.getElementById('btnImportTier2').setAttribute("onblur", "document.getElementById('btnImportDepartment').focus(); document.getElementById('btnImportDepartment').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportDepartment') != null)
        document.getElementById('btnImportDepartment').setAttribute("onblur", "document.getElementById('btnImportmcu').focus(); document.getElementById('btnImportmcu').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportmcu') != null)
        document.getElementById('btnImportmcu').setAttribute("onblur", "document.getElementById('btnImportEndpoints').focus(); document.getElementById('btnImportEndpoints').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportEndpoints') != null)
        document.getElementById('btnImportEndpoints').setAttribute("onblur", "document.getElementById('btnImportRooms').focus(); document.getElementById('btnImportRooms').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportRooms') != null)
        document.getElementById('btnImportRooms').setAttribute("onblur", "document.getElementById('btnImportUsers').focus(); document.getElementById('btnImportUsers').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportUsers') != null) {
        if (strRequest == "")
            document.getElementById('btnImportUsers').setAttribute("onblur", "document.getElementById('btnImportConferences').focus(); document.getElementById('btnImportConferences').setAttribute('onfocus', '');");
        else
            document.getElementById('btnImportUsers').setAttribute("onblur", "document.getElementById('btnCancel').focus(); document.getElementById('btnCancel').setAttribute('onfocus', '');");
    }

    if (document.getElementById('btnImportConferences') != null)
        document.getElementById('btnImportConferences').setAttribute("onblur", "document.getElementById('cssXMLFileUpload').focus(); document.getElementById('cssXMLFileUpload').setAttribute('onfocus', '');");

    if (document.getElementById('cssXMLFileUpload') != null)
        document.getElementById('cssXMLFileUpload').setAttribute("onblur", "document.getElementById('btnImportDefaultCSSXML').focus(); document.getElementById('btnImportDefaultCSSXML').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportDefaultCSSXML') != null)
        document.getElementById('btnImportDefaultCSSXML').setAttribute("onblur", "document.getElementById('btnCancel').focus(); document.getElementById('btnCancel').setAttribute('onfocus', '');");

    
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 
</body>
</html>
