<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.Room" %>

<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- FB 2050 -->
<!--window Dressing start-->
<% 
    if (Request.QueryString["cal"] == "2")
    { 
%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<% 
    } 
%>
<!--window Dressing end-->

<script type="text/javascript" src="inc/functions.js"></script>

<script type="text/javascript" src="script/errorList.js"></script>

<script type="text/javascript" src="extract.js"></script>

<%--Login Management--%>

<%--FB 3055-URL Starts--%>
<script>
    var ar = window.location.href.split("/");
    ar = ar[ar.length - 1];
    var pg = "";
    if (ar.indexOf("?") > -1)
        pg = ar.split("?")[1];
    else
        pg = ar;

    var page = pg.toLowerCase();
    //alert(page);
    var cnt = 0;

    if (page.indexOf("cal=2") > -1)
        cnt++;
    if (page.indexOf("cal=2") > -1 && page.indexOf("rid") > -1)
        cnt++;
    if (page.indexOf("cal=2") > -1 && page.indexOf("pageid") > -1)
        cnt++;

    if (cnt == 0)
        window.location.href = "thankyou.aspx";
</script>
<%--FB 3055-URL End--%>

<script type="text/javascript" language="javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
//ZD 100622 starts
function fnTimezonecheck()
{
    
    var lsttimezone = document.getElementById('<%=lstTimezone.ClientID%>');
    if(lsttimezone.value == "-1")
    {  
        lsttimezone.focus();
        return false;
    }   
}
//ZD 100622 Ends

function ViewEndpointDetails()
{
    val = document.getElementById("lstEndpoint").value;
	
    if (val == "" || val == "-1") {
        alert(RoomprofEnd);
    } else {
        url = "dispatcher/admindispatcher.asp?eid=" + val + "&cmd=GetEndpoint&ed=1&wintype=pop";

        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else { // has been defined
            if (!winrtc.closed) {     // still open
                winrtc.close();
                winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
                winrtc.focus();
            } else {
                winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
                winrtc.focus();
            }
        }
    }
}

function getYourOwnEmailList (i)
{
    if (i == -2)//Login Management
    {
//        url = "dispatcher/conferencedispatcher.asp?frm=roomassist&frmname=frmMainroom&cmd=GetEmailList&emailListPage=1&wintype=pop";
      if(queryField("sb") > 0 )
            url = "../en/emaillist2.aspx?t=e&frm=roomassist&wintype=ifr&fn=frmMainroom&n=";
            else
            url = "../en/emaillist2main.aspx?t=e&frm=roomassist&fn=frmMainroom&n=";
    }
    else
    {
//        url = "dispatcher/conferencedispatcher.asp?frm=approver&frmname=frmMainroom&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
        url = "../en/emaillist2main.aspx?t=e&frm=approver&fn=frmMainroom&n=" + i;
	}
    if (!window.winrtc) {	// has not yet been defined
        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
        winrtc.focus();
    } else // has been defined
        if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2735
            winrtc.focus();
        }
}


function deleteApprover(id)
{
    eval("document.frmMainroom.Approver" + id + "ID").value = "";
    eval("document.frmMainroom.Approver" + id).value = "";
}

//Endpoint Fix
function deleteEndpoint()
{
    eval("document.frmMainroom.hdnEPID").value = "";
    eval("document.frmMainroom.txtEndpoint").value = "";
}

function deleteAssistant()
{
    eval("document.frmMainroom.AssistantID").value = "";
    eval("document.frmMainroom.Assistant").value = "";
    eval("document.frmMainroom.txtContactEmail").value = ""; //ZD 100619 
    eval("document.frmMainroom.AssistantEmail").value = ""; //ZD 100619 
}


function frmMainroom_Validator()
{
    //fB 2415 Start
    if (!Page_ClientValidate())
	        return Page_IsValid; 
	//FB 2415 End
    //for DQA comments - start
    var txtroomname = document.getElementById('<%=txtRoomName.ClientID%>');
    if(txtroomname.value == "")
    {        
        reqName.style.display = 'block';
        txtroomname.focus();
        return false;
    } // FB 1640
    else if (txtroomname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$/)==-1) //FB 1888
    {        
        regRoomName.style.display = 'block';
        txtroomname.focus();
        return false;
    }    
    
    //Coomented for FB 2594
    //var txtroomphone = document.getElementById('<%=txtRoomPhone.ClientID%>');
    //if (txtroomphone.value != '' && txtroomphone.value.search(/^(\(|\d| |-|\))*$/)==-1)
    //{        
      //  regRoomPhone.style.display = 'block';
        //regRoomPhone.innerText = 'Numeric values only';
        //errLabel.innerText = 'Please Enter - Valid Room Phone Number';//Added for FB 1459
        //txtroomphone.focus();
        //return false;
    //}   
    
    var txtmaximumcapacity = document.getElementById('<%=txtMaximumCapacity.ClientID%>')
    if(txtmaximumcapacity != null && txtmaximumcapacity.value != '') //FB 2694
    {
        var maxCapVal = parseInt(txtmaximumcapacity.value);    
        if( !isFinite(maxCapVal) || maxCapVal < 0 || maxCapVal > 10000)
        {
            CapacityValidator.style.display = 'block';
            txtmaximumcapacity.focus();
            return false;
        }
    }   
    
    var txtmaxconcurrentCalls = document.getElementById('<%=txtMaxConcurrentCalls.ClientID%>');
    if (txtmaxconcurrentCalls != null && txtmaxconcurrentCalls.value != '' && txtmaxconcurrentCalls.value.search(/^(\(|\d|\))*$/)==-1) //FB 2694
    {        
        regMaxCall.style.display = 'block';
        txtmaxconcurrentCalls.focus();
        return false;
    }    
    
//ZD 101563 - Start
//    var txtsetuptime = document.getElementById('txtSetupTime.ClientID');
//    if (txtsetuptime != null && txtsetuptime.value != '' && txtsetuptime.value.search(/^(\(|\d|\))*$/)==-1) //FB 2694
//    {        
//        regMaxCall.style.display = 'block';
//        regMaxCall.focus();
//        return false;
//    }    

//    var txtteardowntime = document.getElementById('txtTeardownTime.ClientID');
//    if (txtteardowntime != null && txtteardowntime.value != '' && txtteardowntime.value.search(/^(\(|\d|\))*$/)==-1) //FB 2694
//    {        
//        regTeardownTime.style.display = 'block';
//        txtteardowntime.focus();
//        return false;
//    }    
//ZD 101563 - End
  
    
    var assistant = document.getElementById('<%=Assistant.ClientID%>');
    var editHref = document.getElementById("EditHref");
    
    if(assistant.value == "")
    {
        //alert("Please select the Assistant-in-charge");        
        AssistantValidator.style.display = 'block';
        editHref.focus();
        return false;
    }
    //for DQA comments - end
    
//    var lstvideo = document.getElementById('<%=lstVideo.ClientID%>');
//    if (lstvideo.value == "1" || lstvideo.value == "3") {//Edited for FB 1459
//    errLabel.innerText = ""; //Added for FB 1459
//        if (txtroomphone.value == "") {
//            //alert(EN_150);
//            regRoomPhone.style.display = 'block'; 
//            regRoomPhone.innerText = 'Required';
//            errLabel.innerText = 'Please Enter - Room Phone Number';//Added for FB 1459
//            txtroomphone.focus();
//            return (false);
//        }
//    }   
   
    var cb = document.getElementById('<%=txtMultipleAssistant.ClientID%>');
    if(cb.value != '')
    {
        if(cb.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+?|!`,\[\]{}\x22=:#$%&()'~]*$/)==-1)
        {   
            regMultipleAssistant.style.display = 'block';
            cb.focus();
            return false;
        }
    }
    
    var emailsary = (cb.value).split(/,| |:|;/g);
    var newval = "";	var num = 0;
    for (i = 0; i<emailsary.length; i++) {
        if (Trim(emailsary[i]) != "") {
            newval += Trim(emailsary[i]) + ";"
            num ++;
        }
    }
    newval = Trim(newval);
    while ( (newval.length > 0) && (newval.charAt(newval.length-1) == ";") )
        newval = newval.substring(0, newval.length-1);

    cb.value = newval;
    if (num > 10) {
        alert(EN_192);
        cb.focus();
        return false;
    }
    
    var lsttoptier = document.getElementById('<%=lstTopTier.ClientID%>');
    var lstmiddletier = document.getElementById('<%=lstMiddleTier.ClientID%>');
    
    if(lsttoptier.value == "-1")
    {
        //alert("Please select the Top Tier");
        reqTopTier.style.display = 'block';
        lsttoptier.focus();
        return false;
    }

    if(lstmiddletier.value == "-1")
    {
        //alert("Please select the Middle Tier");
        reqMiddleTier.style.display = 'block';
        lstmiddletier.focus();
        return false;
    }

    var txtzipcode = document.getElementById('<%=txtZipCode.ClientID%>');
    //FB 2222
    if (txtzipcode.value != '' && txtzipcode.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1) 
    {        
        regZipCode.style.display = 'block';
        txtzipcode.focus();
        return false;
    }  
    
    var txtparkingdirections = document.getElementById('<%=txtParkingDirections.ClientID%>');
    if (txtparkingdirections.value != '' && txtparkingdirections.value.search(/[^`']*/)==-1)
    {        
        regParkDirections.style.display = 'block';
        txtparkingdirections.focus();
        return false;
   }     
    
    var txtaddcomments = document.getElementById('<%=txtAdditionalComments.ClientID%>');
    if (txtaddcomments.value != '' && txtaddcomments.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
    {        
        regAddComments.style.display = 'block';
        txtaddcomments.focus();
        return false;
    }  
    
    var txtmaplink = document.getElementById('<%=txtMapLink.ClientID%>');
    if (txtmaplink.value != '' && txtmaplink.value.search(/[^`']*/)==-1)
    {        
        regMapLink.style.display = 'block';
        txtmaplink.focus();
        return false;
    }  
   
   //ZD 100622 starts
    /*var lsttimezone = document.getElementById('<%=lstTimezone.ClientID%>');
    if(lsttimezone.value == "-1")
    {  
      
        //alert("Please select the Timezone");
        regTimeZone.style.display = 'block';
        lsttimezone.focus();
        return false;
    }   
     //ZD 100622 Ends
    
   /* var txtlongitude = document.getElementById('<%=txtLongitude.ClientID%>');
    if (txtlongitude.value != '' && txtlongitude.value.search(/^(\(|\d| |-|\))*$/)==-1)
    {        
        regLongitude.style.display = 'block';
        txtlongitude.focus();
        return false;
    }  
   
    var txtlatitude = document.getElementById('<%=txtLatitude.ClientID%>');
    if (txtlatitude.value != '' && txtlatitude.value.search(/^(\(|\d| |-|\))*$/)==-1)
    {        
        regLatitude.style.display = 'block';
        txtlatitude.focus();
        return false;
    }  */
    // dept
    // FB 2342 starts
    //FB 2342 stopped validation
//    var txtroomemail1 = document.getElementById('<%=txtRoomEmail.ClientID%>');
//    if(txtroomemail1.value == "")
//    {        
//        reqRoomEmail.style.display = 'block';
//        txtroomemail1.focus();
//        return false;
//    }
//    else 
//    {
//         var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
//         if(reg.test (txtroomemail1 .value)==false)
//         {
//                reqroomEmail1_1.style.display='block';
//                txtroomemail1.focus();
//                return false;
//         }
//    } 
    // FB 2342 end
    var cb1 = document.getElementById('<%=lstVideo.ClientID%>');
    var cb2 = document.getElementById('<%=txtEndpoint.ClientID%>');
    if ( (cb1.selectedIndex != 0) && (cb2.value == "") ){
        //alert(EN_205);
        regEndPoint.style.display = 'block';
        regEndPoint.innerText = "<asp:Literal Text='<%$ Resources:WebResources, AddaEndpointErrMsg%>' runat='server'></asp:Literal>";
        cb2.focus();
        return false;
    }
     if ( (cb1.selectedIndex <= 0) && (cb2.value != "") ) {
        //alert(EN_204);
        regVideo.style.display = 'block';
        regVideo.innerText = "<asp:Literal Text='<%$ Resources:WebResources, AddMediaErrMsg%>' runat='server'></asp:Literal>";
        cb1.focus();
        return false;
    }
    
    var hdnmultipledept = document.getElementById('<%=hdnMultipleDept.ClientID%>');
    var departmentlist = document.getElementById('<%=DepartmentList.ClientID%>');

    if (hdnmultipledept.value == 1) 
    {
        if ((departmentlist.value == "") && (departmentlist.length > 0) ) 
        {
            isConfirm = confirm(RoomProfdept)
            if (isConfirm == false)
            {
                return(false);
            }
        }
    }
    
    //FB 2400 Starts
    if(cb1.value != "0")
    {
        var RoomTele = document.getElementById('<%=DrpisTelepresence.ClientID%>')
        var EPTele = document.getElementById('<%=hdnisEPTelePresence.ClientID%>');
      
        if((RoomTele!=null ||RoomTele== "undefined")&&(EPTele!=null||EPTele== "undefined")) //ZD 100622
        {
            if(EPTele.value != RoomTele.value)
            {
                  alert(RoomProfTele)
                  return(false);
            }
        }
    }
    //FB 2400 Ends
    //ZD 101244 start
    if (document.getElementById("ChkSecure") != null && document.getElementById("ChkSecure").checked)
     document.getElementById("trSecMails").style.display = "";
     else
     document.getElementById("trSecMails").style.display = "None";
    //ZD 101244 End

    DataLoading(1);//ZD 100176
    return(true);
    
}

function fnClose()
{
    DataLoading(1);//ZD 100176 
//    window.location.replace('dispatcher/admindispatcher.asp?cmd=ManageConfRoom'); //Login management
    window.location.replace("manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=");//Login management
    
    return true;
}


function fnOpen()
{
   //var url = "manageimage.asp?p=image/room/";
   var url = "manageimage.aspx";// This code is added for ManageImage.aspx Conversion
   window.open(url, "", "width=700,height=500,top=0,left=0,resizable=yes,scrollbars=no,status=no");// This code is edited for ManageImage.aspx Conversion
   return false;
}
/*
function AddImage(imgname)
{
	addopt(document.getElementById("drpSecImgList"), imgname, imgname, true, true);
}

function DelImage(imgname)
{
	deloptbyval(document.getElementById("drpSecImgList"), imgname);
}*/

function chgEndpoint()
{
    var endPt = document.getElementById('<%=lstEndpoint.ClientID%>');
	document.getElementById('<%=btnEndpointDetails.ClientID%>').disabled = (endPt.value == "-1") ? "none" : "";
}

//Added for FB 1459  Start
//function fnRmIsEmpty()
//{
//    var ltvideo = document.getElementById('<%=lstVideo.ClientID%>');
//    if (ltvideo.value == "1" || ltvideo.value == "3") {
//        if(document.getElementById("txtroomphone").value == " ") {
//            errLabel.innerText = 'Please Enter - Valid Room Phone Number';
//            document.getElementById("txtroomphone").focus();
//            return false;
//        }
//    }
//}

//Added for FB 1459  End

//added for Endpoint Search - Start
function OpenEndpointSearch()
{

        if(OpenEndpointSearch.arguments != null)
        {
            var rmargs = OpenEndpointSearch.arguments;
            var prnt = rmargs[0];
            var isTele = document.getElementById('<%=DrpisTelepresence.ClientID%>'); //FB 2400
            var url = "";
            //FB 2694 Starts
            if(isTele != null)
                url = "EndpointSearch.aspx?frm=" + prnt + "&isRoomTel=" + isTele.value; //FB 2400
            else
                url = "EndpointSearch.aspx?frm="+prnt + "&isRoomTel=0";
           //FB 2694 Ends
            window.open(url, "EndpointSearch", "width="+ screen. availWidth +",height=666px,resizable=no,scrollbars=yes,status=no,top=0,left=0");
        }
       
    
}
function AddEndpoint()
{
  
   
}

//added for Endpoint Search - End

function toggleDiv(id,flagit) 
{
    if (flagit=="1")
    {
        if (document.layers) document.layers[''+id+''].visibility = "show"
        else if (document.all) document.all[''+id+''].style.visibility = "visible"
        else if (document.getElementById) document.getElementById(''+id+'').style.visibility = "visible"
    }
    else
        if (flagit=="0")
        {
            if (document.layers) document.layers[''+id+''].visibility = "hide"
            else if (document.all) document.all[''+id+''].style.visibility = "hidden"
            else if (document.getElementById) document.getElementById(''+id+'').style.visibility = "hidden"
        }
}

// FB 2136 Start
/*function fnOpenSecurityBadge()
{

var e = document.getElementById("drpSecImgList");
var selOption = e.options[e.selectedIndex].text;
var optionArray = selOption.split(".");
var url = "ManageSecurityBadge.aspx?drpSelOption=" + optionArray[0] + "_" + "<%=Session["organizationID"]%>" + ".jpg" ;
window.open(url, "", "width=675,height=500,top=0,left=0,resizable=no,scrollbars=no,status=no");// This code is edited for ManageImage.aspx Conversion
return false;

}*/

// >> Please refer updateImage() in ManageSecurityBadge.aspx <<
//function fnUpdateHdnField(selValue)
//{
//alert('auto' + selValue.substring(0, selValue.length-4));
//document.getElementById('hdnSelecOption').value = selValue.substring(0, selValue.length-4);
//}

// FB 2136 End
/*
function fnSecImgSelection()
{
    var e = document.getElementById("drpSecImgList");
    var selOption = e.options[e.selectedIndex].value;
    document.getElementById('hdnSecSelection').value = selOption;
}*/
//ZD 100176 start
function DataLoading(val)
{
if (val == "1")
    document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
 else
    document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100176 End

//ZD 100425 Starts
function checkFileType()
{
    if (!Page_ClientValidate())
       return Page_IsValid;
       
    clearLabel();
    var filepath=document.getElementById('roomfileimage').value;

    if(filepath == "")
    {
        alert(SelectImage);
        return false;
    }
}
function checkImageFileType()
{
    if (!Page_ClientValidate())
       return Page_IsValid;
       
    clearLabel();
    var filepath1=document.getElementById('fleMap1').value;
    var filepath2=document.getElementById('fleMap2').value;
    var filepath3=document.getElementById('fleMisc1').value;
    var filepath4=document.getElementById('fleMisc2').value;

    if(filepath1 == "" && filepath2 == "" && filepath3 == "" && filepath4 == "")
    {
        alert(SelectImage);
        return false;
    }
    return true;
}
var labelStatus = 0;
function clearLabel()
{
    if(labelStatus == 1)
    {
        if(document.getElementById('errLabel')!=null)
            document.getElementById('errLabel').innerText = "";
    }
    labelStatus=1;
}
//ZD 100425 End
    //ZD 102168  Starts
    function AddRemoveHelpReq(opr)
        {
        
        var lstHelpReq = document.getElementById("lstHelpReqEmail");
        var txtHelpReq = document.getElementById("txtHelpReqEmail");
        var hdnHelpReq = document.getElementById("hdnHelpReq");

        if (!Page_ClientValidate('grp1')) // ZD 102365
            return Page_IsValid;
	    
        if (opr == "Rem")
        {
            var i;
            for (i = lstHelpReq.options.length - 1; i >= 0; i--) {
                if (lstHelpReq.options[i].selected)
                    {
                    hdnHelpReq.value = hdnHelpReq.value.replace(lstHelpReq.options[i].text, "").replace(/;;/i, ";");
                    lstHelpReq.remove(i);
                }
            }
        }
        else if (opr == "add")
        {
            if (txtHelpReq.value.replace(/\s/g, "") == "") //trim the textbox
                return false;
	            
            if (hdnHelpReq.value.indexOf(txtHelpReq.value) >= 0) 
            {
                document.getElementById("errLabel").innerHTML = SecurityMsg;
                document.getElementById("errLabel").className = "lblError";
                return false;
            }
        
	        
        if (lstHelpReq.options.length > 0)
            hdnHelpReq.value = hdnHelpReq.value + ";";

        var option = document.createElement("Option");
        option.text = txtHelpReq.value;
        option.title = txtHelpReq.value;
        lstHelpReq.add(option);
        hdnHelpReq.value = hdnHelpReq.value + txtHelpReq.value;
	        
        txtHelpReq.value = "";
        txtHelpReq.focus();
        }

        return false;
    }
    //ZD 102168  Ends
//ZD 101244 start
function EnableValidate() {
        if (document.getElementById("ChkSecure") != null) {
            if(document.getElementById("ChkSecure").checked)
            {
                ValidatorEnable(document.getElementById("RegtxtHelpReqEmail"), true);
                ValidatorEnable(document.getElementById("RegtxtHelpReqEmail_1"), true);
                document.getElementById("txtHelpReqEmail").style.display = "";
                document.getElementById("btnAddHelpReq").style.display = "";
                document.getElementById("lstHelpReqEmail").style.display = "";
                document.getElementById("trSecMails").style.display = "";
            }
            else
            {
                ValidatorEnable(document.getElementById("RegtxtHelpReqEmail"), false);
                ValidatorEnable(document.getElementById("RegtxtHelpReqEmail_1"), false);
                document.getElementById("txtHelpReqEmail").style.display = "None";
                document.getElementById("btnAddHelpReq").style.display = "None";
                document.getElementById("lstHelpReqEmail").style.display = "None";
                document.getElementById("trSecMails").style.display = "None";
            }
       }
    }
//ZD 101244 End
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>myVRM</title>
   <script type="text/javascript"> // FB 2815
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
   </script>   
   <%--ZD 100664 Start--%>
    <script src="script/CallMonitorJquery/jquery.1.4.2.js" type="text/javascript"></script> 
    <script type="text/javascript">
     $(document).ready(function() {
           $('.treeNode').click(function() {
            var target = $(this).attr('tag');
            if ($('#' + target).is(":visible")) {
                $('#' + target).hide("slow");
                $(this).attr('src', 'image/loc/nolines_plus.gif');
            } else {
                $('#' + target).show("slow");
                
                $(this).attr('src', 'image/loc/nolines_minus.gif');
            }

        });
        });
    </script>
    <%--ZD 100664 End--%>
</head>
<body>
    <form id="frmMainroom" runat="server" onsubmit="DataLoading(1)"><%--ZD 100176--%> 
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" runat="server" id="hdnMultipleDept" />
    <input type="hidden" runat="server" id="hdnRoomID" />
    <input type="hidden" runat="server" id="hdnSelecOption" />
    <input type="hidden" runat="server" id="hdnSecSelection" />
    <input type="hidden" runat="server" id="hdnisEPTelePresence" /> <%--FB 2400--%>
    <input type="hidden" runat="server" id="hdnRoomCategory" /><%--ZD 100619--%>
    <input type="hidden" name="hdnHelpReq" id="hdnHelpReq" runat="server" /> <%--FB 102168--%>
    <%--FB 2136--%>
    <input name="hdnEPID" type="hidden" id="hdnEPID" runat="server" />
    <%--Endpoint Search--%>
    <%--Code changed for FB 1425 QA Bug -Start--%>
    <input type="hidden" id="hdntzone" runat="server" />
    <%--Code changed for FB 1425 QA Bug -End--%>
    <div>
        <center>
            <div id="dataLoadingDIV" style="display:none" align="center" >
                <img border='0' src='image/wait1.gif'  alt='Loading..' />
            </div> <%--ZD 100678 End--%>
            <h3>
                <asp:Label id="lblTitle" runat="server" cssclass="h3" text="<%$ Resources:WebResources, ManageRoomProfile_lblTitle%>"></asp:Label>
            </h3>
            <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
        </center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
            <%--table 1 starts here--%>
            <tr>
                <%--Basic Configuration--%>
                <td align="center">
                    <table id="tblBasicConfiguration" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                    <%--ZD 100664 - Start --%>
                    <tr id="trHistory" runat="server">
                        <td>
                            <table width="100%" border="0">
                                <tr>
                                    <td valign="top" align="left" style="width: 2px">
                                    <span class="blackblodtext" style="vertical-align:top"><asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, History%>" runat="server"></asp:Literal></span>
                                        <a href="#" onmouseup="if(event.keyCode == 13){this.childNodes[0].click();return false;}" onkeydown="if(event.keyCode == 13){document.getElementById('imgHistory').click();return false;}" >
                                        <img id="imgHistory" src="image/loc/nolines_plus.gif" class="treeNode" style="border:none" alt="Expand/Collapse" tag="tblHistory" /></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                    <td> 
                      <div id="tblHistory" style="display:none; width:600px; float:left">
                        <table cellpadding="4" cellspacing="0" border="0" style="border-color:Gray; border-radius:15px; background-color:#ECE9D8"  width="75%" class="tableBody" align="center" >
                            <tr>
                                <td class="subtitleblueblodtext" align="center">
                                    <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, ExpressConference_Details%>" runat="server"></asp:Literal>
                                </td>            
                            </tr>
                            <tr align="center">
                                <td align="center">
                                    
                                    <asp:DataGrid ID="dgChangedHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="true" 
                                        BorderStyle="solid" BorderWidth="0" ShowFooter="False"  
                                        Width="100%" Visible="true" style="border-collapse:separate" >
                                        <SelectedItemStyle  CssClass="tableBody"/>
                                        <AlternatingItemStyle CssClass="tableBody" />
                                        <ItemStyle CssClass="tableBody"  />                        
                                        <FooterStyle CssClass="tableBody"/>
                                        <HeaderStyle CssClass="tableHeader" />
                                        <Columns>
                                            <asp:BoundColumn DataField="ModifiedUserId"  HeaderStyle-Width="0%" Visible="false" ItemStyle-BackColor="#ECE9D8" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ModifiedDateTime" HeaderText="<%$ Resources:WebResources, Date%>" ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ModifiedUserName" HeaderText="<%$ Resources:WebResources, User%>"  ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Description" HeaderText="<%$ Resources:WebResources, Description%>"  ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                    
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
                    <%--ZD 100664 - End --%>
                        <%--<tr>
                            <td colspan="2" align="left">
                                <table id="Table4" cellpadding="2" cellspacing="2" border="0" style="width: 100%; margin-left:-20px">
                                    <tr align="left">
                                        <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_LastModifiedb%>" runat="server"></asp:Literal></td>
                                        <td style="width: 85%" align="left" valign="top">
                                            <asp:Label ID="lblMUser" runat="server" CssClass="active"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_LastModifieda%>" runat="server"></asp:Literal></td>
                                        <td style="width: 85%" align="left" valign="top">
                                            <asp:Label ID="lblMdate" runat="server" CssClass="active"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>--%><%--ZD 100664 - End --%>
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext" style="margin-left:-15px"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_BasicConfigura%>" runat="server"></asp:Literal></span>
                            </td>
                            <td class="reqfldText" align="center">
                                <asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_RequiredField%>" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table id="tblBasicConfigurationDetials" cellpadding="2" cellspacing="2" border="0"
                                    style="width: 95%">
                                    <%-- Basic Configuration Parameters starts here --%>
                                    <tr>
                                        <%--Window Dressing--%><%--FB 2579 Start--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_RoomName%>" runat="server"></asp:Literal> <span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 35%" align="left" valign="top">
                                            <%--Edited For FF & FB 2050--%>
                                            <asp:TextBox ID="txtRoomName" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtRoomName"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            <%-- Code Added for FB 1640--%>
                                            <asp:RegularExpressionValidator ID="regRoomName" ControlToValidate="txtRoomName"
                                                Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                                ErrorMessage="<%$ Resources:WebResources, InvalidCharacters35%>" 
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$"></asp:RegularExpressionValidator> <%-- ZD 103424--%> <%-- ZD 103424 ZD 104000--%>
                                            <%--FB 1888--%>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_RoomPhoneNumb%>" runat="server"></asp:Literal></td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtRoomPhone" runat="server" CssClass="altText"></asp:TextBox>
                                            <%--Edited for FB 1459--%><%--FB 2594--%>
                                            <asp:RegularExpressionValidator ID="regRoomPhone" ControlToValidate="txtRoomPhone"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters39%>"
                                                ValidationGroup="Submit" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^;?|!`\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <%--ZD 104844 - Start--%>
                                    <tr id="trCallCapacity" runat="server"> <%--FB 2694--%>                                        
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_MaximumCapacit%>" runat="server"></asp:Literal></td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox id="txtMaximumCapacity" runat="server" cssclass="altText"></asp:TextBox>
                                            <asp:RangeValidator id="CapacityValidator" runat="server" controltovalidate="txtMaximumCapacity" setfocusonerror="true" type="integer" minimumvalue="0" maximumvalue="10000" cssclass="lblError" text="<%$ Resources:WebResources, ManageRoomProfile_CapacityValidator%>"></asp:RangeValidator>
                                        </td>

                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, ManageRoomProfile_Media%>" runat="server"></asp:Literal></td>
                                        <td align="left" valign="top">
                                            <asp:DropDownList ID="lstVideo" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="<%$ Resources:WebResources, RoomSearch_MediaNone%>" Value="0" Selected="True" />
                                                <asp:ListItem Text="<%$ Resources:WebResources, RoomSearch_MediaAudio%>" Value="1" />
                                                <asp:ListItem Text="<%$ Resources:WebResources, RoomSearch_MediaVideo%>" Value="2" /> <%--ZD 100528--%>
                                                <%--FB 1744--%>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="regVideo" runat="server" ControlToValidate="lstVideo"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <%--ZD 104844 - End--%>
                                    <%--ZD 101563 Start--%>
                                    <%--<tr id="trsetupteardown" runat="server"> 
                                        
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_SetupTimeBuff%>" runat="server"></asp:Literal></td>
                                        <td style="width: 30%" align="left" valign="top">
                                            
                                            <asp:TextBox ID="txtSetupTime" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regSetupTime" ControlToValidate="txtSetupTime"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, EnterNumbersonly%>"
                                                ValidationGroup="Submit" ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                        </td>
                                        
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_TeardownTimeB%>" runat="server"></asp:Literal></td>
                                        
                                        <td style="width: 30%" align="left" valign="top">
                                            
                                            <asp:TextBox ID="txtTeardownTime" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regTeardownTime" ControlToValidate="txtTeardownTime"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, EnterNumbersonly%>"
                                                ValidationGroup="Submit" ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>--%>
                                    <%--ZD 101563 End--%>
                                    <%--ZD 104844 - Start--%>
                                    <tr>
                                        <td id="tdlblMaxConcurrentCalls" runat="server" style="width: 15%" align="left" valign="top" class="blackblodtext"><asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, ManageRoomProfile_MaximumofCon%>" runat="server"></asp:Literal></td>                                        
                                        <td id="tdMaxConcurrentCalls" runat="server" style="width: 30%" align="left" valign="top">                                        
                                            <asp:TextBox ID="txtMaxConcurrentCalls" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regMaxCall" ControlToValidate="txtMaxConcurrentCalls"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, EnterNumbersonly%>"
                                                ValidationGroup="Submit" ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                        </td>

                                        <td align="left" valign="top" class="blackblodtext">
                                            <asp:Label runat="server" ID="lblProjector"><asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ProjectorAvailable%>" runat="server"></asp:Literal></asp:Label>   <%--FB 2694 ZD 101088--%>
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:DropDownList ID="lstProjector" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="<%$ Resources:WebResources, Yes%>" Value="1" Selected="True" />
                                                <asp:ListItem Text="<%$ Resources:WebResources, No%>" Value="0" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <%--ZD 104844 - End--%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_AssistantInChar%>" runat="server"></asp:Literal><span class="reqfldText">*</span><%--FB 2974--%>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="Assistant" runat="server" CssClass="altText"></asp:TextBox>
                                            <a id="EditHref" href="javascript: getYourOwnEmailList(-2);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="edit" width="17" height="15" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>" ></a> <%--FB 2798--%>
                                            <a href="javascript: deleteAssistant();" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, Delete%>"></a> <%--FB 2798--%>
                                            <asp:RequiredFieldValidator ID="AssistantValidator" runat="server" ControlToValidate="Assistant"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Multipleassist%>" runat="server"></asp:Literal><br />
                                            (<asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, Semicolonseparated%>" runat="server"></asp:Literal>)
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtMultipleAssistant" Rows="2" TextMode="MultiLine" Width="275px"
                                                runat="server" CssClass="altText"></asp:TextBox> <%-- FB 2050 --%>
                                            <asp:RegularExpressionValidator ID="regMultipleAssistant" ControlToValidate="txtMultipleAssistant"
                                                Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                                ErrorMessage="<%$ Resources:WebResources, InvalidCharacters40%>"
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`,\[\]{}\x22=:#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <%--ZD 100619 Starts--%>
                                     <tr>
                                        <td align="left" valign="top" class="blackblodtext">
                                           <asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, ConferenceSetup_ContactEmail%>" runat="server"></asp:Literal> <span class="reqfldText">*</span>
                                        </td>
                                        <td align="left">
                                           <asp:TextBox ID="txtContactEmail" runat="server" CssClass="altText" Enabled="false"></asp:TextBox>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtContactEmail"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" valign="top" class="blackblodtext">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <%--ZD 100619 Ends--%>
                                    <tr id="trVIP" runat="server"> <%--FB 2694--%>
 										<td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                            <asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ExpressConference_ContactPhone%>" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <asp:TextBox ID="txtContactPhone" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtContactPhone"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters39%>"
                                                ValidationGroup="Submit" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^;?|!`\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%--FB 1982--%>
                                        <%--Window Dressing--%>
                                        <td align="left" valign="top" id="lblVIP" runat="server" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_VIPRoom%>" runat="server"></asp:Literal></td>
                                        <td align="left" id="tdVIP" runat="server">
                                            <asp:DropDownList ID="DrpisVIP" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="<%$ Resources:WebResources, Yes%>" Value="1" />
                                                <asp:ListItem Text="<%$ Resources:WebResources, No%>" Value="0" Selected="True" />
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" valign="top" class="blackblodtext">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <%--ZD 101244 start--%>
                                      <tr>
                                        <td class="blackblodtext" align="left" valign="top">
                                            <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, Manageroom_secureFacility%>" runat="server"></asp:Literal>
                                        </td>
                                        <td align="left" valign="top"> 
                                            <asp:CheckBox ID="ChkSecure" runat="server" onclick="javascript:EnableValidate();"/>
                                        </td>
                                        <td  class="blackblodtext" align="left" valign="top">
                                            <asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, SecEmails%>" runat="server"></asp:Literal>
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:TextBox runat="server" ID="txtsecurityemail" CssClass="altText" Enabled="false" TextMode="MultiLine" style="margin-bottom:10px" Width="275" ></asp:TextBox>
                                         </td>
                                      </tr>
                                      <tr id="trSecMails" runat="server" style="display:none">
                                      <td class="blackblodtext" align="left" valign="top">
                                            <asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, RoomSecEmails%>" runat="server"></asp:Literal>
                                        </td>
                                        <td class="blackblodtext" align="left" valign="top">
                                            <asp:TextBox ID="txtHelpReqEmail" runat="server" CssClass="altText" ValidationGroup="grp1" ></asp:TextBox> <%-- ZD 102365 --%>
                                            <asp:Button id="btnAddHelpReq" runat="server" Text="<%$ Resources:WebResources, ManageUserProfile_btnAddHelpReq%>" class="altShortBlueButtonFormat" OnClientClick="javascript:return AddRemoveHelpReq('add')" ValidationGroup="grp1" />
                                                <asp:RegularExpressionValidator ID="RegtxtHelpReqEmail" ControlToValidate="txtHelpReqEmail" Display="dynamic" runat="server" 
                                                ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" ValidationGroup="grp1"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="RegtxtHelpReqEmail_1" ControlToValidate="txtHelpReqEmail" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters14%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$" ValidationGroup="grp1"></asp:RegularExpressionValidator>                                    
                                            <br /> <br />
                                            <asp:ListBox runat="server" ID="lstHelpReqEmail" CssClass="altSelectFormat" Rows="5" SelectionMode="Multiple"  onDblClick="javascript:return AddRemoveHelpReq('Rem')"  ></asp:ListBox>
                                            <br /><span style="color: #666666; font-weight:normal"><asp:Literal ID="Literal55" Text="<%$ Resources:WebResources, mainadministrator_Doubleclickon%>" runat="server"></asp:Literal></span><%--ZD 103053--%>
                                        </td>
                                        <td align="left"></td>
                                        <td align="left"></td>
                                    </tr>
                                    <%--ZD 101244 End--%>
                                    
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_CatererFacilit%>" runat="server"></asp:Literal></td>
                                        <td align="left">
                                            <asp:DropDownList ID="CatererList" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="<%$ Resources:WebResources, Yes%>" Value="1" Selected="True" />
                                                <asp:ListItem Text="<%$ Resources:WebResources, No%>" Value="0" />
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_HandicappedAcc%>" runat="server"></asp:Literal></td>
                                        <td align="left">
                                            <asp:DropDownList ID="DrpHandi" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="<%$ Resources:WebResources, Yes%>" Value="1" />
                                                <asp:ListItem Text="<%$ Resources:WebResources, No%>" Value="0" Selected="True" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trServiceType" runat="server" > <%--FB 2694 --%>
                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_TelepresenceRo%>" runat="server"></asp:Literal></td>
                                        <td align="left">
                                            <asp:DropDownList ID="DrpisTelepresence" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="<%$ Resources:WebResources, Yes%>" Value="1" />
                                                <asp:ListItem Text="<%$ Resources:WebResources, No%>" Value="0" Selected="True" />
                                            </asp:DropDownList>
                                        </td>
                                        <%--ZD 102159--%>
                                        <td align="left" valign="Middle" class="blackblodtext" style="display:none;"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_ServiceType%>" runat="server"></asp:Literal></td>
                                        <td align="left" style="display:none;">
                                            <asp:DropDownList ID="DrpServiceType" CssClass="SelectFormat" DataTextField="Name"
                                                DataValueField="ID" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trDedication" runat="server"> <%--FB 2694--%>
                                        <%--FB 2334--%>
                                        <td align="left" height="38">
                                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_DedicatedVideo%>" runat="server"></asp:Literal></span>
                                        </td>
                                        <td align="left">
                                            <input type="checkbox" id="Chkdedicatedvideo" runat="server" />
                                        </td>
                                        <%--FB 2390--%>
                                        <td align="left" height="38"><span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_DedicatedPrese%>" runat="server"></asp:Literal></span></td>
                                        <td align="left">
                                            <input type="checkbox" id="Chkdedpresentcodec" runat="server" />
                                        </td>
                                    </tr>

                                    <%--ZD 101098 START--%>
                                     <tr id="triControlRoom" runat="server"> 
                                        <td align="left" height="38">
                                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_iControlRoom%>" runat="server"></asp:Literal></span>
                                        </td>
                                        <td align="left">
                                            <input type="checkbox" id="chkiControlRoom" runat="server" />
                                        </td>
                                        <td align="left" height="38"></td>
                                        <td align="left">
                                        </td>
                                    </tr>
                                    <%--ZD 101098 END--%>
                                    
                                    
                                    <%--FB 2415 Start--%>
                                    <tr id="trAVEmail" runat="server"> <%--FB 2694--%>
                                        <td align="left" height="38">
                                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_AVOnsitesuppo%>" runat="server"></asp:Literal></span>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtAVOnsiteSupportEmail" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regEmail1_1" ControlToValidate="txtAVOnsiteSupportEmail"
                                                Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="regEmail1_2" ControlToValidate="txtAVOnsiteSupportEmail"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters14%>"
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td colspan="2">
                                        </td>
                                    </tr>
                                    <%--FB 2415 End--%>
                                </table>
                                <%-- Basic Configuration Parameters end here --%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--Location--%>
                <td align="center">
                    <table id="tblLocation" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                        <tr>
                            <%--Window Dressing--%>
                            <td align="left">
                                <span class="subtitleblueblodtext" style="margin-left:-15px"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Location%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="tblLocationDetails" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <%-- Basic Configuration Parameters starts here --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 19%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_TopTier%>" runat="server"></asp:Literal><span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 34%" align="left" valign="top"><%-- FB 2694--%>
                                            <%--Edited For FF & FB 2050--%>
                                            <asp:DropDownList ID="lstTopTier" DataTextField="Name" DataValueField="ID" runat="server"
                                                CssClass="altSelectFormat" OnSelectedIndexChanged="UpdateMiddleTiers" OnTextChanged="UpdateSecuredetails" AutoPostBack="true"
                                                onchange="javascript:DataLoading(1)">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqTopTier" runat="server" ControlToValidate="lstTopTier"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 21%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_MiddleTier%>" runat="server"></asp:Literal><span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:DropDownList ID="lstMiddleTier" DataTextField="Name" DataValueField="ID" runat="server" OnSelectedIndexChanged="UpdateCheckSecuredetails" AutoPostBack="true"
                                                CssClass="altSelectFormat">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqMiddleTier" runat="server" ControlToValidate="lstMiddleTier"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 16%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Floor%>" runat="server"></asp:Literal></td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtFloor" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Room%>" runat="server"></asp:Literal></td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtRoomNumber" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_StreetAddress%>" runat="server"></asp:Literal></td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtStreetAddress1" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_StreetAddress2%>" runat="server"></asp:Literal></td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtStreetAddress2" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_City%>" runat="server"></asp:Literal></td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <td style="width:20%" ><%--FB 2657 UI Modification Start--%>
                                            <table cellpadding="2"  style="width:100%; border-collapse:collapse; text-align:left; vertical-align:top; margin-left:-1px" ><%-- FB 2694--%>
                                                <tr>
                                                    <td align="left"><%-- FB 2694--%>
                                                        <span class="blackblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Country%>" runat="server"></asp:Literal></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left"><%-- FB 2694--%>
                                                        <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_StateProvince%>" runat="server"></asp:Literal><%--FB 2657--%>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 30%">
                                            <table cellpadding="2" style="width: 100%; border-collapse: collapse; text-align: left">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="lstCountries" CssClass="altText" runat="server" DataTextField="Name"
                                                            DataValueField="ID" OnSelectedIndexChanged="UpdateStates" AutoPostBack="true"
                                                            Width="275px">
                                                            <%-- FB 2050 --%>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="lstStates" CssClass="altText" Width="110" runat="server" DataTextField="Code"
                                                            DataValueField="ID">
                                                            <%--FB 2657--%>
                                                        </asp:DropDownList>
                                                        &nbsp&nbsp<b><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_PostalCode%>" runat="server"></asp:Literal></b><%--FB 2657--%>
                                                        <asp:TextBox ID="txtZipCode" Width="50" runat="server" CssClass="altText"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="regZipCode" ControlToValidate="txtZipCode" Display="dynamic"
                                                            runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters38%>"
                                                            ValidationGroup="Submit" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@#$%&'~]*$"></asp:RegularExpressionValidator>
                                                        <%--FB 2222--%><%--FB 2657 UI Modification End--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <%--FB 2342 starts--%>
                                    <tr>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_RoomEmailQueu%>" runat="server"></asp:Literal>
                                        </td>
                                        <td align="left" valign="top" height="10" style="width: 50">
                                            <asp:TextBox ID="txtRoomEmail" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqRoomEmail" ControlToValidate="txtRoomEmail" Display="dynamic"
                                                ErrorMessage="<B><%$ Resources:WebResources, Required%>" runat="server" Enabled="false" CssClass="lblError"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="reqroomEmail1_1" ValidationGroup="Submit" ControlToValidate="txtRoomEmail"
                                                Display="dynamic" runat="server" Enabled="false" ErrorMessage="<%$ Resources:WebResources, Invalidemail%>"
                                                ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
                                                SetFocusOnError="true" CssClass="lblError"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="reqroomEmail1_2" ControlToValidate="txtRoomEmail"
                                                ValidationGroup="Submit" Display="dynamic" runat="server" Enabled="false" SetFocusOnError="true"
                                                ErrorMessage="<%$ Resources:WebResources, InvalidCharacters14%>"
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$" CssClass="lblError"></asp:RegularExpressionValidator>
                                        </td>
                                          <%--ZD 100196 Start--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_RoomUID%>" runat="server"></asp:Literal></td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <asp:TextBox ID="txtRoomUID" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <%--FB 2724 End--%>
                                    </tr>
                                    <%--FB 2342 end--%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_ParkingDirecti%>" runat="server"></asp:Literal></td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtParkingDirections" TextMode="MultiLine" Rows="2" Width="275"
                                                runat="server" CssClass="altText"></asp:TextBox>
                                            <%-- FB 2050 --%>
                                            <asp:RegularExpressionValidator ID="regParkDirections" ControlToValidate="txtParkingDirections"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters41%>"
                                                ValidationGroup="Submit" ValidationExpression="[^`']*"></asp:RegularExpressionValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_AdditionalComm%>" runat="server"></asp:Literal></td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtAdditionalComments" TextMode="MultiLine" Rows="2" Width="275"
                                                runat="server" CssClass="altText"></asp:TextBox>
                                            <%-- FB 2050 --%>
                                            <asp:RegularExpressionValidator ID="regAddComments" ControlToValidate="txtAdditionalComments"
                                                Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                                ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_MapLink%>" runat="server"></asp:Literal></td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtMapLink" TextMode="MultiLine" Rows="2" Width="275" runat="server"
                                                CssClass="altText"></asp:TextBox>
                                            <%-- FB 2050 --%>
                                            <asp:RegularExpressionValidator ID="regMapLink" ControlToValidate="txtMapLink" Display="dynamic"
                                                runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters41%>"
                                                ValidationGroup="Submit" ValidationExpression="[^`']*"></asp:RegularExpressionValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <%--Code changed for FB 1425 QA Bug -Start--%>
                                        <td style="width: 20%" align="left" valign="top" id="TzTD1" runat="server" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_TzTD1%>" runat="server"></asp:Literal><span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top" id="TzTD2" runat="server">
                                            <%--Edited For FF--%>
                                            <asp:DropDownList ID="lstTimezone" runat="server" CssClass="altSelectFormat" DataTextField="timezoneName"
                                                DataValueField="timezoneID">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="regTimeZone"  ValidationGroup="Submit" CssClass="lblError" ControlToValidate="lstTimeZone" ErrorMessage="<%$ Resources:WebResources, Required%>" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator> <%--ZD 100622--%>
                                           <%-- <asp:RequiredFieldValidator ID="regTimeZone" runat="server" ControlToValidate="lstTimezone"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Required"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <%--Code changed for FB 1425 QA Bug -End--%>
                                    </tr>
                                    <tr id="trLatitude" runat="server"> <%--FB 2694--%>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Latitude%>" runat="server"></asp:Literal></td>
                                        <%--Edited For FF and FB 2050 --%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtLatitude" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Longitude%>" runat="server"></asp:Literal></td>
                                        <%-- FB 2050 --%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtLongitude" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <%-- Location Parameters end here --%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--Images--%>
                <td align="center">
                    <table id="tblImages" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="left" colspan="4">
                                <span class="subtitleblueblodtext" style="margin-left:-20px"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Images%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <%--Window Dressing--%>
                            <%--Window Dressing--%>
                            <td align="left" valign="top" class="blackblodtext" style="display: none">
                                Room Image
                                <br />
                                <i style="font-size: xx-small">Use CTRL key to select multiple images.</i>
                            </td>
                            <td align="left" valign="top" style="display: none">
                                <asp:ListBox ID="lstRoomImage" runat="server" CssClass="SelectFormat" SelectionMode="multiple">
                                </asp:ListBox>
                                <%--code added for Soft Edge button--%>
                                <input type="button" name="Edit" class="altShortBlueButtonFormat" onclick="javascript:fnOpen()"
                                    style="width: 30pt" value="Edit" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4">
                                <%-- <asp:UpdatePanel ID="RoomImgUpdatePanel" runat="server"  UpdateMode="Always" RenderMode="Inline" >
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="BtnUploadRmImg"  />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:Panel  id="RoomImagePanel" runat="server">--%>
                                <table align="left" cellpadding="2" cellspacing="0" border="0" style="width:95%"> <!-- FB 2050 -->
                                    <!-- FB 2050 -->
                                    <tr id="trDynamicRoomLayout" runat="server"> <%--FB 2694--%>
                                        <td align="left" style="width: 25%" valign="top" class="blackblodtext">
                                            <!-- FB 2050 -->
                                            <asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_DynamicRoomLa%>" runat="server"></asp:Literal>
                                        </td>
                                        <td align="left" style="width: 75%" valign="top">
                                            <!-- FB 2050 -->
                                            <asp:DropDownList ID="lstDynamicRoomLayout" runat="server" CssClass="SelectFormat"
                                                Width="175px">
                                                <%-- FB 2050 --%>
                                                <asp:ListItem Text="<%$ Resources:WebResources, Enable%>" Value="1" Selected="True" />
                                                <asp:ListItem Text="<%$ Resources:WebResources, Disable%>" Value="0" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                     <%--FB 2694--%>
                                     <%--FB 2050--%>
                                     <%--FB 2136 Start--%>
									 <%--ZD 100420--%>
									 <%--ZD 101271-Commented--%>
                                    <%--<tr id="trSecurityImage" runat="server" style="display:none">
                                        <td align="left" class="blackblodtext" valign="top">
                                            <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ManageRoomProfile_SecurityImage%>" runat="server"></asp:Literal>
                                        </td>
                                        <td colspan="3">
                                            <asp:DropDownList ID="drpSecImgList" runat="server" DataTextField="badgename" DataValueField="badgeid"
                                                CssClass="SelectFormat" Width="175px" EnableViewState="true" onchange="javascript:fnSecImgSelection();">
                                            </asp:DropDownList>
                                                <button id="btnMngSecImg" class="altMedium0BlueButtonFormat" onclick="javascript:return fnOpenSecurityBadge()" >
                                                <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ConferenceList_btnManage%>" runat="server"></asp:Literal></button>
                                            <br />
                                            <br />
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td align="left" style="width:25%" valign="top" class="blackblodtext"><%--FB 2694--%>
                                            <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ManageRoomProfile_RoomImage%>" runat="server"></asp:Literal>
                                            &nbsp;<span class="blackItalictext"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ManageRoomProfile_250x250pixels%>" runat="server"></asp:Literal></span> <%--ZD 101611--%>
                                        </td>
                                        <td colspan="3" align="left"> <%--FB 2909 Start--%>
                                        <%--ZD 102277 Start--%>
                                        <table border="0" width="100%" style="border-collapse:collapse">
                                            <tr>
                                                <td> 
                                                     <div>
                                                     <input id="Text1" type="text" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' runat="server">
                                                     <div class="file_input_div"><input id="Button1" type="button" runat="server" value="<%$ Resources:WebResources, Browse%>" class="file_input_button" onclick="document.getElementById('roomfileimage').click();return false;"  />  <%--accept="image/*" --%>
											            
                                                        <input type="file" class="file_input_hidden" id="roomfileimage" accept="image/*" tabindex="-1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,1);"/>
                                                        </div></div>
                                                        
                                                <%--</td>--%>
                                                <%--<td align="left">--%>
                                                     <button ID="BtnUploadRmImg" Class="altLongBlueButtonFormat" runat="server"  style="margin-left:52px;width:250px;"
                                                 onserverclick="UploadRoomImage"  ValidationGroup="Submit1" onclick="javascript:return fnUpload2(document.getElementById('roomfileimage').value, this.id, 'Submit1');" >
                                                 <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, ManageRoomProfile_BtnUploadRmImg%>" runat="server"></asp:Literal></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RegularExpressionValidator ID="regFNVal1" ControlToValidate="roomfileimage"
                                                    Display="dynamic" runat="server" ValidationGroup="Submit1" SetFocusOnError="true"
                                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters45%>"
                                                    ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                    <asp:RegularExpressionValidator ID="regRoomfileimage" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="roomfileimage" CssClass="lblError" ErrorMessage="<%$ Resources:WebResources, Filetypeinvalid%>" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                        <%--ZD 102277 End--%>
                                         <%--FB 3055-2ndIssue--%>
                                             
                                           <%-- <input type="file" id="roomfileimage" contenteditable="false" enableviewstate="true"
                                                size="50" class="altText" runat="server" />--%>  <%--FB 2909 End--%>
											<%--ZD 100420--%>
                                            <%--<asp:Button ID="BtnUploadRmImg" CssClass="altLongBlueButtonFormat" runat="server"  style="margin-left:2px"
                                                Text="Upload Room Image" OnClick="UploadRoomImage"  ValidationGroup="Submit1"  OnClientClick="DataLoading(1)"/> --%><%--FB 2909 End--%> <%--ZD 100176--%> 
                                                
											<%--ZD 100420--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                         <%-- FB 2136 Starts --%>
                                        <td align="left" colspan = "3"> 
                                           <div style="overflow-y: hidden; overflow-x: auto; height: auto; width: 600px;">
                                                <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgItems"
                                                    AutoGenerateColumns="false" OnItemCreated="BindRowsDeleteMessage" OnDeleteCommand="RemoveImage"
                                                    runat="server" Width="70%" GridLines="None" Visible="false" Style="border-collapse: separate">
                                                    <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Center" />
                                                    <AlternatingItemStyle CssClass="tableBody" />
                                                    <ItemStyle CssClass="tableBody" />
                                                    <FooterStyle CssClass="tableBody" />
                                                    <Columns>
                                                        <asp:BoundColumn DataField="ImageName" Visible="true" HeaderText="<%$ Resources:WebResources, Name%>" HeaderStyle-CssClass="tableHeader"
                                                            ItemStyle-Width="20%"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Image" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Imagetype" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ImagePath" Visible="false"></asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, EditInventory_tdHImage%>" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="40%"
                                                            ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <asp:Image ID="itemImage" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>'
                                                                    Width="30" Height="30" runat="server" AlternateText="<%$ Resources:WebResources, ManageVirtualMeetingRoom_RoomImage%>" /> <%--ZD 100419--%>
                                                             </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnDelete" Text="<%$ Resources:WebResources, ManageRoomProfile_btnDelete%>" CommandName="Delete" runat="server"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid><br />
                                            </div>
                                            <%-- FB 2136 Ends --%>
                                        </td>
                                    </tr>
                                </table>
                                <%-- </asp:Panel>
                                     </ContentTemplate>
                                </asp:UpdatePanel>--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4">
                                <table align="left" id="tblImagesDetails" cellpadding="2" cellspacing="0" border="0"
                                    style="width: 95%"> <%--FB 2136--%>
                                    <%-- Images Parameters starts here & FB 2050 --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%; padding-top:6px" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Map1%>" runat="server"></asp:Literal>&nbsp;<span class="blackItalictext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_250x250pixels%>" runat="server"></asp:Literal></span></td><%--ZD 102700--%>
                                        <td style="width: 85%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                      <%--<input type="file" id="fleMap1" contenteditable="false" enableviewstate="true" size="50"
                                                            class="altText" runat="server" />--%> <%--FB 2909 Start--%>
                                                            <div>
                                                            <input type="text" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' runat="server"/>
                                                            <div class="file_input_div"><input type="button" runat="server" value="<%$ Resources:WebResources, Browse%>" class="file_input_button" onclick="document.getElementById('fleMap1').click();return false;"  />
                                                            <input type="file"  class="file_input_hidden" id="fleMap1" accept="image/*" tabindex="-1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,2);"/></div></div> <%--FB 2909 End--%>  <%--FB 3055-2ndIssue--%>
                                                            <%--ZD 102700 - Start--%>
                                                            &nbsp;&nbsp;
                                                            <cc1:ImageControl ID="Map1ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                            </cc1:ImageControl>
                                                            <asp:Label ID="lblUploadMap1" Text="" Visible="false" runat="server"></asp:Label>&nbsp;
                                                            <asp:Button ID="btnRemoveMap1" CssClass="altShortBlueButtonFormat" Text="<%$ Resources:WebResources, ManageRoomProfile_btnRemoveMap1%>"
                                                                Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="1" Width="80px" />
                                                            <%--ZD 102700 - End--%>
                                                            <asp:RegularExpressionValidator ID="regfleMap1" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="fleMap1" CssClass="lblError" ErrorMessage="<%$ Resources:WebResources, Filetypeinvalid%>" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                                            &nbsp;&nbsp;
                                                            <div>
                                                            <asp:RegularExpressionValidator ID="regFNVal2" ControlToValidate="fleMap1" Display="dynamic" runat="server" ValidationGroup="Submit1" 
                                                                SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters45%>" ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                            </div>
                                                        
                                                        <asp:Label ID="hdnUploadMap1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Map1ImageDt" name="Map1ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%;padding-top:6px" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Map2%>" runat="server"></asp:Literal>&nbsp;<span class="blackItalictext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_250x250pixels%>" runat="server"></asp:Literal></span></td><%--ZD 102700--%>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <%--<input type="file" id="fleMap2" contenteditable="false" enableviewstate="true" size="50"
                                                            class="altText" runat="server" />--%> <%--FB 2909 Start--%>
                                                            <div>
                                                            <input type="text" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' runat="server"/>
                                                            <div class="file_input_div"><input type="button" runat="server" value="<%$ Resources:WebResources, Browse%>" class="file_input_button"  onclick="document.getElementById('fleMap2').click();return false;"  />
                                                            <input type="file"  class="file_input_hidden" id="fleMap2" accept="image/*" tabindex="-1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,3);"/></div></div> <%--FB 2909 End--%> <%--FB 3055-2ndIssue--%>
                                                            <%--ZD 102700 Start--%>
                                                            &nbsp;&nbsp;
                                                            <cc1:ImageControl ID="Map2ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                            </cc1:ImageControl>
                                                            <asp:Label ID="lblUploadMap2" Text="" Visible="false" runat="server"></asp:Label>&nbsp;
                                                            <asp:Button ID="btnRemoveMap2" CssClass="altShortBlueButtonFormat" Text="<%$ Resources:WebResources, ManageRoomProfile_btnRemoveMap1%>"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="2" Width="80px" />
                                                            <%--ZD 102700 End--%>
                                                            <asp:RegularExpressionValidator ID="regfleMap2" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="fleMap2" CssClass="lblError" ErrorMessage="<%$ Resources:WebResources, Filetypeinvalid%>" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                                            &nbsp;&nbsp;
                                                            <div>
                                                            <asp:RegularExpressionValidator ID="regFNVal3" ControlToValidate="fleMap2" Display="dynamic" runat="server" ValidationGroup="Submit1" 
                                                                SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters45%>" ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                            </div>
                                                        
                                                        <asp:Label ID="hdnUploadMap2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Map2ImageDt" name="Map2ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                        <%--FB 2136--%>
                                    <%--<tr style="display: none">
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Security1%>" runat="server"></asp:Literal></td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="file" id="fleSecurity1" contenteditable="false" enableviewstate="true"
                                                            size="50" class="altText" runat="server" />
                                                        <cc1:ImageControl ID="Sec1ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadSecurity1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveSecurity1" CssClass="altShortBlueButtonFormat" Text="<%$ Resources:WebResources, ManageRoomProfile_btnRemoveMap1%>"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="3" />
                                                        <asp:Label ID="hdnUploadSecurity1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Sec1ImageDt" name="Sec1ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>--%>
                                        <%--FB 2136--%>
                                    <%--<tr style="display: none">
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Security2%>" runat="server"></asp:Literal></td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="file" id="fleSecurity2" contenteditable="false" enableviewstate="true"
                                                            size="50" class="altText" runat="server" />
                                                        <cc1:ImageControl ID="Sec2ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadSecurity2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveSecurity2" CssClass="altShortBlueButtonFormat" Text="<%$ Resources:WebResources, ManageRoomProfile_btnRemoveMap1%>"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="4" />
                                                        <asp:Label ID="hdnUploadSecurity2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Sec2ImageDt" name="Sec2ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>--%>
                                    <tr id="trMisc1" runat="server"> <%--FB 2694--%>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%;padding-top:6px" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Misc1%>" runat="server"></asp:Literal>&nbsp;<span class="blackItalictext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_250x250pixels%>" runat="server"></asp:Literal></span></td><%--ZD 102700--%>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <%--<input type="file" id="fleMisc1" contenteditable="false" enableviewstate="true" size="50"
                                                            class="altText" runat="server" />--%> <%--FB 2909 Start--%>
                                                            <div>
                                                            <input type="text" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' runat="server"/>
                                                            <div class="file_input_div"><input type="button" runat="server" value="<%$ Resources:WebResources, Browse%>" class="file_input_button"  onclick="document.getElementById('fleMisc1').click();return false;"  />
                                                            <input type="file"  class="file_input_hidden" id="fleMisc1" accept="image/*" tabindex="-1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,4);"/></div></div><%--FB 2909 End--%> <%--FB 3055-2ndIssue--%>
                                                            <%--ZD 102700 Start--%>
                                                            &nbsp;&nbsp;
                                                            <cc1:ImageControl ID="Misc1ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                            </cc1:ImageControl>
                                                            <asp:Label ID="lblUploadMisc1" Text="" Visible="false" runat="server"></asp:Label>&nbsp;
                                                            <asp:Button ID="btnRemoveMisc1" CssClass="altShortBlueButtonFormat" Text="<%$ Resources:WebResources, ManageRoomProfile_btnRemoveMap1%>"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="5"  Width="80px" />
                                                            <%--ZD 102700 End--%>
                                                            <asp:RegularExpressionValidator ID="regfleMisc1" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="fleMisc1" CssClass="lblError" ErrorMessage="<%$ Resources:WebResources, Filetypeinvalid%>" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                                            &nbsp;&nbsp;
                                                            <div>
                                                            <asp:RegularExpressionValidator ID="regFNVal4" ControlToValidate="fleMisc1" Display="dynamic" runat="server" ValidationGroup="Submit1" 
                                                                SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters45%>" ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                            </div>
                                                        
                                                        <asp:Label ID="hdnUploadMisc1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Misc1ImageDt" name="Misc1ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trMisc2" runat="server"> <%--FB 2694--%>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%;padding-top:6px" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Misc2%>" runat="server"></asp:Literal>&nbsp;<span class="blackItalictext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_250x250pixels%>" runat="server"></asp:Literal></span></td><%--ZD 102700--%>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <%--<input type="file" id="fleMisc2" contenteditable="false" enableviewstate="true" size="50"
                                                            class="altText" runat="server" />--%><%--FB 2909 Start--%>
                                                            <div>
                                                            <input type="text" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' runat="server">
                                                            <div class="file_input_div"><input type="button" runat="server" value="<%$ Resources:WebResources, Browse%>" class="file_input_button"  onclick="document.getElementById('fleMisc2').click();return false;"  />
                                                            <input type="file"  class="file_input_hidden" id="fleMisc2" accept="image/*" tabindex="-1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,5);"/></div></div><%--FB 2909 End--%> <%--FB 3055-2ndIssue--%>
                                                            <%--ZD 102700 Start--%>
                                                            &nbsp;&nbsp;
                                                            <cc1:ImageControl ID="Misc2ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                            </cc1:ImageControl>
                                                            <asp:Label ID="lblUploadMisc2" Text="" Visible="false" runat="server"></asp:Label>&nbsp;
                                                            <asp:Button ID="btnRemoveMisc2" CssClass="altShortBlueButtonFormat" Text="<%$ Resources:WebResources, ManageRoomProfile_btnRemoveMap1%>"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="6"  Width="80px"/>
                                                            <%--ZD 102700 End--%>
                                                            <asp:RegularExpressionValidator ID="regfleMisc2" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="fleMisc2" CssClass="lblError" ErrorMessage="<%$ Resources:WebResources, Filetypeinvalid%>" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                                            &nbsp;&nbsp;
                                                            <div>
                                                            <asp:RegularExpressionValidator ID="regFNVal5" ControlToValidate="fleMisc2" Display="dynamic" runat="server" ValidationGroup="Submit1" 
                                                                SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters45%>" ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                            </div><%--ZD 102277 End--%>
                                                        
                                                        <asp:Label ID="hdnUploadMisc2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Misc2ImageDt" name="Misc2ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <%-- Images Parameters ends here --%>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <%--<asp:Button ID="btnUploadFiles" OnClick="UploadFiles" runat="server" Text="Upload Images" CssClass="altLongBlueButtonFormat" />--%><%-- FB 3055 />--%>
								<%--ZD 100420--%>
                                <%--<asp:Button ID="btnUploadImages" OnClick="UploadOtherImages" runat="server" Text="Upload Images" ValidationGroup="Submit1" OnClientClick="DataLoading(1)"
                                    CssClass="altLongBlueButtonFormat" />--%><%--ZD 100176--%> 
                                    <button ID="btnUploadImages" onserverclick="UploadOtherImages" runat="server" ValidationGroup="Submit1" onclick="javascript:return fnUpload4(document.getElementById('fleMap1'), document.getElementById('fleMap2'), document.getElementById('fleMisc1'), document.getElementById('fleMisc2'), this.id, 'Submit1');"
                                    Class="altLongBlueButtonFormat">
									<asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_btnUploadImages%>" runat="server"></asp:Literal></button> 
								<%--ZD 100420--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--Approvers--%>
                <td align="center">
                    <table id="tblApprovers" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext" runat="server" id="spnConf" style="margin-left:-20px"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_spnConf%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table id="Table1" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <%-- Images Parameters starts here --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_PrimaryApprove%>" runat="server"></asp:Literal></td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <asp:TextBox ID="Approver0" CssClass="altText" runat="server" />
                                            <a href="javascript: getYourOwnEmailList(0);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="edit" width="17" height="15" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>" ></a> <%--FB 2798--%>
                                            <a href="javascript: deleteApprover(0);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, Delete%>"></a> <%--FB 2798--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_SecondaryAppro%>" runat="server"></asp:Literal></td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <asp:TextBox ID="Approver1" CssClass="altText" runat="server" />
                                            <!-- Code Modified by Offshore FB # 412 Start(Changed getYourOwnEmailList(0) and deleteApprover() to getYourOwnEmailList(1))  -->
                                            <a href="javascript: getYourOwnEmailList(1);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="edit" width="17" height="15" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>"/></a> <%--FB 2798--%>
                                            <a href="javascript: deleteApprover(1);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16"  style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, Delete%>"></a> <%--FB 2798--%>
                                            <!-- Code Modified by Offshore FB # 412 End  -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_SecondaryAppro2%>" runat="server"></asp:Literal></td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <asp:TextBox ID="Approver2" CssClass="altText" runat="server" />
                                            <!-- Code Modified by Offshore FB # 412 Start(Changed getYourOwnEmailList(0) to getYourOwnEmailList(2))  -->
                                            <a href="javascript: getYourOwnEmailList(2);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="edit" width="17" height="15" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>"></a> <%--FB 2798--%>
                                            <a href="javascript: deleteApprover(2);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, Delete%>"></a> <%--FB 2798--%>
                                            <!-- Code Modified by Offshore FB # 412 Start(Changed getYourOwnEmailList(0) to getYourOwnEmailList(2))  -->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--Endpoint--%>
                <td align="center">
                    <table id="tblEndpoint" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext" style="margin-left:-20px"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_EndpointAssign%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table id="Table2" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <%-- Images Parameters starts here --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <%--Endpoint Search--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Endpoint%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtEndpoint" CssClass="altText" Width="40%" ReadOnly="true"></asp:TextBox>
                                            <a href="javascript: deleteEndpoint();" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, Delete%>"></a> <%--FB 2798--%>
                                            <%--<asp:Button Text="Add Endpoint" class="altMedium0BlueButtonFormat" OnClientClick="javascript:OpenEndpointSearch('frmMainroom');"
                                                OnClick="BindEndpoint" runat="server" ID="addEndpoint" Width="180px" />--%>  
                                            <button type="button" class="altMedium0BlueButtonFormat" 
                                               onserverclick="BindEndpoint" runat="server" ID="addEndpoint" style="display:none" >
											Add Endpoint</button>                                              
                                            <button type="button" name="opnEndpoint" id="opnEndpoint" onclick="javascript:return OpenEndpointSearch('frmMainroom'); return false;"
                                                class="altMedium0BlueButtonFormat" style="width:180px;" >
												<asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_addEndpoint%>" runat="server"></asp:Literal></button>
                                            <button name="addEndpoint" id="addEndpoint1" onclick="javascript:AddEndpoint();"
                                                style="display: none;" ></button><br />
                                            <asp:RequiredFieldValidator ID="regEndPoint" runat="server" ControlToValidate="lstEndpoint"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="width: 75%; display: none" align="left" valign="top">
                                            <%--Edited for FF--%>
                                            <asp:DropDownList ID="lstEndpoint" runat="server" CssClass="altSelectFormat" DataTextField="Name"
                                                DataValueField="ID" onChange="chgEndpoint();">
                                                <asp:ListItem Text="Please Select..." Value="-1"></asp:ListItem>
                                                <%--Code added for FB 1257--%>
                                            </asp:DropDownList>
                                            <asp:Button ID="btnEndpointDetails" CssClass="altShortBlueButtonFormat" Text="<%$ Resources:WebResources, ManageRoomProfile_btnEndpointDetails%>"
                                                runat="server" OnClientClick="ViewEndpointDetails();return false;" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%if (hdnMultipleDept.Value == "1")
              {
            %>
            <tr>
                <td align="center">
                    <table cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext" style="margin-left:-20px"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_Department%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr align="left">
                            <td align="left">
                                <table id="Table3" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <%-- Images Parameters starts here --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td width="25%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_RoomsDepartmen%>" runat="server"></asp:Literal></td><%--FB 2579 End--%>
                                        <td style="width: 75%" align="left" valign="top">
                                            <asp:ListBox ID="DepartmentList" runat="server" CssClass="altText" DataTextField="Name"
                                                DataValueField="ID" SelectionMode="multiple"></asp:ListBox>
                                             <%--ZD 100422 Start--%>
                                     <span style='color: #666666;'>* <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, SelectMultiDept%>" runat="server"></asp:Literal></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
            <tr>
                <td style="height: 60px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table id="tblButtons" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="center" style="width: 33%">
								<%--ZD 100420--%>
                                <button ID="btnReset" runat="server" Class="altLongBlueButtonFormat" onclick="javascript:DataLoading('1');"
                                    onserverclick="ResetRoomProfile" >
								<asp:Literal Text="<%$ Resources:WebResources, Reset%>" runat="server"></asp:Literal></button><%--ZD 100176--%> 
								<%--ZD 100420--%>
                            </td>
                            <td align="center" style="width: 33%">
                                <%--code added for Soft Edge button--%>
                                <input name="Go" type="button" id="btnGoBack"  runat="server" class="altLongBlueButtonFormat" onclick="javascript:fnClose();"
                                    value="<%$ Resources:WebResources, ManageTier2_btnGoBack%>" />
                            </td>
                            <td align="center" style="width: 33%">
								<%--ZD 100420--%>
                            <asp:Button ID="btnSubmitAddNew" runat="server" ValidationGroup="Submit" Text="<%$ Resources:WebResources, ManageRoomProfile_btnSubmitAddNew%>"
                                      Width="150pt" OnClick="SubmitAddNewRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" /><%-- FB 2796--%>
                                 <%--<button ID="btnSubmitAddNew" runat="server" ValidationGroup="Submit" style="width:150pt"
                                    onserverclick="SubmitAddNewRoomProfile" onclick="return frmMainroom_Validator();" >Submit / New Room</button>--%>
                               <%--ZD 100420--%>       
                            </td>
                            <td align="center" style="width: 33%">
								<%--ZD 100420--%>
                                <asp:Button ID="btnSubmit" ValidationGroup="Submit" runat="server" Text="<%$ Resources:WebResources, Submit%>"
                                   Width="150pt" OnClick="SubmitRoomProfile" OnClientClick="javascript:return frmMainroom_Validator();fnTimezonecheck();" /><%-- FB 2796--%>
                                   <%--<button ID="btnSubmit" ValidationGroup="Submit" runat="server" style="width:150pt"
                                   onserverclick="SubmitRoomProfile" onclick="return frmMainroom_Validator();">Submit</button> --%>
								<%--ZD 100420--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <%--table 1 ends here--%>
        <input type="hidden" id="Approver0ID" runat="server" />
        <input type="hidden" id="Approver1ID" runat="server" />
        <input type="hidden" id="Approver2ID" runat="server" />
        <input type="hidden" id="AssistantID" runat="server" />
        <input type="hidden" id="AssistantName" runat="server" />
        <input type="hidden" id="AssistantEmail" runat="server" /> <%--ZD 100619--%>
    </div>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<%--FB 2694 Start--%>
<%--<script type="text/javascript">
if("<%=Session["isVIP"]%>" !=null)
    if("<%=Session["isVIP"]%>" == "0")//FB 1982
    {
        document.getElementById("trVIP").style.display = "none"; 
    }
</script>--%>
<%--FB 2694 End--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--ZD 100419 start--%>
<script type="text/javascript">

// ZD 102365 Start
if (document.getElementById("hdnHelpReq").value != "") {
    document.getElementById("ChkSecure").checked = true;
    var vals = document.getElementById("hdnHelpReq").value.split(';');
    for(var i=0; i<vals.length; i++)
    {
        var option = document.createElement("Option");
        option.text = vals[i];
        option.title = vals[i];
        document.getElementById("lstHelpReqEmail").add(option);
    }
}
// ZD 102365 End

EnableValidate();

document.onkeydown = function(evt) {
    evt = evt || window.event;
    var keyCode = evt.keyCode;
    if (keyCode == 8) {
        if (document.getElementById("btnCancel") != null) { // backspace
            var str = document.activeElement.type;
            if (!(str == "text" || str == "textarea" || str == "password")) {
                document.getElementById("btnCancel").click();
                return false;
            }
        }
        if (document.getElementById("btnGoBack") != null) { // backspace
            var str = document.activeElement.type;
            if (!(str == "text" || str == "textarea" || str == "password")) {
                document.getElementById("btnGoBack").click();
                return false;
            }
        }
    }
    fnOnKeyDown(evt);
};

if(document.getElementById("Map1ImageCtrl")!=null)
    document.getElementById("Map1ImageCtrl").setAttribute("alt", "Image Map1");
if (document.getElementById("Map2ImageCtrl") != null)
    document.getElementById("Map2ImageCtrl").setAttribute("alt", "Image Map2");
if (document.getElementById("Misc1ImageCtrl") != null)
    document.getElementById("Misc1ImageCtrl").setAttribute("alt", "Misc Image1");
if (document.getElementById("Misc2ImageCtrl") != null)
    document.getElementById("Misc2ImageCtrl").setAttribute("alt", "Misc Image1");
//ZD 101271
//if (document.getElementById("Sec1ImageCtrl") != null)
//    document.getElementById("Sec1ImageCtrl").setAttribute("alt", "Security Image1");
//if (document.getElementById("Sec2ImageCtrl") != null)
//    document.getElementById("Sec2ImageCtrl").setAttribute("alt", "Security Image2");
//ZD 100420
if (document.getElementById('btnGoBack') != null)
    document.getElementById('btnGoBack').setAttribute("onblur", "document.getElementById('btnSubmitAddNew').focus(); document.getElementById('btnSubmitAddNew').setAttribute('onfocus', '');");
if (document.getElementById('btnSubmitAddNew') != null)
    document.getElementById('btnSubmitAddNew').setAttribute("onblur", "document.getElementById('btnSubmit').focus(); document.getElementById('btnSubmit').setAttribute('onfocus', '');");
if (document.getElementById('txtHelpReqEmail') != null)
    document.getElementById('txtHelpReqEmail').setAttribute("onblur", "document.getElementById('btnAddHelpReq').focus(); document.getElementById('btnAddHelpReq').setAttribute('onfocus', '');");

//ZD 102364
function fnUpload2(obj, btnid, grpname) {

    if (!Page_ClientValidate(grpname))
        return Page_IsValid;

    if (obj == null || obj == undefined || obj == "") {
        alert(WithoutImage);
        return false;
    }
    else {
        __doPostBack(btnid, '');
    }
}

function fnUpload4(obj1, obj2, obj3, obj4, btnid, grpname) 
{
    if (!Page_ClientValidate(grpname))
        return Page_IsValid;

    var val = "";
    if (obj1 != null && obj1.value != "")
        val += obj1.value;
    if (obj2 != null && obj2.value != "")
        val += obj2.value;
    if (obj3 != null && obj3.value != "")
        val += obj3.value;
    if (obj4 != null && obj4.value != "")
        val += obj4.value;

    if (val == "") {
        alert(WithoutImage);
        return false;
    }
    else {
        __doPostBack(btnid, '');
    }
}

//ZD 102700 - Start
if (document.getElementById('btnRemoveMap1') != null)
    document.getElementById('btnRemoveMap1').style.verticalAlign = "top";

if (document.getElementById('btnRemoveMap2') != null)
    document.getElementById('btnRemoveMap2').style.verticalAlign = "top";

if (document.getElementById('btnRemoveMisc1') != null)
    document.getElementById('btnRemoveMisc1').style.verticalAlign = "top";

if (document.getElementById('btnRemoveMisc2') != null)
    document.getElementById('btnRemoveMisc2').style.verticalAlign = "top";
//ZD 102700 - End

</script>
<%--ZD 100419 End--%>
<% 
    if (Request.QueryString["cal"] == "2")
    { 
%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%} %>

<script type="text/javascript">chgEndpoint();</script>

