<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EndpointSearch.aspx.cs" Inherits="en_EndpointSearch"
    EnableEventValidation="false" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%--<script type="text/javascript" language="JavaScript" src="inc/disablerclick.js"></script>--%>

<script type="text/javascript" src="script/errorList.js"></script>

<script type="text/javascript" language="JavaScript" src="inc/functions.js"></script>

<script type="text/javascript" src="extract.js"></script>

<script type="text/javascript" src="script/mousepos.js"></script> <%-- ZD 102723 --%>

<script type="text/javascript" src="script/showmsg.js"></script> <%-- ZD 102723 --%>

<script language="JavaScript" src="inc/functions.js"></script>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>

<%--ZD 100420 Start--%>
<style type="text/css">

a img { outline:none;
    text-decoration:none;
    border:0;
}

</style>
<%--ZD 100420 - End--%>

<script type="text/javascript">
    // <![CDATA[
    var isIE = false;
    if (navigator.userAgent.indexOf('Trident') > -1)
        isIE = true;
        
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End   
    //FB 2958 - START
    function DataLoading(val) {
        if (document.getElementById("dataLoadingDIV") == null)
            return false;
        document.getElementById("dataLoadingDIV").style.position = 'absolute';

        document.getElementById("dataLoadingDIV").style.left = window.screen.width / 2 - 100;
        document.getElementById("dataLoadingDIV").style.top = 100;

        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //FB 2958 - END

function pageBarFirstButton_Click(){

try 
{

var drp =  document.getElementById("DrpDwnListView");
    
    if(drp)
    {
        if(drp.value == "1")
            grid2.GotoPage(0);
        else
        {
            grid.GotoPage(0);
            fnViewRoom();
        }
    }
}
catch(exception)
{}

}

function pageBarPrevButton_Click(){
try{
var drp =  document.getElementById("DrpDwnListView");
    
    if(drp)
    {
        
        if(drp.value == "1")
            grid2.PrevPage();
        else
        {
            grid.PrevPage();
            fnViewRoom();
        }
    }
}
catch(exception)
{}
    
}
function pageBarNextButton_Click(){
try{
var drp =  document.getElementById("DrpDwnListView");
    
    if(drp)
    {
        if(drp.value == "1")
             grid2.NextPage();
        else
        {
             grid.NextPage();
             fnViewRoom();
        }             
    }
}
catch(exception)
{}
   
   
}

function pageBarLastButton_Click(s, e){
try{
var drp =  document.getElementById("DrpDwnListView");
    
    if(drp)
    {
        if(drp.value == "1")
            grid2.GotoPage(grid2.cpPageCount - 1);
        else
        {
            grid.GotoPage(grid.cpPageCount - 1);
            fnViewRoom();
        }
    }
}
catch(exception)
{}
    
}
function pageBarTextBox_Init(s, e) {
try{
    s.SetText(s.cpText);
}
catch(exception)
{}
}
function pageBarTextBox_KeyPress(s, e){
try{

    if(e.htmlEvent.keyCode != 13)
        return;
    e.htmlEvent.cancelBubble = true;
    e.htmlEvent.returnValue = false;
    var pageIndex = (parseInt(s.GetText()) <= 0) ? 0 : parseInt(s.GetText()) - 1;
    
    
    var drp =  document.getElementById("DrpDwnListView");
    
    if(drp)
    {
        if(drp.value == "1")
           grid2.GotoPage(pageIndex);
        else
           grid.GotoPage(pageIndex);
    }
}
catch(exception)
{}
    
   
    
}
function pageBarTextBox_ValueChanged(s, e){
try{

    var pageIndex = (parseInt(s.GetText()) <= 0) ? 0 : parseInt(s.GetText()) - 1;
    
    var drp =  document.getElementById("DrpDwnListView");
    
    if(drp)
    {
        if(drp.value == "1")
            grid2.GotoPage(pageIndex);
        else
            grid.GotoPage(pageIndex);
    }
}
catch(exception)
{}
    
}
function pagerBarComboBox_SelectedIndexChanged(){
try{
 var drp =  document.getElementById("DrpDwnListView");
    
    if(drp)
    {
        if(drp.value == "1")
            grid2.PerformCallback(pagerBarComboBox_SelectedIndexChanged.arguments[0].value);
        else
            grid.PerformCallback(pagerBarComboBox_SelectedIndexChanged.arguments[0].value); 
    }
}
catch(exception)
{}
}

// ]]>
</script>

<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
</script>

<script type="text/javascript" src="script/mytreeNET.js"></script>

<script type="text/javascript">

  var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Day%>", 10),
  parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
  
</script>

<script type="text/javascript" src="script/cal.js"></script>

<script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js" ></script>

<script type="text/javascript" src="script/calendar-setup.js"></script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Endpoint Search</title>
    <link rel="stylesheet" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />
    <%--Edited for FF--%>
</head>
<body style="margin: 0 0 0 0">
    <form id="frmEndpointSearch" runat="server" class="tabContents">
    <asp:ScriptManager ID="RoomsearchScript" runat="server">
     <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
    </asp:ScriptManager>    
    <%--ZD 100420--%>
     <script type="text/javascript">
         var prm = Sys.WebForms.PageRequestManager.getInstance();
         prm.add_beginRequest(BeginRequestHandler);
         prm.add_endRequest(EndRequestHandler);
         function BeginRequestHandler(sender, args) {
             window.parent.scroll(0, 0);
         }
         function EndRequestHandler(sender, args) {
             window.parent.scroll(0, 0);
         }
     </script>
    <input type="hidden" id="cmd" value="GetSettingsSelect" />
    <input type="hidden" id="helpPage" value="84" />
    <input type="hidden" id="hdnRoomIDs" runat="server" />
    <input runat="server" id="IsSettingsChange" type="hidden" />
    <input type="hidden" id="hdnCapacityH" runat="server" />
    <input type="hidden" id="hdnCapacityL" runat="server" />
    <input type="hidden" id="hdnMCU" runat="server" value="0" />
    <input type="hidden" id="hdnMedia" runat="server" />
    <input type="hidden" id="hdnLoc" runat="server" value="0" />
    <input type="hidden" id="hdnName" runat="server" />
    <input type="hidden" id="hdnProfileName" runat="server" />
    <input type="hidden" id="hdnAvailable" runat="server" />
    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
        <img border='0' src='image/wait1.gif' alt='Loading..' />
    </div><%--ZD 100678--%>
    <%
        if (Request.QueryString["hf"] != null)
        {
            if (Request.QueryString["hf"].ToString() == "1")
            {
    %>
    <table width="100%" border="0">
        <tr>
            <td align="center">
                <h3><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_EndpointSearch%>" runat="server"></asp:Literal><asp:Button id="close" runat="server" text="<%$ Resources:WebResources, EndpointSearch_close%>" class="altShort2BlueButtonFormat" onclientclick="javascript:ClosePopup();"></asp:Button><%--FB 1552--%>
                    <input type='submit' name='SoftEdgeTest1' style='max-height: 0px; max-width: 0px;
                        height: 0px; width: 0px; background-color: Transparent; border: None;' />
                </h3>
            </td>
        </tr>
    </table>
    <%              
                
        }
        }   
    %>
    <div class="tabContents" style="height: 545px; vertical-align: super;">
        <table width="100%">
            <tr valign="top">
                <td style="width: 31%; height: 540px">
                    <br />
                    <asp:Panel ID="Filters" runat="server" Height="550px" CssClass="treeSelectedNode"
                        BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" ScrollBars="Auto">
                        <table width="100%">
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="ExtenderName" runat="server" TargetControlID="NameTable"
                                        ImageControlID="RmNameImg" CollapseControlID="RmNameImg" ExpandControlID="RmNameImg"
                                        ExpandedImage="image/loc/nolines_minus.gif" CollapsedImage="image/loc/nolines_plus.gif"
                                        Collapsed="false" CollapsedSize="30">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="NameTable" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a href="#" id="ancEndpoint" onkeydown="javascript:return fnTabNav('RmNameImg','ancProfileName','null',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="RmNameImg" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse" /></a>
                                                    <span class=""><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_EndpointName%>" runat="server"></asp:Literal></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="TxtNameSearch" CssClass="altText" runat="server" Width="60%"></asp:TextBox>&nbsp;
                                                    <%-- Code Added for FB 1640--%>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="TxtNameSearch"
                                                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters38%>"
                                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@#$%&'~]*$"></asp:RegularExpressionValidator>
                                                    <button name="NameSubmit" class="altMedium0BlueButtonFormat"
                                                        style="width: 80px;" onclick="javascript:NameSearch();return false;">
                                                        <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button><%--ZD 100420--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="ProfileName" runat="server" TargetControlID="ProfileTable"
                                        ImageControlID="RmNameImg1" CollapseControlID="RmNameImg1" ExpandControlID="RmNameImg1"
                                        ExpandedImage="image/loc/nolines_minus.gif" CollapsedImage="image/loc/nolines_plus.gif"
                                        Collapsed="false" CollapsedSize="30">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="ProfileTable" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a href="#" id="ancProfileName" onkeydown="javascript:return fnTabNav('RmNameImg1','ancMCU','ancEndpoint',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="RmNameImg1" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse"/></a>
                                                    <span class=""><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_ProfileName%>" runat="server"></asp:Literal></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="TxtProfileName" CssClass="altText" runat="server" Width="60%"></asp:TextBox>&nbsp;
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="TxtProfileName"
                                                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:@^#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                    <button name="NameSubmit" class="altMedium0BlueButtonFormat"
                                                         style="width: 80px;" onclick="javascript:ProfileNameSearch();return false;">
                                                         <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button><%--ZD 100420--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr align="center">
                                <td align="center">
                                    <span class="blackbigblodtext"><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_OR%>" runat="server"></asp:Literal></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="AvExtender" runat="server" TargetControlID="AVTable"
                                        CollapseControlID="AVImg" ImageControlID="AVImg" ExpandControlID="AVImg" ExpandedImage="image/loc/nolines_minus.gif"
                                        Collapsed="false" CollapsedImage="image/loc/nolines_Plus.gif" CollapsedSize="30">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="AvTable" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a href="#" id="ancMCU" onkeydown="javascript:return fnTabNav('AVImg','ancEndpointType','ancProfileName',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="AVImg" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse"/></a>
                                                    <span class=""><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_AssignedtoMCU%>" runat="server"></asp:Literal></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstBridges" Width="80%" runat="server"
                                                        DataTextField="BridgeName" DataValueField="BridgeID" onchange="javascript:ChangeDropValue()">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="BtnUpdateStates" />
                                        </Triggers>
                                        <ContentTemplate>
                                            <ajax:CollapsiblePanelExtender ID="LocExtender" runat="server" TargetControlID="LocPanel"
                                                ImageControlID="CntryImg" CollapseControlID="CntryImg" ExpandControlID="CntryImg"
                                                CollapsedImage="image/loc/nolines_plus.gif" Collapsed="false" ExpandedImage="image/loc/nolines_Minus.gif"
                                                CollapsedSize="30">
                                            </ajax:CollapsiblePanelExtender>
                                            <asp:Panel ID="LocPanel" runat="server">
                                                <table class="treeSelectedNode" width="100%">
                                                    <tr>
                                                        <td class="tableHeader">
                                                            <a href="#" id="ancEndpointType" onkeydown="javascript:return fnTabNav('CntryImg','ancEndpointModel','ancMCU',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="CntryImg" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse" /></a>
                                                            <span class=""><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_EndpointType%>" runat="server"></asp:Literal></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="blacktext">
                                                            <asp:DropDownList ID="lstAddressType" runat="server" CssClass="altText" DataTextField="Name"
                                                                DataValueField="ID" onchange="javascript:ChangeDropValue()">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Button ID="BtnUpdateStates" Style="display: none;" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <%--ZD 100736 START--%>
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="ManufacturerExtender" runat="server" TargetControlID="ManufacturerPanel"
                                        ImageControlID="MediaIm" CollapseControlID="MediaIm" ExpandControlID="MediaIm"
                                        ExpandedImage="image/loc/nolines_minus.gif" Collapsed="false" CollapsedImage="image/loc/nolines_plus.gif"
                                        CollapsedSize="30">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="ManufacturerPanel" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a href="#" id="ancManufacturer" onkeydown="javascript:return fnTabNav('MediaIm','ancTelnetAPI','ancEndpointType',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="MediaIm" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse"/></a>
                                                    <span class=""><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, EndpointSearch_EndpointManufacturer%>" runat="server"></asp:Literal></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="blacktext">
                                                    <asp:DropDownList ID="lstManufacturer" Visible="true" runat="server" CssClass="altText" Width="63%"
                                                        DataTextField="ManufacturerName" DataValueField="ManufacturerID" AutoPostBack="true" OnSelectedIndexChanged="fnUpdateVideoEquipment">  
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <%--ZD 100736 END--%>
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="MediaExtender" runat="server" TargetControlID="MediaPanel"
                                        ImageControlID="MediaImg" CollapseControlID="MediaImg" ExpandControlID="MediaImg"
                                        ExpandedImage="image/loc/nolines_minus.gif" Collapsed="false" CollapsedImage="image/loc/nolines_plus.gif"
                                        CollapsedSize="30">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="MediaPanel" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a href="#" id="ancEndpointModel" onkeydown="javascript:return fnTabNav('MediaImg','ancTelnetAPI','ancEndpointType',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="MediaImg" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse"/></a>
                                                    <span class=""><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_EndpointModel%>" runat="server"></asp:Literal></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="blacktext">
                                                    <asp:DropDownList ID="lstLineRate" Visible="false" runat="server" CssClass="altText"
                                                        DataTextField="LineRateName" DataValueField="LineRateID">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="lstVideoProtocol" Visible="false" runat="server" CssClass="altText"
                                                        DataTextField="Name" DataValueField="ID">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="lstConnectionType" runat="server" DataTextField="Name" DataValueField="ID"
                                                        Visible="false">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="lstVideoEquipment" Visible="true" runat="server" CssClass="altText" Width="63%"
                                                        DataTextField="EquipmentDisplayName" DataValueField="VideoEquipmentID" onchange="javascript:ChangeDropValue()"> <%--ZD 100749--%> <%--ZD 100736--%>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="PhotoExtender" runat="server" TargetControlID="PhotoTable"
                                        ImageControlID="PhotImg" CollapseControlID="PhotImg" ExpandControlID="PhotImg"
                                        ExpandedImage="image/loc/nolines_minus.gif" Collapsed="false" CollapsedImage="image/loc/nolines_plus.gif"
                                        CollapsedSize="30">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="PhotoTable" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a href="#" id="ancTelnetAPI" onkeydown="javascript:return fnTabNav('PhotImg','ancOutsideNW','ancEndpointModel',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="PhotImg" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse"/></a>
                                                    <span class=""><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_TelnetAPI%>" runat="server"></asp:Literal></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="ChkTelnetAPI" Text="<%$ Resources:WebResources, EndpointSearch_ChkTelnetAPI%>" onclick="javascript:RefreshEndpoints()" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="OutsideTable"
                                        ImageControlID="OutsideNw" CollapseControlID="OutsideNw" ExpandControlID="OutsideNw"
                                        ExpandedImage="image/loc/nolines_minus.gif" Collapsed="false" CollapsedImage="image/loc/nolines_plus.gif"
                                        CollapsedSize="30">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="OutsideTable" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a href="#" id="ancOutsideNW" onkeydown="javascript:return fnTabNav('OutsideNw','Reset1','ancTelnetAPI',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="OutsideNw" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse"/></a>
                                                    <span class=""><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_OutsideNetwork%>" runat="server"></asp:Literal></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="ChkOutsideNw" Text=" <%$ Resources:WebResources, EndpointSearch_ChkOutsideNw%>" onclick="javascript:RefreshEndpoints()" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <button type="reset" name="reset" class="altMedium0BlueButtonFormat"
                                        id="Reset1" onclick="JavaScript: history.go(0);">
                                        <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, Reset%>" runat="server"></asp:Literal></button><%--ZD 100420--%>                                        
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td style="width: 69%; height: 540px">
                    <asp:UpdatePanel ID="RoomsUpdate" UpdateMode="Conditional" runat="server" RenderMode="Inline">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnRefreshEndpoint" />
                        </Triggers>
                        <ContentTemplate>
                            <center>
                                <asp:Label ID="LblError" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                            </center>
                            <table width="100%">
                                <tr>
                                    <td style="width: 70%">
                                        <%--<span class="blackblodtext">View Type :</span>&nbsp;--%>
                                        <asp:Label runat="server" ID="lblViewType" Text="<%$ Resources:WebResources, EndpointSearch_lblViewType%>" CssClass="blackblodtext"></asp:Label>&nbsp; <%--FB 2599--%>
                                        <asp:DropDownList ID="DrpDwnListView" CssClass="altText" runat="server" AutoPostBack="false"
                                            onchange="javascript:ChangeViewType()">
                                            <asp:ListItem Text="<%$ Resources:WebResources, CompactView%>" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="<%$ Resources:WebResources, DetailsView%>" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="blackblodtext"><font size="1">
                                            <asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_Clickonthe%>" runat="server"></asp:Literal></font></span>
                                        <input type="hidden" id="hdnView" runat="server" />
                                        <input type="hidden" id="addroom" value="0" runat="server" />
                                        <input type="hidden" id="hdnDelRoom" value="0" runat="server" />
                                        <input type="hidden" id="hdnDelEndpointID" runat="server" />
                                        <input type="hidden" id="hdnEditroom" value="0" runat="server" />
                                        <input runat="server" id="selectedlocframe" type="hidden" />
                                        <input type="hidden" id="hdnObject" runat="server" />
                                        <input type="hidden" id="hdnEptID" runat="server" />
                                        <input type="hidden" id="locstr" name="locstr" value="" runat="server" />
                                        <input type="hidden" id="Tierslocstr" name="Tierslocstr" value="" runat="server" />
                                        <dx:ASPxPopupControl ID="MoreInfoPopup" runat="server" ClientInstanceName="MoreInfoPopup" CloseAction="CloseButton"
                                            Width="250px">
                                            <ContentCollection>
                                                <dx:PopupControlContentControl>
                                                    <dxe:ASPxMemo ID="txtMemo" runat="server" Height="91px" Width="240px" ReadOnly="true" ClientInstanceName="memo"
                                                        Border-BorderStyle="None">
                                                        <ClientSideEvents Init="function(s, e) {s.GetInputElement().style.overflowY='hidden';ASPxClientUtils.AttachEventToElement(window.document, 'keydown', function(evt) {
        if(evt.keyCode == ASPxClientUtils.StringToShortcutCode('ESCAPE'))
            MoreInfoPopup.Hide();
        });}" />
                                                    </dxe:ASPxMemo>
                                                </dx:PopupControlContentControl>
                                            </ContentCollection>
                                            <ContentStyle>
                                                <Paddings PaddingBottom="5px" />
                                            </ContentStyle>
                                        </dx:ASPxPopupControl>
                                        <asp:Panel ID="PanelRooms" runat="server" Height="540px" CssClass="treeSelectedNode"
                                            BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px">
                                            <div align="center" id="conftypeDIV" style="width: 100%;" class="treeSelectedNode">
                                                <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                    <tr id="DetailsView" runat="server" style="display: none;">
                                                        <td width="40%" align="left" style="font-size: x-small; font-family: arial" valign="middle">
                                                            <%--Edited for FF--%>
                                                            <dxwgv:ASPxGridView AllowSort="true" OnHtmlRowPrepared="ASPxGridView1_HtmlRowCreated"
                                                                ID="grid" Width="100%" ClientInstanceName="grid" runat="server" KeyFieldName="EndpointID"
                                                                EnableRowsCache="True" OnCustomCallback="Grid_CustomCallback" OnDataBound="Grid_DataBound"
                                                                AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <dxwgv:GridViewDataColumn FieldName="EndpointName" VisibleIndex="1" Width="87%" HeaderStyle-HorizontalAlign="Center"
                                                                        Caption="<%$ Resources:WebResources, endpointdetails_EndpointDetail%>" HeaderStyle-Font-Bold="true" />
                                                                    <dxwgv:GridViewDataColumn FieldName="EndpointID" Visible="False" />
                                                                    <dxwgv:GridViewDataColumn FieldName="EndpointName" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-Font-Bold="true" Caption="<%$ Resources:WebResources, Actions%>" />
                                                                </Columns>
                                                                <Styles>
                                                                    <CommandColumn Paddings-Padding="1">
                                                                    </CommandColumn>
                                                                    <AlternatingRow Enabled="True" Border-BorderColor="Azure">
                                                                    </AlternatingRow>
                                                                </Styles>
                                                                <Settings ShowPreview="true" GridLines="Both" UseFixedTableLayout="true" ShowColumnHeaders="true" />
                                                                <SettingsCustomizationWindow Enabled="True" />
                                                                <SettingsBehavior ColumnResizeMode="Control" />
                                                                <SettingsText EmptyDataRow="<%$ Resources:WebResources, NoData%>" GroupPanel="<%$ Resources:WebResources, GridViewGroupMsg%>"/>
                                                                <SettingsPager Mode="ShowPager" PageSize="5" AlwaysShowPager="true" Position="Top">
                                                                </SettingsPager>
                                                                <Templates>
                                                                    <DataRow>
                                                                        <div style="padding: 1px">
                                                                            <table class="templateTable" cellpadding="0" cellspacing="1" width="100%" border="0">
                                                                                <tr style="font-size: x-small; font-family: arial" valign="middle">
                                                                                    <td>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="templateCaption" style="width: 15%">
                                                                                                    <b><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_EndpointName%>" runat="server"></asp:Literal> :</b>
                                                                                                    <%#  DataBinder.Eval(Container, "DataItem.EndpointName")%>
                                                                                                </td>
                                                                                                <td class="templateCaption" style="width: 15%;">
                                                                                                    <%--FB 1886--%>
                                                                                                    <b><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_Rooms%>" runat="server"></asp:Literal></b>
                                                                                                    <%#  DataBinder.Eval(Container, "DataItem.RoomName")%>
                                                                                                     <asp:ImageButton ID="imgRoomList" ImageUrl="image/info.png" AlternateText="Info" style="cursor:default"
                                                                                                        runat="server" OnClick="GetRoomDetails" ToolTip="<%$ Resources:WebResources, Clicktoviewrooms%>"  /> <%--ZD 100419--%><%-- ZD 102590 --%>
                                                                                                    <asp:Label runat="server" ID="lblIsTelepresence" Text='<%# Eval("isTelePresence") %>' Visible="false">
                                                                                                    </asp:Label> <%--FB 2400--%>
                                                                                                     
                                                                                                </td>
                                                                                                <td class="templateCaption" style="width: 10%">
                                                                                                    <b><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_DefaultProfile%>" runat="server"></asp:Literal></b>
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.ProfileName")%>
                                                                                                </td>
                                                                                                <td class="templateCaption" style="width: 9%">
                                                                                                    <b><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_TotalProfiles%>" runat="server"></asp:Literal> :</b>
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.ProfileCount")%>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="templateCaption" style="width: 9%">
                                                                                                    <%--FB 1886--%>
                                                                                                    <b><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_Address%>" runat="server"></asp:Literal></b>
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.Address")%>
                                                                                                </td>
                                                                                                <td class="templateCaption" style="width: 15%">
                                                                                                    <b><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_EndpointModel%>" runat="server"></asp:Literal> :</b>
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.VideoEquipment")%>
                                                                                                </td>
                                                                                                <td class="templateCaption" style="width: 9%">
                                                                                                    <b><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_DialingOption%>" runat="server"></asp:Literal></b>
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.ConnectionType")%>
                                                                                                </td>
                                                                                                <td class="templateCaption" style="width: 9%">
                                                                                                    <b><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_VideoProtocol%>" runat="server"></asp:Literal></b>
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.DefaultProtocol")%>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="templateCaption" style="width: 9%">
                                                                                                    <%--FB 1886--%>
                                                                                                    <b><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_AddressType%>" runat="server"></asp:Literal></b>
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.AddressType")%>
                                                                                                </td>
                                                                                                <td class="templateCaption" style="width: 15%">
                                                                                                    <b><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_Bandwidth%>" runat="server"></asp:Literal></b>
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.LineRate")%>
                                                                                                </td>
                                                                                                <td style="width: 9%" class="templateCaption">
                                                                                                    <b><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_AssignedMCU%>" runat="server"></asp:Literal></b>
                                                                                                    <asp:Label ID="lblBridgeID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Bridge")%>'
                                                                                                        Visible="false"></asp:Label>
                                                                                                    <dxe:ASPxLabel runat="server" ID="lblBrID" Text='<%# Eval("Bridge") %>' Visible="false">
                                                                                                    </dxe:ASPxLabel>
                                                                                                    <asp:Label ID="lblBridgeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgeName")%>'></asp:Label>
                                                                                                    <br />
                                                                                                    <asp:HyperLink NavigateUrl="javascript:return false;" Text="<%$ Resources:WebResources, EndpointSearch_btnViewBridgeDetails%>" Style="cursor: pointer;" runat="server" ID="btnViewBridgeDetails"
                                                                                                        Visible='<%# !DataBinder.Eval(Container, "DataItem.BridgeName").ToString().Equals("") %>'></asp:HyperLink>
                                                                                                </td>
                                                                                                <td class="templateCaption" style="width: 10%">
                                                                                                    <b><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_OutsideNetwork%>" runat="server"></asp:Literal> :</b>
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.IsOutside")%>
                                                                                                </td>
                                                                                                <td style="width: 9%; display: none" class="templateCaption" nowrap>
                                                                                                    <%--FB 1886--%>
                                                                                                    <asp:Label ID="txtProfile" runat="server" Font-Bold="true" Text="<%$ Resources:WebResources, EndpointSearch_txtProfile%>"></asp:Label>
                                                                                                    <asp:DropDownList CssClass="altLong4SelectFormat" runat="server" ID="lstProfiles"
                                                                                                        DataTextField="ProfileName" DataValueField="ProfileID">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="right" class="templateCaption" colspan="5" nowrap><%-- FB 2670--%>
                                                                                        <asp:HyperLink NavigateUrl="javascript:return false;" ID="Editendpoint" Text="<%$ Resources:WebResources, EndpointSearch_Editendpoint%>" Style="cursor: pointer;" runat="server"></asp:HyperLink>
                                                                                        <asp:HyperLink NavigateUrl="javascript:return false;" ID="DeleteEndpoint" Text="<%$ Resources:WebResources, EndpointSearch_DeleteEndpoint%>" Style="cursor: pointer; "
                                                                                            runat="server"></asp:HyperLink>
                                                                                        <asp:HyperLink NavigateUrl="javascript:return false;" ID="SelectEP" Text="<%$ Resources:WebResources, EndpointSearch_SelectEP%>" Style="cursor: pointer;" runat="server"></asp:HyperLink>
                                                                                        &nbsp;<asp:HyperLink NavigateUrl="javascript:return false;" ID="AddEP" Text="<%$ Resources:WebResources, EndpointSearch_AddEP%>" Style="cursor: pointer;" runat="server"></asp:HyperLink>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </DataRow>
                                                                </Templates>
                                                            </dxwgv:ASPxGridView>
                                                        </td>
                                                    </tr>
                                                    <tr id="ListView" runat="server">
                                                        <td width="40%" align="center" style="font-weight: bold; font-size: small; color: green;
                                                            font-family: arial; font" valign="middle">
                                                            <%--Edited for FF--%>
                                                            <dxwgv:ASPxGridView OnHtmlRowCreated="ASPxGridView1_HtmlRowCreated" ID="grid2" ClientInstanceName="grid2"
                                                                runat="server" KeyFieldName="EndpointID" Width="100%" EnableRowsCache="True"
                                                                OnCustomCallback="Grid2_CustomCallback" OnDataBound="Grid2_DataBound" AutoGenerateColumns="false"
                                                                SettingsBehavior-AllowSort="true" EnableCallBacks="false">
                                                                <Columns>
                                                                    <dxwgv:GridViewDataColumn FieldName="EndpointName" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center"
                                                                        Caption="<%$ Resources:WebResources, EndpointList_EndpointName%>" HeaderStyle-Font-Bold="true" />
                                                                    <dxwgv:GridViewDataColumn FieldName="EndpointID" Visible="False" />
                                                                    <dxwgv:GridViewDataColumn FieldName="RoomName" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-Font-Bold="true" Caption="<%$ Resources:WebResources, RoomName%>" Visible="false" />
                                                                    <%--FB 1886--%>
                                                                    <dxwgv:GridViewDataColumn FieldName="EndpointName" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-Font-Bold="true" Caption="<%$ Resources:WebResources, Actions%>" />
                                                                </Columns>
                                                                <Styles>
                                                                    <CommandColumn Paddings-Padding="1">
                                                                    </CommandColumn>
                                                                </Styles>
                                                                <SettingsBehavior AllowMultiSelection="false" />
                                                                <SettingsText EmptyDataRow="<%$ Resources:WebResources, NoData%>" HeaderFilterShowAll="false" GroupPanel="<%$ Resources:WebResources, GridViewGroupMsg%>"/>
                                                                <SettingsPager Mode="ShowPager" PageSize="100" AlwaysShowPager="true" Position="Top" >
                                                                </SettingsPager>
                                                                <Templates>
                                                                    <DataRow>
                                                                        <div style="padding: 1px">
                                                                            <table class="templateTable" cellpadding="0" cellspacing="1" width="100%">
                                                                                <tr>
                                                                                    <td class="templateCaption" style="width: 50%">
                                                                                        <%#  DataBinder.Eval(Container, "DataItem.EndpointName")%>
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="imgRoomList" ImageUrl="image/info.png" AlternateText="Info" style="cursor:default"
                                                                                                        runat="server" OnClick="GetRoomDetails" ToolTip="<%$ Resources:WebResources, Clicktoviewrooms%>" /> <%--ZD 100419--%><%-- ZD 102590 --%>
                                                                                        <asp:Label runat="server" ID="lblIsTelepresence" Text='<%# Eval("isTelePresence") %>' Visible="false">
                                                                                                    </asp:Label> <%--FB 2400--%>               
                                                                                    </td>
                                                                                    <td class="templateCaption" style="width: 34%;display:none;" >
                                                                                        <%--FB 1886--%>
                                                                                        <%--<%#  DataBinder.Eval(Container, "DataItem.RoomName")%> --%>                                                                                       
                                                                                    </td>
                                                                                    <td align="center" nowrap class="templateCaption"> <%--FB 2670--%>
                                                                                        <asp:HyperLink NavigateUrl="javascript:return false;" ID="Editendpoint" Text="<%$ Resources:WebResources, EndpointSearch_Editendpoint%>" Style="cursor: pointer;" runat="server"></asp:HyperLink>&nbsp;&nbsp;
                                                                                        <asp:HyperLink NavigateUrl="javascript:return false;" ID="DeleteEndpoint" Text="<%$ Resources:WebResources, EndpointSearch_DeleteEndpoint%>" Style="cursor: pointer; "
                                                                                            runat="server"></asp:HyperLink>
                                                                                        <asp:HyperLink NavigateUrl="javascript:return false;" ID="SelectEP" Text="<%$ Resources:WebResources, EndpointSearch_SelectEP%>" Style="cursor: pointer;" runat="server"></asp:HyperLink>
                                                                                        <asp:HyperLink NavigateUrl="javascript:return false;" ID="AddEP" Text="<%$ Resources:WebResources, EndpointSearch_AddEP%>" Style="cursor: pointer;" runat="server"></asp:HyperLink>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </DataRow>
                                                                </Templates>
                                                            </dxwgv:ASPxGridView>
                                                        </td>
                                                    </tr>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="TrLicense" runat="server">
                                    <td align="left">
                                        <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_TotalEndpoints%>" runat="server"></asp:Literal></span>
                                        <asp:Label ID="lblTtlEndpoints" CssClass="blackblodtext" runat="server"></asp:Label><%--FB 2594--%>
                                        <span class="blackblodtext" id="lblpublEP" runat="server"> ;<asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_lblpublEP%>" runat="server"></asp:Literal></span>
                                        <asp:Label ID="lblTtlPublicEP" CssClass="blackblodtext" runat="server"></asp:Label>
                                        <span class="blackblodtext"> ;<asp:Literal Text="<%$ Resources:WebResources, EndpointSearch_LicensesRemain%>" runat="server"></asp:Literal></span>
                                        <asp:Label ID="lblTempLic" CssClass="blackblodtext" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button ID="btnRefreshEndpoint" Style="display: none;" runat="server" OnClick="ChangeCalendarDate" />
                            </td> </tr> </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>

<script type="text/javascript"><!--

function ClosePopup()
 {
    
    try 
    {
        var args = ClosePopup.arguments;

        if(opener)
        {
      
            var f = top.opener.document.forms['<%=Parentframe%>'];  
            var prnt = parent.opener.document.getElementById("hdnEPID"); //Edited for FF
            var add = parent.opener.document.getElementById("addEndpoint");  //Edited for FF
            var isEPTel = parent.opener.document.getElementById("hdnisEPTelePresence"); //FB 2400
            
          if(prnt)
          {
            prnt.value = args[0];
            
            if(args[1] != null)//FB 2400
                isEPTel.value = args[1];
          }
          
            if(add)
                add.click();

        }

        if (parent.opener.document.getElementById("chkUnlistedEndpoint") != null) //FB 1552
        {
            if (parent.opener.document.getElementById("chkUnlistedEndpoint").checked)
                parent.opener.document.getElementById("chkListedEndpoint").checked = false;
        }
                
        window.close()
    }
    catch(exception)
    {
      window.close()
    }
       
 }
 
 function AddEndpoint()
{
        var url = "";
        var locs = document.getElementById("locstrname");
        if(locs)
        {
            var rooms = locs.value.split(',');
            var i = rooms.length;
            
            var lst  =  document.getElementById("RoomList");
            
            if(lst)
                clearlistbox(lst);
            
            while (i--) 
              {
                  if(rooms[i].split('|')[0] != "")
                  {
                    AddItem(rooms[i].split('|')[1],rooms[i].split('|')[0]);
                  }
                
             }
         }
}

 
function ViewBridgeDetails(bid)
{
    url = "BridgeDetailsViewOnly.aspx?hf=1&bid=" + bid;
    window.open(url, "BrdigeDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
    return false;
}

function CorrectHdnString()
{
    var locs =  document.getElementById("selectedlocframe");
    var vlue = "";
    
    roomIdsStr = locs.value.split(',');
    
    var i = roomIdsStr.length;
        
          while (i--) 
          {
            if (roomIdsStr[i] != "") 
            {
                if(vlue == "")
                    vlue =  roomIdsStr[i].trim();
                else
                    vlue += ","+  roomIdsStr[i].trim(); 
            }
            
         }
         
         locs.value = vlue;

}

function DeleteEndpoint() {

    
    var args = DeleteEndpoint.arguments;
    var endpointID = document.getElementById("hdnDelEndpointID");
    if (endpointID.value == "")
        endpointID.value = args[0];

    answer = confirm(RSDeleteEP)

    if (answer != "0") {
        var refrsh = document.getElementById("btnRefreshEndpoint");
        if (refrsh)
            refrsh.click();
    }
    else {
        endpointID.value = "";

        return false;  //ZD 100420
    }
}

//FB 2361 - Start
function fnViewRoom()
{
    var args = fnViewRoom.arguments;
    var endpointID =  document.getElementById("hdnEptID");
    var hdnObject =  document.getElementById("hdnObject");
    
    if(args.length > 0)
    {
        hdnObject.value = args[0].id;
    
        if(endpointID)
            endpointID.value = args[1];   
    }    
    
    return true;
}

//FB 2361 - End
    
//--></script>

<script type="text/javascript">

function RefreshEndpoints()
{
    var args = RefreshEndpoints.arguments;
    
    if(args)
    {
        if(args[0])
        {
            if(args[0].value == "")
            {
                alert(RSValidValue);
                return false;
            }
            
            var isfilter = document.getElementById("hdnIsFilterChanged");
            if(isfilter)
                isfilter.value = "Y";
        }
    }
    
    var telnet = document.getElementById("ChkTelnetAPI");
    
    var telnet = document.getElementById("ChkOutsideNw");
    
    
     var hdNm = document.getElementById("hdnName");
    if(hdNm)
        hdNm.value = "0";
        
    var hdNm = document.getElementById("hdnProfileName");
    if(hdNm)
        hdNm.value = "0";
        
     var hdNm = document.getElementById("hdnView");
    if(hdNm)
        hdNm.value = "0";
    
    var refrsh = document.getElementById("btnRefreshEndpoint");
    if(refrsh)
        refrsh.click();
    
}

function ChangeDropValue()
{
    var hdNm = document.getElementById("hdnName");
    if(hdNm)
        hdNm.value = "0";
        
    var hdNm = document.getElementById("hdnProfileName");
    if(hdNm)
        hdNm.value = "0";
        
     var hdNm = document.getElementById("hdnView");
    if(hdNm)
        hdNm.value = "0";
        
   var arg = ChangeDropValue.arguments;
   
  var refrsh = document.getElementById("btnRefreshEndpoint");
    if(refrsh)
        refrsh.click();
}


function MCUItemChanged()
{
    var avchg = document.getElementById("hdnMCU");
    if(avchg)
        avchg.value = "1";
     RefreshEndpoints();
        
}

function NameSearch()
{
    var hdNm = document.getElementById("hdnName");
    if(hdNm)
        hdNm.value = "1";
        
   var hdPn = document.getElementById("hdnProfileName");
   
   if(hdPn)
        hdPn.value = "0";
  
        
    var txtNm = document.getElementById("TxtNameSearch"); 
    
    if(txtNm.value == "")
    {
        alert(RSValidEptName);
        return false;
    }
    
    var refrshNm = document.getElementById("btnRefreshEndpoint");
    if(refrshNm)
        refrshNm.click();
    
}

function ProfileNameSearch()
{
    
    var hdPn = document.getElementById("hdnProfileName");
    var hdNm = document.getElementById("hdnName");
   
    if(hdNm)
        hdNm.value = "0";
   
    if(hdPn)
        hdPn.value = "1";
        
    var txtPm = document.getElementById("TxtProfileName");     
    
    if(txtPm.value == "")
    {
        alert(RSValidProfileName);
        return false;
    }
    
    var refrshNm = document.getElementById("btnRefreshEndpoint");
    if(refrshNm)
        refrshNm.click();
    
}

function ViewChng()
{
    var hdNm = document.getElementById("hdnView");
    if(hdNm)
        hdNm.value = "1";
    
}

var prm = Sys.WebForms.PageRequestManager.getInstance();
prm.add_initializeRequest(initializeRequest);

prm.add_endRequest(endRequest);

var postbackElement;
  
  function initializeRequest(sender, args) {
document.body.style.cursor = "wait";
DataLoading(1);
//document.getElementById("btnCompare").disabled = true;
}

function endRequest(sender, args) {document.body.style.cursor = "default";DataLoading(0);
//document.getElementById("btnCompare").disabled =  false;

 
}
 
 

 function EditEndpoint()
 {
   
    var rmids = EditEndpoint.arguments;
    
    var rmid = "";
    var drpValue = "";
     drpValue =  document.getElementById("DrpDwnListView");
    
    if(rmids)
    {
        rmid = rmids[0];
    }

    if(parent)
    {
      parent.location.replace("EditEndpoint.aspx?t=&EpID=" + rmid +"&DrpValue="+ drpValue.value);
    }
 }
 
 function AddTerminal()
 {
    var rmids = AddTerminal.arguments;
    var rmid = "";
    var rmid1 = "";

    if(rmids)
    {
        rmid = rmids[0];
        rmid1 = rmids[1];
    }
    if (parent)
     {
         //FB 1552 start
         parent.opener.document.getElementById("hdnEndpointID").value = rmid;
         parent.opener.document.getElementById("chkUnlistedEndpoint").checked = false;
         var add = parent.opener.document.getElementById("selectEndPoint");
         parent.window.focus();
         add.click();
         window.close();
        //parent.location.replace("AddTerminalEndpoint.aspx?t=TC&epid=" + rmid + "&cid=" + rmid1 + "&tpe=U");
        //FB 1552 end
    }
 }

 
 function ClearAllSelection()
 {
 try 
    {
   var locs =  document.getElementById("selectedlocframe");
 var adlocs =  document.getElementById("addroom");
 var hdNm = document.getElementById("locstr");
 
        locs.value = "";
        hdNm.value = "";
            
       if(adlocs)
        adlocs.value = "1";
    
        if(parent)
        {
          var prnt = parent.document.getElementById("selectedloc");
          if(prnt)
            prnt.value = locs.value;
        }
            
         if(opener)
        {
            var prnt = opener.document.getElementById("selectedloc");
          if(prnt)
          {
            prnt.value =locs.value;
          }
        }
       
      var refrsh = document.getElementById("btnRefreshEndpoint");
      if(refrsh)
        refrsh.click();
      
     }
    catch(exception)
    {
      window.close()
    }
 }
 
 function ChangeViewType()
 {
    var tr2=  document.getElementById("DetailsView");
    var tr1 =  document.getElementById("ListView");
    var drp =  document.getElementById("DrpDwnListView");
    
    if(drp)
    {
        tr1.style.display = 'none';
        tr2.style.display = 'none';        
        
        if(drp.value == "1")
            tr1.style.display = 'block';
        else
        {
            tr2.style.display = 'block';
            fnViewRoom();
        }
    }


}
function fnTabNav(cur, nxt, prv, event) {    
    
    var cur = document.getElementById(cur);
    var nxt = document.getElementById(nxt);
    var prv = document.getElementById(prv);

    if (event.keyCode == 9) {
        if (event.shiftKey) {
            if (cur != null) {
                if (cur.src.toLowerCase().indexOf('plus') > -1)
                    if (prv != null) {
                    prv.focus();
                    return false;
                }
            }

        }
        else {
            if (cur != null) {
                if (cur.src.toLowerCase().indexOf('plus') > -1)
                    if (nxt != null) {
                    nxt.focus();
                    return false;
                }
            }
        }
    }

}
//ZD 100428 START- Close the popup window using the esc key
document.onkeydown = EscClosePopup;
function EscClosePopup(e) {
    if (e == null)
        var e = window.event;
    if (e.keyCode == 27) {

        window.close();

    }
}
//ZD 100428 END
document.getElementById("ancEndpoint").setAttribute("onfocus", "window.parent.scrollTo(0,0);");
//ZD 101416
if (document.getElementById('grid2_tcPagerBarT') != null)
    document.getElementById('grid2_tcPagerBarT').style.width = "100%";


</script>

</html>
