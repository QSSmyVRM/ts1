﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>

<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_PasswordChange" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="../en/Organizations/Original/Styles/main.css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>myVRM</title>

    <script type="text/javascript">
        var path = '<%=Session["OrgCSSPath"]%>';
        if (path == "")
            path = "Organizations/Org_11/CSS/Mirror/Styles/main.css";
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
        document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script>

    <script language="JavaScript" src="inc/functions.js" type="text/javascript"></script>

    <script type="text/javascript">
        function fnLoginPage() {
            window.location.replace('genlogin.aspx');
            return false;
        }
    </script>

</head>
<body>
    <center>
        <form id="frmChanePwdReq" autocomplete="off" runat="server"><%--ZD 101190--%>
        <asp:HiddenField ID="txtUserID" Value="" runat="server" />
        <table>
            <tr style="height: 80px">
                <td>
                </td>
            </tr>
        </table>
        <table cellpadding="2" cellspacing="2" width="700">
            <tr align="left">
                <td>
                    <div>
                        <table style="width: 90%">
                            <tr>
                                <td align="center">
                                    <h3>
                                        Request Password
                                    </h3>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 1168px">
                                    <asp:Label ID="LblError" runat="server" Text="" CssClass="lblError"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="2" cellspacing="2" width="700">
                            <tr>
                                <td style="width: 80px">
                                </td>
                                <td align="left" colspan="2">
                                    <asp:Label ID="LblMessage" runat="server" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px">
                                </td>
                            </tr>
                            <tr id="trAdmin" runat="server">
                                <td>
                                </td>
                                <td align="left" colspan="2">
                                    <b><span class="subtitleblueblodtext">Please change expired password for the below user.</span>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px">
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="left" style="width: 80px">
                                    <label class="blackblodtext">
                                        Username</label><%--ZD 102706--%>
                                </td>
                                <td align="left" style="width: 500px">
                                    <asp:Label ID="lblUserName" runat="server"> </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="left" style="width: 80px">
                                    <label class="blackblodtext">
                                        Email</label>
                                </td>
                                <td align="left" style="width: 500px">
                                    <asp:Label ID="lblEmail" runat="server"> </asp:Label>
                                </td>
                            </tr>
                            <tr id="tdMsg" runat="server">
                                <td>
                                </td>
                                <td align="left" style="width: 80px">
                                    <label class="blackblodtext">
                                        Message</label>
                                </td>
                                <td align="left" style="width: 500px">
                                    <asp:TextBox ID="txtComment" runat="server" CssClass="altText" Style="width: 500px;"
                                        TextMode="MultiLine" MaxLength="256" autocomplete="off"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="tdPWD" runat="server">
                                <td>
                                </td>
                                <td align="left" style="width: 126px">
                                    <label>
                                        New Password<span class="reqfldstarText">*</span></label>
                                </td>
                                <td align="left" style="width: 500px">
                                    <asp:TextBox ID="txtNewPwd" runat="server" TextMode="Password" CssClass="altText"
                                        Style="width: 200px;" MaxLength="256" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqPassword1" ControlToValidate="txtNewPwd" Display="dynamic"
                                        ErrorMessage="Required" runat="server"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regPassword1" ControlToValidate="txtNewPwd" Display="dynamic"
                                        runat="server" SetFocusOnError="true" ErrorMessage="<br> & < and > are invalid characters."
                                        ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                    <asp:CompareValidator ID="cmpValPassword1" runat="server" ControlToCompare="txtPassword2"
                                        ControlToValidate="txtNewPwd" Display="Dynamic" ErrorMessage="<br>Re-enter password."></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr id="tdPWD1" runat="server">
                                <td>
                                </td>
                                <td align="left" style="width: 126px">
                                    <label>
                                        Confirm Password<span class="reqfldstarText">*</span></label>
                                </td>
                                <td align="left" style="width: 500px">
                                    <asp:TextBox ID="txtPassword2" runat="server" CssClass="altText" TextMode="Password"
                                        Style="width: 200px;" MaxLength="256" autocomplete="off"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="regPassword2" ControlToValidate="txtPassword2"
                                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < and > are invalid characters."
                                        ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                    <asp:CompareValidator ID="cmpValPassword2" runat="server" ControlToCompare="txtNewPwd"
                                        ControlToValidate="txtPassword2" Display="Dynamic" ErrorMessage="<br>Passwords do not match."></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 40px">
                                </td>
                            </tr>
                            <tr id="trUser" runat="server">
                                <td>
                                </td>
                                <td align="left" colspan="2">
                                    <span>* Please click on Submit to notify the administrators on password expiration.</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 40px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="2">
                                    <table width="70%">
                                        <tr>
                                            <td align="right">
                                                <button id="btnCancel" type="button" class="altMedium0BlueButtonFormat" onclick="javascript:return fnLoginPage()">
                                                    Cancel</button>
                                                <br />
                                            </td>
                                            <td align="left">
                                                <button id="btnSubmit" class="altMedium0BlueButtonFormat" style="width: 100pt" onserverclick="SubmitAction"
                                                    runat="server">
                                                    Submit</button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        </form>
    </center>
</body>
</html>

<script type="text/javascript" src="inc/softedge.js">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };  </script>

<%--<!-- #INCLUDE FILE="inc/mainbottom2.aspx" -->--%>
