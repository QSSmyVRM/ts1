
Declare @tzone as int, @confid as int, @instanceid as int, @chDate as datetime
Declare @chTime as datetime, @setupTime as datetime, @tearTime as datetime

DECLARE getDSTCursor CURSOR for

select a.timezone, a.confid, a.instanceid
, dbo.changeToGMTTime(a.timezone,dateadd(minute, -t.Bias, a.confdate)) as changeddate
, dbo.changeToGMTTime(a.timezone,dateadd(minute, -t.Bias, a.conftime)) as changedtime
, dbo.changeToGMTTime(a.timezone,dateadd(minute, -t.Bias, a.SetupTime)) as changedSetupTime
, dbo.changeToGMTTime(a.timezone,dateadd(minute, -t.Bias, a.TearDownTime)) as changedTearDownTime
from Conf_Conference_D a , gen_timezone_s t where a.timezone = t.timezoneid and t.DST = 1
and status in (0,1)and confdate >= getdate()
--and settingtime < '2014-03-10 00:00:00.000' --For Disney
order by a.confid , a.instanceid
OPEN getDSTCursor

FETCH NEXT FROM getDSTCursor INTO  @tzone, @confid, @instanceid, @chDate, @chTime, @setupTime, @tearTime
WHILE @@FETCH_STATUS = 0
BEGIN

update Conf_Conference_D set confdate = @chDate, conftime = @chTime, SetupTime = @setupTime, TearDownTime = @tearTime
where confid = @confid  and instanceid = @instanceid

update Conf_Room_D set StartDate = @chDate where ConfID = @confid and instanceID = @instanceid

if(@instanceid = 1)
Begin
	update Conf_RecurInfo_D set startTime = @chDate where confid = @confid 
End

FETCH NEXT FROM getDSTCursor INTO @tzone, @confid, @instanceid, @chDate, @chTime, @setupTime, @tearTime
END
CLOSE getDSTCursor
DEALLOCATE getDSTCursor
