//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using myVRM.DataLayer;
using System.Data; //FB 2027
//ZD 100151 Start
using System.Xml.XPath;
using System.IO;
using System.Xml.Linq;
//ZD 100151 End

namespace myVRM.BusinessLayer
{
    /// <summary>
    /// Data Layer Logic for loading/saving Rooms data objects
    /// </summary>
    /// FB 2065
    public class vrmAttributeType //ZD 103569 - Changed image constant as string from int
    {
        public const string RoomOnly = "Room.jpg";
        public const string AudioOnly = "Audio.jpg";
        public const string Video = "Video.jpg";
        public const string Telepresence = "Telepresence.jpg";
        public const string HotdeskingAudio = "HotdeskingAudio.jpg";
        public const string HotdeskingVideo = "HotdeskingVideo.jpg";
        public const string GuestVideo = "GuestVideo.jpg"; //ZD 100619
    }
    public class RoomFactory
    {
        /// <summary>
        /// construct Room factory 
        /// </summary>
        private myVRMException myVRMEx; 
        private static log4net.ILog m_log;
        private string m_configPath;
        private const int defaultOrgId = 11;
        internal int organizationID = 0;

        //ZD 100151 Start  
        XmlWriter xWriter = null;
        XmlWriterSettings xSettings = null;
        XPathNavigator xNavigator = null;
        XPathDocument xDoc = null;
        StringReader xStrReader = null;
        XPathNavigator xNode = null;
        //ZD 100151 End
       
        private conferenceDAO m_confDAO;
        private orgDAO m_OrgDAO;
        private userDAO m_usrDAO;
        private deptDAO m_deptDAO;
        private GeneralDAO m_generalDAO;
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private IConferenceDAO m_IconfDAO;
        private IConfUserDAO m_IconfUser;
        private IConfRoomDAO m_IconfRoom;
        private IUserDao m_IUserDAO;
        private IConfAttrDAO m_IconfAttrDAO;
        private IDeptCustomAttrDao m_IDeptCustDAO;
        private ICustomLangDao m_ICustomAttrLangDAO;
        private IDeptCustomAttrOptionDao m_IDeptCustOptDAO;
        private IRoomDAO m_vrmRoomDAO;//FB 2027(GetOldRoom)
        private IDeptDao m_IdeptDAO;
        private ILocDeptDAO m_IlocDeptDAO;
        private ILocApprovDAO m_ILocApprovDAO;
        private IT3RoomDAO m_IT3DAO;
        private IT2RoomDAO m_IT2DAO;
        private LocationDAO m_locDAO;
        private myVRMSearch m_Search;
        private HardwareFactory m_HardwareFac;
        private UserFactory m_usrFactory;
        private IUserDao m_vrmUserDAO;//FB 2632
        private IStateDAO m_IStateDAO; //FB 2392
        private ICountryDAO m_ICountryDAO;
        private IESPublicRoomDAO m_IESPublicRoomDAO; //FB 2392-WhyGO
        private WorkOrderDAO m_woDAO;//Code added fro Room search
        private InvListDAO m_InvListDAO;//Code added fro Room search
        private InvCategoryDAO m_InvCategoryDAO;//Code added fro Room search
        private IEptDao m_vrmEpt;
        private hardwareDAO m_Hardware;

        List<vrmDeptCustomAttr> custAttrs = null;
        vrmFactory vrmFact = null;

        private IUserDeptDao m_IuserDeptDAO;
        internal OrgData orgInfo;
        private IRoomDAO m_IRoomDAO; //FB 2027
        private ns_SqlHelper.SqlHelper m_roomlayer = null; //FB 2027 DeleteRoom
        private imageFactory vrmImg = null; //FB 2136
        //private ISecBadgeDao m_ISecBadgeDao; //FB 2136
        //private secBadgeDao m_secBadgeDao; //FB 2136
        internal int PublicRoom = -1;
        private int m_iMaxRecords = 20;
        private SystemDAO m_systemDAO; // FB 2724
        private ISysMailDAO m_ISysMailDAO; //FB 2724
        private IOrgDAO m_IOrgDAO;//FB 2724
        private UtilFactory m_utilFactory; //FB 2724
        private Conference m_ConferenceFactory = null;//ZD 100522
        private ISyncLocDAO m_ISyncLocDAO; //ZD 101527
        private IFloorRoomDAO m_IFloorRoomDAO; //ZD 102123
        private IUserDeleteListDAO m_UserDeleteListDAO; //ZD 103878

        #region Constructor
        /// <summary>
        /// RoomFactory - Constructor
        /// </summary>
        /// <param name="obj"></param>
        public RoomFactory(ref vrmDataObject obj)
        {
            try
            {
                m_log = obj.log;
                m_configPath = obj.ConfigPath;

                m_confDAO = new conferenceDAO(m_configPath, m_log);
                m_OrgDAO = new orgDAO(m_configPath, m_log);
                m_usrDAO = new userDAO(m_configPath, m_log);
                m_deptDAO = new deptDAO(m_configPath, m_log);
                m_locDAO = new LocationDAO(obj.ConfigPath, obj.log);//FB 2027(GetOldRoom)
                m_generalDAO = new GeneralDAO(m_configPath, m_log);
                m_Hardware = new hardwareDAO(m_configPath, m_log);
                m_utilFactory = new UtilFactory(ref obj);//FB 2724

                vrmFact = new vrmFactory(ref obj);
                m_Search = new myVRMSearch(obj);//FB 2027(GetOldRoom)
                m_HardwareFac = new HardwareFactory(ref obj);
                m_usrFactory = new UserFactory(ref obj);
                m_vrmRoomDAO = m_locDAO.GetRoomDAO();
                m_IdeptDAO = m_deptDAO.GetDeptDao();
                m_IlocDeptDAO = m_locDAO.GetLocDeptDAO();
                m_ILocApprovDAO = m_locDAO.GetLocApprovDAO();
                m_IT3DAO = m_locDAO.GetT3RoomDAO();
                m_IT2DAO = m_locDAO.GetT2RoomDAO();
                m_IStateDAO = m_generalDAO.GetStateDAO(); //FB 2392
                m_ICountryDAO = m_generalDAO.GetCountryDAO();
                m_IESPublicRoomDAO = m_locDAO.GetESPublicRoomDAO(); //FB 2392-WhyGO
                m_woDAO = new WorkOrderDAO(m_configPath, m_log);//Code added for room search
                m_InvListDAO = m_woDAO.GetInvListDAO();//Code added for room search
                m_InvCategoryDAO = m_woDAO.GetCategoryDAO();//Code added for room search
                m_vrmEpt = m_Hardware.GetEptDao();
                m_IOrgSettingsDAO = m_OrgDAO.GetOrgSettingsDao();
                m_IconfDAO = m_confDAO.GetConferenceDao();
                m_IconfRoom = m_confDAO.GetConfRoomDao();
                m_IconfUser = m_confDAO.GetConfUserDao();
                m_IUserDAO = m_usrDAO.GetUserDao();
                m_IconfAttrDAO = m_confDAO.GetConfAttrDao();
                m_IDeptCustDAO = m_deptDAO.GetDeptCustomAttrDao();
                m_ICustomAttrLangDAO = m_deptDAO.GetCustomLangDao();
                m_IDeptCustOptDAO = m_deptDAO.GetDeptCustomAttrOptionDao();

                m_IuserDeptDAO = m_deptDAO.GetUserDeptDao(); //FB 2027
                m_IRoomDAO = m_locDAO.GetRoomDAO();
                m_IT3DAO = m_locDAO.GetT3RoomDAO();
                m_IT2DAO = m_locDAO.GetT2RoomDAO();
                m_vrmUserDAO = m_usrDAO.GetUserDao();//FB 2632

                //m_secBadgeDao = new secBadgeDao(m_configPath, m_log); //FB 2136
                //m_ISecBadgeDao = m_secBadgeDao.GetSecImageDao();//FB 2136
                vrmImg = new imageFactory(ref obj); //FB 2136
                m_systemDAO = new SystemDAO(m_configPath, m_log); //FB 2724
                m_ISysMailDAO = m_systemDAO.GetSysMailDao();//FB 2724
                m_IOrgDAO = m_OrgDAO.GetOrgDao(); //FB 2274
                m_ISyncLocDAO = m_locDAO.GetSyncLocDAO(); //ZD 101527
                m_IFloorRoomDAO = m_locDAO.GetFloorRoomDAO(); //ZD 102123
                m_UserDeleteListDAO = m_usrDAO.GetUserDeleteListDAO();//ZD 103878
            }
            catch(Exception ex)
            {
                m_log.Error("RoomFactory Constructor :" + ex.Message);
                throw ex;
            }
        }
        #endregion

        #region GetRoomMonthlyView
        /// <summary>
        /// GetRoomMonthlyView
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomMonthlyView(ref vrmDataObject obj)
        {
            StringBuilder outXML = new StringBuilder(); //ZD 100151
            try
            {
                DateTime calDate = DateTime.Now;
                XmlDocument xd = new XmlDocument();

                xd.LoadXml(obj.inXml);
                XmlNode node;
                
                node = xd.SelectSingleNode("//calendarView/date");
                if (node != null)
                    DateTime.TryParse(node.InnerText.Trim(), out calDate);

                DateTime fromDate = new DateTime(calDate.Year, calDate.Month, 1, 0, 0, 0);
                DateTime NextMonth = calDate.AddMonths(1); //FB 2368
                DateTime toDate = new DateTime(calDate.Year, calDate.Month, calDate.AddMonths(1).AddDays(-NextMonth.Day).Day, 23, 59, 59); //FB 2368

                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                //ZD 100151
                using (xWriter = XmlWriter.Create(outXML, xSettings))
                {
                    xWriter.WriteStartElement("monthlyView");
                    GetCalendarView(obj, fromDate, toDate, 0, ref xWriter);
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.Flush();
                    obj.outXml = outXML.ToString();
                }
                //ZD 100151
                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetRoomMonthlyView :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch(Exception ex)
            {
                m_log.Error("GetRoomMonthlyView :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetRoomWeeklyView
        /// <summary>
        /// GetRoomWeeklyView
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomWeeklyView(ref vrmDataObject obj)
        {
            StringBuilder outXML = new StringBuilder();//ZD 100151
            try
            {
                DateTime fromDate = DateTime.Now;
                DateTime toDate = DateTime.Now;
                DateTime calDate = DateTime.Now;
                XmlDocument xd = new XmlDocument();

                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//calendarView/date");
                if (node != null)
                    DateTime.TryParse(node.InnerText.Trim(), out calDate);

                int thisDay = (int)calDate.DayOfWeek;
                fromDate = calDate.AddDays(-thisDay);
                toDate = fromDate.AddDays(6);
                fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
                toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);

                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                //ZD 100151
                using (xWriter = XmlWriter.Create(outXML, xSettings))
                {
                    xWriter.WriteStartElement("weeklyView");
                    GetCalendarView(obj, fromDate, toDate, 0, ref xWriter);
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.Flush();
                    obj.outXml = outXML.ToString();
                }
                //ZD 100151
                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetRoomWeeklyView :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("GetRoomWeeklyView :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetRoomDailyView
        /// <summary>
        /// GetRoomDailyView
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomDailyView(ref vrmDataObject obj)
        {
            try
            {
                DateTime calDate = DateTime.Now;
                XmlDocument xd = new XmlDocument();
                StringBuilder outXML = new StringBuilder(); //ZD 100151

                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//calendarView/date");
                if (node != null)
                    DateTime.TryParse(node.InnerText.Trim(), out calDate);

                DateTime fromDate = new DateTime(calDate.Year, calDate.Month, calDate.Day, 0, 0, 0);
                DateTime toDate = new DateTime(calDate.Year, calDate.Month, calDate.Day, 23, 59, 59);
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                //ZD 100151
                using (xWriter = XmlWriter.Create(outXML, xSettings))
                {
                    xWriter.WriteStartElement("dailyView");
                    GetCalendarView(obj, fromDate, toDate, 0, ref xWriter);
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.Flush();
                    obj.outXml = outXML.ToString();
                }
                //ZD 100151
                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetRoomDailyView :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("GetRoomDailyView :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetCalendarView
        /// <summary>
        /// GetCalendarView
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        private bool GetCalendarView(vrmDataObject obj, DateTime fromDate, DateTime toDate, int Access,ref XmlWriter xWriter) //FB 2724 //ZD 100151
        {
            try
            {
                int userID = 11, RoomID = 0, utcEnabled = 0, Cfrist = 1, LoginUserId = 0;//FB 2724
                List<ICriterion> CritList = new List<ICriterion>();
                List<vrmConference> confs = new List<vrmConference>();
                timeZoneData tzData = new timeZoneData();
                StringBuilder outXML1 = new StringBuilder(); //ZD 100151
                DateTime calDate = DateTime.Now;
                DateTime PerStTime = DateTime.Now, PerEndTime = DateTime.Now; //ZD 100157
                DateTime RoomStrtTime = DateTime.Now, RoomEndTime = DateTime.Now; //ZD 100157
                String temp = "", roomUID = "", ConfType = "", Hostname = "", isDeletedConf = "";//FB 2724
                List<vrmConfRoom> confRooms = null;
                ICriterion criterium = null;
                vrmRoom Loc = null;//FB 2724
                vrmBaseUser vrmUser = null;
                OrgData org = null;
                DateTime setUpTime = DateTime.Now;
                DateTime tearDownTime = DateTime.Now;
                DateTime confdate = DateTime.Now;
                DateTime gmtConfDate = DateTime.Now;
				//ZD 100085 Starts
                DateTime MCUPreStart = DateTime.Now;
                DateTime MCUPreEnd = DateTime.Now;

                #region Reading XML
                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//calendarView/organizationID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out organizationID);


                        if (organizationID < defaultOrgId)
                        {
                            myVRMEx = new myVRMException(423);
                            throw myVRMEx;
                        }

                        xNode = xNavigator.SelectSingleNode("//calendarView/utcEnabled");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out utcEnabled);

                        xNode = xNavigator.SelectSingleNode("//calendarView/userID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out userID);

                        xNode = xNavigator.SelectSingleNode("//calendarView/room");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out RoomID);

                        //FB 2724 Start
                        xNode = xNavigator.SelectSingleNode("//calendarView/LoginUserId");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out LoginUserId);

                        if (userID == 0)
                            userID = LoginUserId;

                        xNode = xNavigator.SelectSingleNode("//calendarView/roomUID");
                        if (xNode != null)
                            roomUID = xNode.Value.Trim();

                        xNode = xNavigator.SelectSingleNode("//calendarView/isDeletedConf");
                        if (xNode != null)
                            isDeletedConf = xNode.Value.Trim();
                    }
                }
                #endregion

                if (roomUID != "")
                {
                    Loc = m_vrmRoomDAO.GetByRoomUID(roomUID);
                    if (Loc != null)
                        RoomID = Loc.RoomID;
                    else
                    {
                        obj.outXml = "No Room Found";
                        return false;
                    }
                }
                //FB 2724 End

                vrmUser = m_IUserDAO.GetByUserId(userID);
                timeZone.changeToGMTTime(vrmUser.TimeZone, ref fromDate);
                timeZone.changeToGMTTime(vrmUser.TimeZone, ref toDate); //sysSettings.TimeZone

                criterium = (Expression.Or(
                             (Expression.And(Expression.Ge("confdate", fromDate), Expression.Le("confdate", toDate))),
                             (Expression.And(Expression.Gt("confEnd", fromDate), Expression.Le("confdate", toDate))))
                            );
                criterium = Expression.And(Expression.Eq("orgId", organizationID), criterium);

                //ZD 100151
                fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
                toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);


                if (isDeletedConf == "0")
                    criterium = Expression.And(Expression.Eq("deleted", 0), criterium);

                if (RoomID != 0)
                {
                    CritList.Add(Expression.Eq("roomId", RoomID));
                    m_IconfRoom.addOrderBy(Order.Asc("StartDate"));

                    confRooms = m_IconfRoom.GetByCriteria(CritList);
                    for (int i = 0; i < confRooms.Count; i++)
                    {
                        CritList = new List<ICriterion>();
                        CritList.Add(criterium);
                        CritList.Add(Expression.Eq("confid", confRooms[i].confid));
                        CritList.Add(Expression.Eq("instanceid", confRooms[i].instanceid));
                        CritList.Add(Expression.Eq("Permanent", 0)); //ZD 100522
                        confs.AddRange(m_IconfDAO.GetByCriteria(CritList));
                    }
                }
                else
                {
                    CritList = new List<ICriterion>();
                    CritList.Add(criterium);
                    CritList.Add(Expression.Eq("Permanent", 0)); //ZD 100522
                    m_IconfDAO.addOrderBy(Order.Asc("confdate"));
                    confs.AddRange(m_IconfDAO.GetByCriteria(CritList));
                }

                org = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                timeZone.GetTimeZone(vrmUser.TimeZone, ref tzData);

                #region WriteXML

                xWriter.WriteElementString("timezoneName", tzData.TimeZone.ToString());
                if (org != null)
                {
                    xWriter.WriteStartElement("systemAvail");
                    xWriter.WriteElementString("open24", org.Open24hrs.ToString());
                    xWriter.WriteStartElement("startTime");
                    xWriter.WriteElementString("startHour", org.SystemStartTime.ToString("hh"));
                    xWriter.WriteElementString("startMin", org.SystemStartTime.ToString("mm"));
                    xWriter.WriteElementString("startSet", org.SystemStartTime.ToString("tt"));
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.WriteStartElement("endTime");
                    xWriter.WriteElementString("endHour", org.SystemEndTime.ToString("hh"));
                    xWriter.WriteElementString("endMin", org.SystemEndTime.ToString("mm"));
                    xWriter.WriteElementString("endSet", org.SystemEndTime.ToString("tt"));
                    xWriter.WriteFullEndElement(); //ZD 101086
                    //ZD 101086 Startss //ZD 100619
                    xWriter.WriteStartElement("dayClosed");
                    xWriter.WriteString(org.Offdays.ToString());
                    xWriter.WriteFullEndElement();
                    //xWriter.WriteElementString("dayClosed", org.Offdays.ToString());
                    //ZD 100157 Starts
                    xWriter.WriteStartElement("Personal");
                    xWriter.WriteElementString("ShowPerHrs", vrmUser.PerCalShowHours.ToString());
                    xWriter.WriteStartElement("startTime");
                    DateTime.TryParse(vrmUser.PerCalStartTime.ToString(), out PerStTime);
                    timeZone.GMTToUserPreferedTime(vrmUser.TimeZone, ref PerStTime);
                    xWriter.WriteElementString("startHour", PerStTime.ToString("hh"));
                    xWriter.WriteElementString("startMin", PerStTime.ToString("mm"));
                    xWriter.WriteElementString("startSet", PerStTime.ToString("tt"));
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.WriteStartElement("endTime");
                    DateTime.TryParse(vrmUser.PerCalEndTime.ToString(), out PerEndTime);
                    timeZone.GMTToUserPreferedTime(vrmUser.TimeZone, ref PerEndTime);
                    xWriter.WriteElementString("endHour", PerEndTime.ToString("hh"));
                    xWriter.WriteElementString("endMin", PerEndTime.ToString("mm"));
                    xWriter.WriteElementString("endSet", PerEndTime.ToString("tt"));
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.WriteStartElement("Room");
                    xWriter.WriteElementString("ShowRoomHrs", vrmUser.RoomCalShowHours.ToString());
                    xWriter.WriteStartElement("startTime");
                    DateTime.TryParse(vrmUser.RoomCalStartTime.ToString(), out RoomStrtTime);
                    timeZone.GMTToUserPreferedTime(vrmUser.TimeZone, ref RoomStrtTime);
                    xWriter.WriteElementString("startHour", RoomStrtTime.ToString("hh"));
                    xWriter.WriteElementString("startMin", RoomStrtTime.ToString("mm"));
                    xWriter.WriteElementString("startSet", RoomStrtTime.ToString("tt"));
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.WriteStartElement("endTime");
                    DateTime.TryParse(vrmUser.RoomCalEndime.ToString(), out RoomEndTime);
                    timeZone.GMTToUserPreferedTime(vrmUser.TimeZone, ref RoomEndTime);
                    xWriter.WriteElementString("endHour", RoomEndTime.ToString("hh"));
                    xWriter.WriteElementString("endMin", RoomEndTime.ToString("mm"));
                    xWriter.WriteElementString("endSet", RoomEndTime.ToString("tt"));
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.WriteFullEndElement(); //ZD 101086
                    //ZD 100157 Ends
                }
                if (confs.Count <= 0)
                {
                    xWriter.WriteStartElement("days");
                    xWriter.WriteStartElement("day");
                    xWriter.WriteElementString("date", fromDate.ToString("MM/dd/yyyy"));
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.WriteFullEndElement(); //ZD 101086
                }
                else
                {
                    for (int i = 0; i < confs.Count; ++i)
                    {
                        vrmConference vrmconff = confs[i];

                        if (RoomID != 0 || vrmconff.isPublic == 1 || vrmconff.owner == userID || vrmUser.Admin == 2 || vrmUser.Admin == 1)
                        {
                            int future = 0, ongoing = 0, ispublic = 0, isPending = 0, isAppr = 0, cnt = 0, onMCU = 0, WaitList = 0;//ZD 100036 //ZD 102532
                            double duration = 0.0;

                            confdate = vrmconff.confdate;
                            setUpTime = vrmconff.SetupTime;
                            tearDownTime = vrmconff.TearDownTime;
                            gmtConfDate = vrmconff.confdate;
							MCUPreStart = vrmconff.MCUPreStart;//ZD 100085
                            MCUPreEnd = vrmconff.MCUPreEnd;

                            if (utcEnabled == 0)
                            {
                                timeZone.userPreferedTime(vrmUser.TimeZone, ref setUpTime);
                                timeZone.userPreferedTime(vrmUser.TimeZone, ref tearDownTime);
                                timeZone.userPreferedTime(vrmUser.TimeZone, ref confdate);
                                timeZone.userPreferedTime(vrmUser.TimeZone, ref MCUPreStart);
                                timeZone.userPreferedTime(vrmUser.TimeZone, ref MCUPreEnd);
								//ZD 100085 End
                            }

                            //if (RoomID == 0) //FB 2961
                            //{
                            //    confdate = setUpTime;
                            //    gmtConfDate = vrmconff.SetupTime;
                            //    Int32.TryParse((tearDownTime - setUpTime).TotalMinutes.ToString(), out cnt);
                            //    vrmconff.duration = cnt;
                            //}

                            if (vrmconff.status != 7 && vrmconff.status != 9) //Completed
                            {
                                if (vrmconff.status == 0) //ZD 100093 //ZD 100036
                                    future = 1;

                                if (vrmconff.status == 6) //ZD 100036
                                    onMCU = 1;

                                if (vrmconff.status == 5)
                                    ongoing = 1;
                                else
                                {
                                    calDate = DateTime.Now;
                                    timeZone.changeToGMTTime(vrmconff.timezone, ref calDate);
                                    if (calDate >= gmtConfDate && vrmconff.status == 0)
                                    {
                                        future = 0;

                                        if (vrmconff.TearDownTime < calDate)
                                            vrmconff.status = 7;
                                        else
                                            ongoing = 1;
                                    }
                                }

                                if (vrmconff.status == 0 && vrmconff.isPublic == 1)
                                    ispublic = 1;

                                if (vrmconff.status == 1)
                                {
                                    isPending = 1;
                                    isAppr = 1;
                                }
                                
                                //ZD 102532
                                if (vrmconff.status == vrmConfStatus.WaitList && vrmconff.conftype == vrmConfType.HotDesking)
                                    WaitList = 1;
                            }

                            if (i == 0 || Cfrist == 1 || (confs[i - 1].confdate != confdate))
                            {
                                if (i != 0 && Cfrist != 1)
                                {
                                    xWriter.WriteFullEndElement(); //ZD 101086
                                    xWriter.WriteFullEndElement(); //ZD 101086
                                    xWriter.WriteFullEndElement(); //ZD 101086
                                }

                                xWriter.WriteStartElement("days");
                                xWriter.WriteStartElement("day");
                                xWriter.WriteElementString("date", confdate.ToString("MM/dd/yyyy"));
                                xWriter.WriteStartElement("conferences");
                                Cfrist = 2;
                            }

                            xWriter.WriteStartElement("conference");
                            xWriter.WriteElementString("confID", vrmconff.confid + "," + vrmconff.instanceid);
                            xWriter.WriteElementString("uniqueID", vrmconff.confnumname.ToString());
                            xWriter.WriteElementString("confName", vrmconff.externalname);
                            xWriter.WriteElementString("ConferenceType", vrmconff.conftype.ToString());

                            //FB 2724 Start
                            ConfType = "";
                            switch (vrmconff.conftype)
                            {
                                case 7:
                                    ConfType = "Room Conference";
                                    break;
                                case 6:
                                    ConfType = "Audio Only Conference";
                                    break;
                                case 2:
                                    ConfType = "Audio/Video Conference";
                                    break;
                                case 4:
                                    ConfType = "Point-To-Point Conference";
                                    break;
                                case 8:
                                    ConfType = "Hotdesking Conference";
                                    break;
                            }
                            //if (vrmconff.conftype == 7)
                            //    ConfType = "Room Conference";
                            //else if (vrmconff.conftype == 6)
                            //    ConfType = "Audio Only Conference";
                            //else if (vrmconff.conftype == 2)
                            //    ConfType = "Audio/Video Conference";
                            //else if (vrmconff.conftype == 4)
                            //    ConfType = "Point-To-Point Conference";
                            //else if (vrmconff.conftype == 8)
                            //    ConfType = "Hotdesking Conference";
                            xWriter.WriteElementString("ConferenceTypeName", ConfType);
                            //FB 2724 End

                            xWriter.WriteElementString("deleted", vrmconff.deleted.ToString());
                            xWriter.WriteElementString("isVIP", vrmconff.isVIP.ToString());
                            xWriter.WriteElementString("isVMR", vrmconff.isVMR.ToString()); //FB 2448
                            //ZD 101086 Startss //ZD 100619
                            xWriter.WriteStartElement("confPassword");
                            xWriter.WriteString(vrmconff.password);
                            xWriter.WriteFullEndElement();
                            xWriter.WriteStartElement("icalID");
                            xWriter.WriteString(vrmconff.IcalID);
                            xWriter.WriteFullEndElement();
                            //xWriter.WriteElementString("confPassword", vrmconff.password); //FB 2622
                            //xWriter.WriteElementString("icalID", vrmconff.IcalID);//FB 2149
                            //ZD 101086 Ends
                            //ZD 100963 START
                            vrmUser host = new vrmUser();
                            //ZD 103878
                            if (vrmconff.UserIDRefText != null && vrmconff.UserIDRefText.ToString().Trim().IndexOf('H') >= 0)
                            {
                                vrmUserDeleteList usrDelList = m_UserDeleteListDAO.GetByuId(vrmconff.owner);
                                m_usrFactory.SetDeletedUserDetails(ref host, ref usrDelList);                                
                            }
                            else
                                host = m_vrmUserDAO.GetByUserId(vrmconff.owner);
                            xWriter.WriteElementString("WorkPhone", host.WorkPhone);
                            xWriter.WriteElementString("CellPhone", host.CellPhone);//FB 2149
                            //ZD 100963 END

                            calDate = vrmconff.confEnd;
                            timeZone.userPreferedTime(vrmUser.TimeZone, ref calDate);
                            vrmconff.confEnd = calDate;

                            if ((vrmconff.duration >= 1440 || vrmconff.confEnd.Day > confdate.Day) && vrmconff.confEnd <= toDate && (toDate - fromDate).TotalMinutes < 1440)
                                Double.TryParse((vrmconff.confEnd - fromDate).TotalMinutes.ToString(), out duration);
                            else
                                Double.TryParse(vrmconff.duration.ToString(), out duration);

                            duration = Math.Round(duration, 0); //ZD 100681

                            xWriter.WriteElementString("confDate", confdate.ToString("MM/dd/yyyy"));

                            temp = confdate.ToString("hh:mm tt");
                            if (confdate < fromDate)
                                temp = "00:00 AM";

                            xWriter.WriteElementString("confTime", temp);

                            temp = setUpTime.ToString("hh:mm tt");
                            if (setUpTime < fromDate)
                                temp = "00:00 AM";

                            xWriter.WriteElementString("setupTime", temp);

                            temp = tearDownTime.ToString("hh:mm tt");
                            if (tearDownTime < fromDate)
                                temp = "00:00 AM";

                            xWriter.WriteElementString("teardownTime", temp);

                            temp = "0";
                            //ZD 100151
                            //for (cnt = 0; cnt < vrmconff.ConfUser.Count; cnt++)
                            //    if (vrmconff.ConfUser[cnt].userid == userID)
                            //        temp = userID.ToString();
                           
							//ZD 100085 Starts   
                            xWriter.WriteElementString("setupDur", vrmconff.SetupTimeinMin.ToString());
                            xWriter.WriteElementString("teardownDur", vrmconff.TearDownTimeinMin.ToString());
                            xWriter.WriteElementString("MCUPreStartDur", vrmconff.McuSetupTime.ToString());
                            xWriter.WriteElementString("MCUPreEndDur", vrmconff.MCUTeardonwnTime.ToString());
							//ZD 100085 End
                            xWriter.WriteElementString("durationMin", duration.ToString());
                            xWriter.WriteElementString("owner", vrmconff.owner.ToString());
                            xWriter.WriteElementString("requestor", vrmconff.userid.ToString()); //ZD 102832
                            if (vrmconff.owner == userID || (vrmUser.Admin == 2 || vrmUser.Admin == 3)) //ZD 101942 
                                xWriter.WriteElementString("hasRightstoEdit", "1");
                            else
                                xWriter.WriteElementString("hasRightstoEdit", "0");
                            xWriter.WriteElementString("ConferenceStatus", vrmconff.status.ToString());//ZD 103051
                            //FB 2724 Start
                            Hostname = "";
                            if (vrmconff.HostName == "")
                            {
                                //ZD 103878
                                //vrmUser owner = m_IUserDAO.GetByUserId(vrmconff.owner);
                                Hostname = host.FirstName + " " + host.LastName;
                            }
                            else
                            {
                                Hostname = vrmconff.HostName;
                            }
                            xWriter.WriteElementString("Hostname", Hostname);
                            //FB 2724 End
                            xWriter.WriteElementString("party", temp);
                            xWriter.WriteStartElement("mainLocation");
                            for (int j = 0; j < vrmconff.ConfRoom.Count; j++)
                            {
                                xWriter.WriteStartElement("location");
                                xWriter.WriteElementString("locationID", vrmconff.ConfRoom[j].roomId.ToString());
                                xWriter.WriteElementString("locationName", vrmconff.ConfRoom[j].Room.Name);
                                xWriter.WriteFullEndElement(); //ZD 101086
                            }
                            xWriter.WriteFullEndElement(); //ZD 101086
                            xWriter.WriteElementString("isImmediate", ongoing.ToString());
                            xWriter.WriteElementString("onMCU", onMCU.ToString()); //ZD 100036
                            xWriter.WriteElementString("isFuture", future.ToString());
                            xWriter.WriteElementString("WaitList", WaitList.ToString());//ZD 102532
                            xWriter.WriteElementString("isPublic", ispublic.ToString());
                            xWriter.WriteElementString("isPending", isPending.ToString());
                            if (Access == 0) //FB 2724
                            {
                                xWriter.WriteElementString("OnSiteAVSupport", vrmconff.OnSiteAVSupport.ToString()); //FB 2632
                                xWriter.WriteElementString("MeetandGreet", vrmconff.MeetandGreet.ToString());
                                xWriter.WriteElementString("ConciergeMonitoring", vrmconff.ConciergeMonitoring.ToString());
                                //FB 2670 Start
                                if (vrmconff.ConfVNOCOperator.Count > 0)
                                {
                                    xWriter.WriteStartElement("ConfVNOCOperators");
                                    for (int vn = 0; vn < vrmconff.ConfVNOCOperator.Count; vn++)
                                    {
                                        //ZD 100151
                                        //vrmConfVNOCOperator ConfVNOCList = vrmconff.ConfVNOCOperator[vn];
                                        //vrmUser vnocOperator = m_vrmUserDAO.GetByUserId(ConfVNOCList.vnocId);
                                        //if (vnocOperator != null)
                                        //    xWriter.WriteElementString("VNOCOperator", vnocOperator.FirstName + " " + vnocOperator.LastName);
                                        xWriter.WriteElementString("VNOCOperator", vrmconff.ConfVNOCOperator[vn].OperatorName);
                                    }
                                    xWriter.WriteFullEndElement(); //ZD 101086
                                }
                                else //ZD 100619
                                {
                                    xWriter.WriteStartElement("ConfVNOCOperators");
                                    xWriter.WriteString(string.Empty);
                                    xWriter.WriteFullEndElement(); //ZD 101086
                                    //xWriter.WriteElementString("ConfVNOCOperators", "");

                                }
                                //FB 2670 End

                            }
                            xWriter.WriteElementString("isApproval", isAppr.ToString());
                            if (Access == 0 && org.EnableCustomOption == "1" && org.ShowCusAttInCalendar == 1) //FB 2724 //ZD 100151 
                            {
                                vrmFact.organizationID = organizationID; //FB 2045
                                vrmFact.FetchCalenderCustomAttrs(vrmUser, vrmconff, ref xWriter);
                            }
                            xWriter.WriteFullEndElement(); //ZD 101086
                        }
                    }
                    if (Cfrist == 2)
                    {
                        xWriter.WriteFullEndElement(); //ZD 101086
                        xWriter.WriteFullEndElement(); //ZD 101086
                        xWriter.WriteFullEndElement(); //ZD 101086
                    }
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("GetCalendarView" + ex.Message);
                obj.outXml = "";
                return false;
                throw ex;
            }
        }
        #endregion

        //FB 2027(GetOldRoom) - Start
        #region GetOldRoom
        /// <summary>
        /// GetOldRoom (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetOldRoom(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0, roomid = 0,locasstUserID=0; //ZD 100619
            string locAsstName = "", locGuestEmail = "", locGuestPhone = "";//ZD 100619
            vrmUser userInfo =null; //ZD 100619
            StringBuilder tempXML = new StringBuilder();
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionList1 = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/roomID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out roomid);

                outXml.Append("<room>");
                vrmRoom Loc = m_vrmRoomDAO.GetByRoomId(roomid);
                outXml.Append("<roomID>" + Loc.RoomID.ToString() + "</roomID>");
                outXml.Append("<roomName>" + Loc.Name + "</roomName>");
                Fetch32LvLocations(userid, roomid, 0, organizationID, obj, ref tempXML);
                outXml.Append(tempXML);
                outXml.Append("<floor>" + Loc.RoomFloor + "</floor>");
                outXml.Append("<number>" + Loc.RoomNumber + "</number>");
                outXml.Append("<phone>" + Loc.RoomPhone + "</phone>");
                outXml.Append("<capacity>" + Loc.Capacity + "</capacity>");
                
               
                //ZD 100619 Starts
                if(Loc.assistant >11)
                {
                    locasstUserID = Loc.assistant;
                    userInfo = m_IUserDAO.GetByUserId(locasstUserID);
                    locAsstName = userInfo.FirstName + " " + userInfo.LastName;
                }
                else if (!string.IsNullOrEmpty(Loc.GuestContactEmail))
                {
                    locasstUserID = 0;
                    locAsstName = Loc.GuestContactName;
                    locGuestEmail = Loc.GuestContactEmail;
                    locGuestPhone = Loc.GuestContactPhone;

                }
                outXml.Append("<assistant>");
                outXml.Append("<assistantID>" + locasstUserID.ToString() + "</assistantID>");
                outXml.Append("<assistantName>" + locAsstName + "</assistantName>");
                outXml.Append("<assistantGuestEmail>" + locGuestEmail + "</assistantGuestEmail>");
                outXml.Append("<assistantGuestPhone>" + locGuestPhone + "</assistantGuestPhone>");
                outXml.Append("</assistant>");
                 //ZD 100619 Ends
                //ZD 101244 start
                outXml.Append("<Secure>" + Loc.Secure + "</Secure>");
                outXml.Append("<Securityemail>" + Loc.Securityemail + "</Securityemail>");
                outXml.Append("<UsersSecurityemail>" + Loc.UsersSecurityemail + "</UsersSecurityemail>");
                //ZD 101244 End
                outXml.Append("<dynamicRoomLayout>" + Loc.DynamicRoomLayout + "</dynamicRoomLayout>");
                outXml.Append("<catererFacility>" + Loc.Caterer + "</catererFacility>");
                outXml.Append("<projector>" + Loc.ProjectorAvailable.ToString() + "</projector>");
                outXml.Append("<maxNumConcurrent>" + Loc.MaxPhoneCall.ToString() + "</maxNumConcurrent>");
                outXml.Append("<roomImage>" + Loc.RoomImage + "</roomImage>");

                outXml.Append("<mapFile></mapFile>");
                outXml.Append("<secPassFile></secPassFile>");
                outXml.Append("<miscAttachFile></miscAttachFile>");
                outXml.Append("<endpoint>" + Loc.endpointid.ToString() + "</endpoint>");
                outXml.Append("<video>" + Loc.VideoAvailable.ToString() + "</video>"); 
                //outXml.Append("<setupTime>" + Loc.SetupTime.ToString() + "</setupTime>");//ZD 101563
                //outXml.Append("<teardownTime>" + Loc.TeardownTime.ToString() + "</teardownTime>");
                outXml.Append("<notificationEmails>" + Loc.notifyemails + "</notificationEmails>");
                outXml.Append("<IsVMR>" + Loc.IsVMR + "</IsVMR>"); //FB 2448
                outXml.Append("<VMRBridgeID>" + Loc.BridgeId + "</VMRBridgeID>"); //ZD 100522
                outXml.Append("<audioProtocols>");
                List<vrmAudioAlg> audiolist = vrmGen.getAudioAlg();
                for (int i = 0; i < audiolist.Count; i++)
                {
                    outXml.Append("<audioProtocol>");
                    outXml.Append("<audioProtocolID>" + audiolist[i].Id.ToString() + "</audioProtocolID>");
                    outXml.Append("<audioProtocolName>" + audiolist[i].AudioType + "</audioProtocolName>");
                    outXml.Append("</audioProtocol>");
                }
                outXml.Append("</audioProtocols>");

                tempXML = new StringBuilder();
                m_Search.GetVideoSessionList(0, ref tempXML);
                outXml.Append(tempXML);
                outXml.Append("<videoEquipment>");
                outXml.Append("<default>" + Loc.DefaultEquipmentid.ToString() + "</default>");
                List<vrmVideoEquipment> videoList = vrmGen.getVideoEquipment();
                for (int k = 0; k < videoList.Count; k++)
                {
                    outXml.Append("<equipment>");
                    outXml.Append("<videoEquipmentID>" + videoList[k].Id.ToString() + "</videoEquipmentID>");
                    outXml.Append("<videoEquipmentName>" + videoList[k].VEName + "</videoEquipmentName>");
                    outXml.Append("</equipment>");
                }
                outXml.Append("</videoEquipment>");

                tempXML = new StringBuilder();
                FetchDepartmentsForRoom(userid, roomid, ref tempXML);
                m_HardwareFac.OldFetchBridgeList(organizationID, ref tempXML);
                GetRoomApprovers(roomid, ref tempXML);
                outXml.Append(tempXML);

                tempXML = new StringBuilder();
                m_usrFactory.FetchAddressTypeList(obj, ref tempXML);
                //outXml.Append(tempXML); //FB 2594-Fetching all endpoints-Not used.
                //m_HardwareFac.FetchEndpointList(organizationID, ref tempXML);
                //outXml.Append(tempXML);
                outXml.Append("</room>");

                obj.outXml = outXml.ToString();
            }
            catch (myVRMException ex)
            {
                m_log.Error("vrmException", ex);
                obj.outXml = ex.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region FetchDepartmentsForRoom
        /// <summary>
        /// FetchDepartmentsForRoom (COM to .Net conversion) 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchDepartmentsForRoom(int userid, int roomid, ref StringBuilder outXml)
        {
            outXml = new StringBuilder();
            List<ICriterion> criterionList1 = new List<ICriterion>();
            List<ICriterion> criterionList2 = new List<ICriterion>();
            try
            {
                OrgData orgdata = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                outXml.Append("<multiDepartment>" + orgdata.MultipleDepartments.ToString() + "</multiDepartment>");
                vrmUser userInfo = m_IUserDAO.GetByUserId(userid);

                outXml.Append("<departments>");
                criterionList1.Add(Expression.Eq("roomId", roomid));
                List<vrmLocDepartment> locdept = m_IlocDeptDAO.GetByCriteria(criterionList1);
                for (int i = 0; i < locdept.Count; i++)
                    outXml.Append("<selected>" + locdept[i].departmentId.ToString() + "</selected>");

                List<vrmDept> depInfo;
                if (userInfo.isSuperAdmin())
                {
                    depInfo = m_IdeptDAO.GetActive();
                    for (int j = 0; j < depInfo.Count; j++)
                    {
                        outXml.Append("<department>");
                        outXml.Append("<id>" + depInfo[j].departmentId.ToString() + "</id>");
                        outXml.Append("<name>" + depInfo[j].departmentName + "</name>");
                        outXml.Append("</department>");
                    }
                }
                else
                {
                    for (int i = 0; i < locdept.Count; i++)
                    {
                        criterionList2 = new List<ICriterion>();
                        criterionList2.Add(Expression.Eq("departmentId", locdept[i].departmentId));
                        criterionList2.Add(Expression.Eq("deleted", 0));
                        depInfo = m_IdeptDAO.GetByCriteria(criterionList2);
                        for (int j = 0; j < depInfo.Count; j++)
                        {
                            outXml.Append("<department>");
                            outXml.Append("<id>" + depInfo[j].departmentId.ToString() + "</id>");
                            outXml.Append("<name>" + depInfo[j].departmentName + "</name>");
                            outXml.Append("</department>");
                        }
                    }
                }
                outXml.Append("</departments>");
            }
            catch (Exception ex)
            {
                m_log.Error("FetchDepartmentsForRoom :" + ex.Message);
                throw ex;
            }
            return true;
        }
        #endregion

        #region GetRoomApprovers
        /// <summary>
        /// GetRoomApprovers (COM to .Net conversion) 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool GetRoomApprovers(int roomid, ref StringBuilder outXml)
        {
            outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                vrmRoom Loc = m_vrmRoomDAO.GetByRoomId(roomid);
                outXml.Append("<approvers>");
                outXml.Append("<responseMsg>" + Loc.ResponseMessage + "</responseMsg>");
                outXml.Append("<responseTime>" + Loc.ResponseTime.ToString() + "</responseTime>");
                criterionList.Add(Expression.Eq("roomid", roomid));
                IList<vrmLocApprover> locApprList = m_ILocApprovDAO.GetByCriteria(criterionList);
                for(int l=0; l < locApprList.Count; l++)
                {
                    vrmUser userInfo = m_IUserDAO.GetByUserId(locApprList[l].approverid);
                    outXml.Append("<approver>");
                    outXml.Append("<ID>" + locApprList[l].approverid.ToString() + "</ID>");
                    outXml.Append("<firstName>" + userInfo.FirstName + "</firstName>");
                    outXml.Append("<lastName>" + userInfo.LastName + "</lastName>");
                    outXml.Append("</approver>");
                }
                outXml.Append("</approvers>");
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("GetRoomApprovers :" + ex.Message);
                throw ex;
            }
        }
        #endregion

        #region Fetch32LvLocations
        /// <summary>
        /// Fetch32LvLocations (COM to .Net conversion) 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool Fetch32LvLocations(int userid, int roomid, int mode, int orgid, vrmDataObject obj, ref StringBuilder locout)
        {
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> critert3 = new List<ICriterion>();
            String stmt = "", stmt1= "";
            outXml.Append("<locationList>");
            try
            {
                OrgData orgInfo = m_IOrgSettingsDAO.GetByOrgId(orgid);
                vrmUser user = m_IUserDAO.GetByUserId(userid);
                vrmRoom Loc = m_vrmRoomDAO.GetByRoomId(roomid);
                outXml.Append("<selected>");
                if (mode == 0)
                    outXml.Append("<level2ID>" + Loc.tier2.ID.ToString() + "</level2ID>");
                else if (mode == 1)
                    outXml.Append("<level2ID>" + roomid + "</level2ID>");
                outXml.Append("</selected>");

                outXml.Append("	<level3List>");
                if ((!user.isSuperAdmin()) && (orgInfo.MultipleDepartments == 1))
                {
                    stmt = "select distinct l3.ID, l3.Name from myVRM.DataLayer.vrmTier3 l3, myVRM.DataLayer.vrmRoom R ";
                    stmt += " where l3.disabled = 0 and l3.ID = R.L3LocationId  and l3.orgId = '" + orgid.ToString() + "'  and R.RoomID in (select crd.roomId from myVRM.DataLayer.vrmLocDepartment crd, myVRM.DataLayer.vrmUserDepartment UD ";
                    stmt += " where crd.departmentId = UD.departmentId and UD.userId = " + userid.ToString() + " )"; //FB 1597
                    stmt += " group by l3.ID, l3.Name";
                }
                else
                {
                    stmt = "select distinct l3.ID, l3.Name from myVRM.DataLayer.vrmTier3 l3 ";
                    stmt += " where l3.disabled = 0  and l3.orgId = '" + orgid.ToString() + "'"; //FB 1597
                }
                stmt += " order by l3.Name ";
                IList tier3 = m_IT3DAO.execQuery(stmt);
                if (tier3.Count > 0)
                {
                    foreach (object[] t3 in tier3)
                    {
                        outXml.Append("<level3>");
                        outXml.Append("<level3ID>" + Convert.ToString(t3[0]) + "</level3ID>");
                        outXml.Append("<level3Name>" + Convert.ToString(t3[1]) + "</level3Name>");
                        outXml.Append("<level2List>");
                        stmt1 = " select ID, Name, Address, Comment from myVRM.DataLayer.vrmTier2 ";
                        stmt1 += "where L3LocationId = " + Convert.ToString(t3[0]) + " and disabled = 0 order by upper(Name)";
                        IList tier2 = m_IT2DAO.execQuery(stmt1);
                        if (tier2.Count > 0)
                        {
                            foreach (object[] t2 in tier2)
                            {
                                outXml.Append("<level2>");
                                outXml.Append("<level2ID>" + Convert.ToString(t2[0]) + "</level2ID>");
                                outXml.Append("<level2Name>" + Convert.ToString(t2[1]) + "</level2Name>");
                                outXml.Append("<level2Address>" + Convert.ToString(t2[2]) + "</level2Address>");
                                outXml.Append("<level2Comment>" + Convert.ToString(t2[3]) + "</level2Comment>");
                                outXml.Append("</level2>");
                            }
                        }
                        outXml.Append("</level2List>");
                        outXml.Append("</level3>");
                    }
                }
                outXml.Append("</level3List>");
                outXml.Append("</locationList>");
                locout.Append(outXml.ToString());
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        //FB 2027(GetOldRoom) - End
		//FB 2027
        #region Fetch3LvTempLocations
        /// <summary>
        /// Fetch3LvTempLocations
        /// </summary>
        /// <param name="lv3list"></param>
        /// <param name="userID"></param>
        public void Fetch3LvTempLocations(int userID, List<int> temprooms, ref StringBuilder outputxml, List<int> T2Ids, List<int> T3Ids) //ZD 102481
        {
            try
            {
                bool isAppend = false;
                StringBuilder str = new StringBuilder();
                List<vrmTier3> tier3List = new List<vrmTier3>();
                //ZD 102481 Starts
                List<vrmTier2> tier2 = new List<vrmTier2>();
                List<vrmRoom> rooms = new List<vrmRoom>();
                List<ICriterion> criterionT2 = new List<ICriterion>();
                List<ICriterion> criterionT3 = new List<ICriterion>();
                List<ICriterion> criterionRooms = new List<ICriterion>();
                vrmTier2 T2 = null;
                vrmRoom room = null;
                //ZD 102481 Ends
                m_Search.organizationID = organizationID;
                //m_Search.GetRoomTree(userID, ref tier3List); //ZD 102481
                //ZD 1024841 Starts
                if (T3Ids.Count > 0)
                {
                    criterionT3 = new List<ICriterion>();
                    criterionT3.Add(Expression.Eq("disabled", 0));
                    criterionT3.Add(Expression.In("ID", T3Ids));
                    tier3List = m_IT3DAO.GetByCriteria(criterionT3);
                    //ZD 1024841 Ends

                    outputxml.Append("<level3List>");
                    for (int t3 = 0; t3 < tier3List.Count; t3++)
                    {
                        isAppend = false;
                        str = new StringBuilder();
                        str.Append("<level3>");
                        str.Append("<level3ID>" + tier3List[t3].ID.ToString() + "</level3ID>");
                        str.Append("<level3Name>" + tier3List[t3].Name + "</level3Name>");
                        str.Append("<level2List>");
                        //ZD 102481 Starts
                        criterionT2 = new List<ICriterion>();
                        criterionT2.Add(Expression.Eq("disabled", 0));
                        criterionT2.Add(Expression.Eq("L3LocationId", tier3List[t3].ID));
                        criterionT2.Add(Expression.In("ID", T2Ids));
                        tier2 = m_IT2DAO.GetByCriteria(criterionT2);
                        for (int t2 = 0; t2 < tier2.Count; t2++)
                        {

                            T2 = tier2[t2]; //ZD 102481 Ends
                            str.Append("<level2>");
                            str.Append("<level2ID>" + T2.ID.ToString() + "</level2ID>");
                            str.Append("<level2Name>" + T2.Name + "</level2Name>");
                            str.Append("<level1List>");

                            //ZD 102481 Starts
                            criterionRooms = new List<ICriterion>();
                            criterionRooms.Add(Expression.Sql("L2LocationId=" + T2.ID));
                            criterionRooms.Add(Expression.Eq("Disabled", 0));
                            criterionRooms.Add(Expression.In("RoomID", temprooms));
                            rooms = m_IRoomDAO.GetByCriteria(criterionRooms);

                            for (int t1 = 0; t1 < rooms.Count; t1++)
                            {
                                room = rooms[t1]; //ZD 102481 Ends

                                str.Append("<level1>");
                                str.Append("<level1ID>" + room.roomId.ToString() + "</level1ID>");
                                str.Append("<level1Name>" + room.Name + "</level1Name>");
                                str.Append("<capacity>" + room.Capacity.ToString() + "</capacity>");
                                str.Append("<projector>" + room.ProjectorAvailable.ToString() + "</projector>");
                                str.Append("<maxNumConcurrent>" + room.MaxPhoneCall.ToString() + "</maxNumConcurrent>");
                                str.Append("<videoAvailable>" + room.VideoAvailable.ToString() + "</videoAvailable>");
                                str.Append("</level1>");

                                isAppend = true;
                            }
                            str.Append("</level1List>");
                            str.Append("</level2>");
                        }
                        str.Append("</level2List>");
                        str.Append("</level3>");

                        if (isAppend)
                            outputxml.Append(str);
                    }
                    outputxml.Append("</level3List>");
                }
            }
            catch (Exception ex)
            {
                m_log.Error("Fetch3LvLocations :" + ex.Message);
                throw ex;
            }
        }
        #endregion

        #region ManageConfRoom
        /// <summary>
        /// ManageConfRoom (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool ManageConfRoom(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0, mode = 0;
            long ttlRoom = 0, vRoom = 0, nvRoom = 0;
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                vrmUser user = m_IUserDAO.GetByUserId(userid);
                if (user.isSuperAdmin())
                    mode = 1;
                else
                    mode = 0;

                OrgData orgdata = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                outXml.Append("<manageConfRoom>");
                outXml.Append("<systemAvail>");
                outXml.Append("<open24>" + orgdata.Open24hrs + "</open24>");
                outXml.Append("<startTime>");
                outXml.Append("<startHour>" + orgdata.SystemStartTime.ToString("hh") + "</startHour>");
                outXml.Append("<startMin>" + orgdata.SystemStartTime.ToString("mm") + "</startMin>");
                outXml.Append("<startSet>" + orgdata.SystemStartTime.ToString("tt") + "</startSet>");
                outXml.Append("</startTime>");
                outXml.Append("<endTime>");
                outXml.Append("<endHour>" + orgdata.SystemEndTime.ToString("hh") + "</endHour>");
                outXml.Append("<endMin>" + orgdata.SystemEndTime.ToString("mm") + "</endMin>");
                outXml.Append("<endSet>" + orgdata.SystemEndTime.ToString("tt") + "</endSet>");
                outXml.Append("</endTime>");
                outXml.Append("<dayClosed>" + orgdata.Offdays + "</dayClosed>");
                outXml.Append("</systemAvail>");

                //ZD 101175 Starts
                criterionList.Add(Expression.Not(Expression.Eq("Disabled", 1)));
                criterionList.Add(Expression.Eq("orgId", organizationID));
                ttlRoom = m_vrmRoomDAO.CountByCriteria(criterionList);
                criterionList.Add(Expression.Eq("VideoAvailable", 2));
                vRoom = m_vrmRoomDAO.CountByCriteria(criterionList);
                nvRoom = ttlRoom - vRoom;

                outXml.Append("<locationList>");
                //if (ttlRoom <= 150) //ZD 102044 - 
                //{
                    outXml.Append("<ttlRoomCnt>" + ttlRoom + "</ttlRoomCnt>");
                    outXml.Append("<selected></selected>");
                    outXml.Append("<mode>" + mode + "</mode>");

                    m_Search.organizationID = organizationID;
                    m_Search.Fetch3LvLocations(ref outXml, userid); //Fetch Rooms
                //}
                //ZD 101175 Ends
                outXml.Append("</locationList>");

                

                outXml.Append("<totalNumber>" + ttlRoom + "</totalNumber>");
                outXml.Append("<totalVideo>" + vRoom + "</totalVideo>");
                outXml.Append("<totalNonVideo>" + nvRoom + "</totalNonVideo>");
                long licRem = (sysSettings.RoomLimit - ttlRoom);
                outXml.Append("<licensesRemain>" + licRem + "</licensesRemain>");
                outXml.Append("</manageConfRoom>");
                obj.outXml = outXml.ToString();
            }
            catch (myVRMException ex)
            {
                m_log.Error("vrmException", ex);
                obj.outXml = ex.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region FetchLocations
        /// <summary>
        /// FetchLocations
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="recurrentmode"></param>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public bool FetchLocations(int confid, int instanceid, int recurrentmode, ref StringBuilder outXML, ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                criterionList.Add(Expression.Eq("confid", confid));
                criterionList.Add(Expression.Eq("instanceid", instanceid));
                List<vrmConfRoom> confroomids = m_IconfRoom.GetByCriteria(criterionList);

                outXML.Append("<mainLocation>");

                for (int i = 0; i < confroomids.Count; i++)
                {
                    vrmRoom roomids = m_vrmRoomDAO.GetByRoomId(confroomids[i].roomId);

                    outXML.Append("<location>");
                    outXML.Append("<locationID>" + roomids.roomId + "</locationID>");
                    outXML.Append("<locationName>" + roomids.Name + " (" + roomids.tier2.Name + ", " + roomids.tier2.TopTierName + ")" + "</locationName>"); //ZD 102481
                    outXML.Append("<locationType>0</locationType>");
                    outXML.Append("<capacity>" + roomids.Capacity + "</capacity>");
                    outXML.Append("<project>" + roomids.ProjectorAvailable + "</project>");
                    outXML.Append("<maxNumConcurrent>" + roomids.MaxPhoneCall + "</maxNumConcurrent>");
                    //FB 2136 - start
                    /*if (roomids.SecurityImage1Id > 0)
                    {
                        vrmSecurityImage secImg = vrmImg.GetSecImage(roomids.SecurityImage1Id);
                        outXML.Append("<SecurityName>" + roomids.SecurityImage1.ToString() + "</SecurityName>");
                        outXML.Append("<SecurityImage>" + vrmImg.ConvertByteArrToBase64(secImg.BadgeImage).ToString() + "</SecurityImage>");
                        outXML.Append("<SecBadgeTextAxis>" + secImg.TextAxis.ToString() + "</SecBadgeTextAxis>");
                        outXML.Append("<SecBadgePhotoAxis>" + secImg.PhotoAxis.ToString() + "</SecBadgePhotoAxis>");
                        outXML.Append("<SecBadgeBarCodeAxis>" + secImg.BarCodeAxis.ToString() + "</SecBadgeBarCodeAxis>");
                    }
                    else
                    {
                        outXML.Append("<SecurityName></SecurityName>");
                        outXML.Append("<SecurityImage></SecurityImage>");
                        outXML.Append("<SecBadgeTextAxis></SecBadgeTextAxis>");
                        outXML.Append("<SecBadgePhotoAxis></SecBadgePhotoAxis>");
                        outXML.Append("<SecBadgeBarCodeAxis></SecBadgeBarCodeAxis>");
                    }*/
                    //FB 2136 Ends                    
                    outXML.Append("</location>");
                }
                outXML.Append("</mainLocation>");

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
            return true;
        }
        #endregion

		#region GetRoomArray

        public void GetRoomArray(Int32 userid, Int32 organizationID, ref ArrayList roomList, Boolean bHasDepartment, ref String rmStr, int mediatype, int serviceType, ArrayList selrooms,string TopTier,string MiddleTier) //FB 2089, FB 2219 //FB 3024
        {
            Boolean isSuperAdmin = false;
            Boolean singleDeptMode = true;
            //FB 3024 Starts
            List<ICriterion> LocSiteList = new List<ICriterion>();
            string a_Order = "";
            List<int> tier3IDs = null;
            String tier2IDs = "";
            int strTopLength = TopTier.Length, strMiddleLength = MiddleTier.Length;
            List<vrmTier2> tier2s = null;
            //FB 3024 Ends
            try
            {
                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                vrmUser usr = new vrmUser();
                usr = m_IUserDAO.GetByUserId(userid);

                if (orgInfo.MultipleDepartments == 1)
                {
                    singleDeptMode = false;

                    if (usr.isSuperAdmin())
                        isSuperAdmin = true;
                    else
                        isSuperAdmin = false;
                }

                String roomId = "";

                List<ICriterion> criterionList = new List<ICriterion>();

                if (!usr.isSuperAdmin() && orgInfo.MultipleDepartments == 1)
                {
                    ArrayList rmIDList = new ArrayList();
                    if (bHasDepartment)
                    {
                        rmIDList = new ArrayList();
                        List<vrmLocDepartment> locDeptList = m_Search.GetLocVsUsrDepartmentList(usr);

                        foreach (vrmLocDepartment locDept in locDeptList)
                            rmIDList.Add(locDept.roomId);

                        criterionList.Add(Expression.Eq("Disabled", 0));
                        criterionList.Add(Expression.In("roomId", rmIDList));
                    }
                    else
                    {

                        List<vrmLocDepartment> locDeptList = m_IlocDeptDAO.GetByCriteria(criterionList);
                        rmIDList = new ArrayList();
                        String deptid = "";
                        foreach (vrmLocDepartment vrmloc in locDeptList)
                            rmIDList.Add(vrmloc.roomId);

                        criterionList.Add(Expression.Eq("Disabled", 0));
                        criterionList.Add(Expression.Eq("orgId", organizationID));
                        criterionList.Add(Expression.Not(Expression.In("roomId", rmIDList)));
                    }
                }
                //Code moved for FB 2038 - Start
                //FB 2089 - IPAD requirement ... start
                if (mediatype >= 0)
                {
                    criterionList.Add(Expression.Eq("VideoAvailable", mediatype));
                }
                //FB 2089 - IPAD requirement ... end

                //FB 2219 start
                if (serviceType > 0)
                {
                    if (!selrooms[0].Equals(""))
                        criterionList.Add(Expression.Or(Expression.Eq("ServiceType", serviceType), Expression.In("roomId", selrooms)));
                    else
                        criterionList.Add(Expression.Lt("ServiceType", serviceType)); //FB 2411
                }
                //FB 2219 end
                //Code moved for FB 2038 - End

                //FB 2392
                if (PublicRoom > 0)
                    criterionList.Add(Expression.Eq("isPublic", 1));
                else if (PublicRoom < 0)
                    criterionList.Add(Expression.Eq("isPublic", 0));

                //FB 3024 Starts

                if (TopTier != "" && MiddleTier != "")
                {
                    a_Order = "L3LocationId"; //Tier3
                    if (strTopLength > 0) //FB 2671
                    {
                        LocSiteList.Add(Expression.Like("Name", TopTier.ToLower() + "%"));
                        LocSiteList.Add(Expression.Eq("disabled", 0));
                        LocSiteList.Add(Expression.Eq("orgId", organizationID));
                        List<vrmTier3> tier3s = m_IT3DAO.GetByCriteria(LocSiteList);

                        if (strMiddleLength > 0)
                        {
                            LocSiteList = new List<ICriterion>();
                            LocSiteList.Add(Expression.Like("Name", MiddleTier.ToLower() + "%"));
                            LocSiteList.Add(Expression.Eq("disabled", 0));
                            LocSiteList.Add(Expression.Eq("orgId", organizationID));
                            tier2s = m_IT2DAO.GetByCriteria(LocSiteList);
                        }
                        tier3IDs = new List<int>();

                        for (int j = 0; j < tier2s.Count; j++)
                        {
                            if (!tier3IDs.Contains(tier2s[j].L3LocationId))
                            {
                                tier3IDs.Add(tier2s[j].L3LocationId);
                                if (tier2IDs == "")
                                    tier2IDs = tier2s[j].ID.ToString();
                                else
                                    tier2IDs += "," + tier2s[j].ID.ToString();
                            }
                        }

                        criterionList.Add(Expression.In("L3LocationId", tier3IDs));
                        criterionList.Add(Expression.Sql("this_.L2LocationId in ( " + tier2IDs + ")"));
                    }
                }
                //FB 3024 Ends
                List<vrmRoom> roomChkList = m_IRoomDAO.GetByCriteria(criterionList);

                foreach (vrmRoom vrmrm in roomChkList)
                {
                    if (rmStr == "")
                        rmStr = vrmrm.roomId.ToString();
                    else
                        rmStr = rmStr + "," + vrmrm.roomId.ToString();

                    roomList.Add(vrmrm.roomId);
                }
            }
            catch (Exception ex)
            {
                m_log.Error("GetRoomArray: " + ex.Message);
            }
        }

        #endregion

        #region Fetch3LvAvailableLocations

        public void Fetch3LvAvailableLocations(Int32 organizationID, Int32 mode, String sqlStatement,
            Int32 userid, Boolean bHasDepartment, ref StringBuilder outxml, ref Int32 confType, int mediatype, int serviceType, String selectedloc, string TopTier,string MiddleTier, string roomCatlist)//FB 2170 //FB 2089, FB 2219 //FB 3024 //ZD 101468
        {
            Boolean roomAvailable = false;
            try
            {
                outxml.Append("<locationList>");

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo.MultipleDepartments == 0)
                    userid = 11;

                outxml.Append("<selected>");

                String stmt = "";

                stmt = " select cr.roomId from myVRM.DataLayer.vrmRoom cr, myVRM.DataLayer.vrmUser u where u.RoomID = cr.roomId";
                stmt += " and u.userid = " + userid.ToString() + " and cr.orgId = '" + organizationID.ToString() + "'"; //Organization Module

                IList rooms = m_IconfRoom.execQuery(stmt);
                if (rooms.Count > 0)
                {
                    //FB 2426 Start

                    //foreach (object[] obj in rooms)
                    //    outxml.Append("<level1ID>" + Convert.ToString(obj[0]) + "</level1ID>");

                    for (int j = 1; j < rooms.Count + 1; j++)
                    {
                        int apprid = 0;
                        Int32.TryParse(rooms[j - 1].ToString(), out apprid);
                        outxml.Append("<level1ID>" + apprid + "</level1ID>");

                    }

                    //FB 2426 End
                }
                outxml.Append("</selected>");

                outxml.Append("<level3List>");

                m_IT3DAO.addOrderBy(Order.Asc("Name"));
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", organizationID));
                criterionList.Add(Expression.Like("Name", TopTier.ToLower() + "%")); //FB 3024
                criterionList.Add(Expression.Eq("disabled", 0));
                List<vrmTier3> tier3List = m_IT3DAO.GetByCriteria(criterionList);
                m_IT3DAO.clearOrderBy();

                for (Int32 t1 = 0; t1 < tier3List.Count; t1++)
                {
                    bool bfirst;
                    bool bfinish;
                    bool bSecond;
                    bool bfinish2;

                    bfirst = true;
                    bfinish = false;

                    m_IT2DAO.addOrderBy(Order.Asc("Name"));
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("L3LocationId", tier3List[t1].ID));
                    criterionList.Add(Expression.Like("Name", MiddleTier.ToLower() + "%")); //FB 3024
                    List<vrmTier2> tier2List = m_IT2DAO.GetByCriteria(criterionList);
                    m_IT2DAO.clearOrderBy();

                    for (Int32 t2 = 0; t2 < tier2List.Count; t2++)
                    {
                        String stmt2 = "";
                        if (mode == 1)
                        {
                            vrmUser usr = new vrmUser();
                            usr = m_IUserDAO.GetByUserId(userid);

                            if (!usr.isSuperAdmin())
                            {
                                stmt2 = " select roomId, Name, Capacity, ProjectorAvailable, MaxPhoneCall, VideoAvailable, isTelepresence, DedicatedVideo, isPublic ";//FB 2170 FB 2334 //FB 2392- WhyGO
                                stmt2 += " from myVRM.DataLayer.vrmRoom where L2LocationId = " + tier2List[t2].ID;
                                stmt2 += " AND Disabled = 0 ";

                                //FB 2089 - IPAD requirement ... start
                                if(mediatype >= 0)
                                    stmt2 += " AND VideoAvailable='"+ mediatype +"' ";
                                //FB 2089 - IPAD requirement ... end
                                //FB 2411 Start
                                string serivetypein = "0";
                                if (serviceType == 3)
                                    serivetypein = "1,2,3";
                                else if (serviceType == 2)
                                    serivetypein = "1,2";
                                else if (serviceType == 1)
                                    serivetypein = "1";


                                if (serviceType > 0)//FB 2219
                                {
                                    if (selectedloc.Trim() == "")
                                        stmt2 += " AND ServiceType in (" + serivetypein + ") ";
                                    else
                                        stmt2 += " AND (ServiceType in (" + serivetypein + ") OR roomId IN  (" + selectedloc.Trim() + "))";
                                }
                                //FB 2411 End

                                if (PublicRoom > 0)//FB 2392
                                    stmt2 += " AND isPublic = 1";
                                else if ( PublicRoom < 0 )
                                    stmt2 += " AND isPublic = 0";


                                if (sqlStatement != "")
                                    stmt2 += sqlStatement;
                                stmt2 += " and L3LocationId=" + tier3List[t1].ID;

                                if (bHasDepartment)
                                {
                                    stmt2 += " and roomId in (select crd.roomId from myVRM.DataLayer.vrmLocDepartment  crd, myVRM.DataLayer.vrmUserDepartment UD";
                                    stmt2 += " where crd.departmentId = UD.departmentId and UD.userId = " + userid + " )";
                                }
                                else
                                {
                                    stmt2 += " and orgId='" + organizationID + "' and ";
                                    stmt2 += " roomId not in (select roomId from myVRM.DataLayer.vrmLocDepartment) and roomId <> 11";
                                }
                                stmt2 += "	ORDER BY Name ";
                            }
                            else
                            {
                                stmt2 = " select roomId, Name, Capacity, ProjectorAvailable, MaxPhoneCall, VideoAvailable, isTelepresence, DedicatedVideo, isPublic ";//FB 2170 FB 2334 //FB 2392- WhyGO
                                stmt2 += " from myVRM.DataLayer.vrmRoom where L2LocationId = " + tier2List[t2].ID + " and L3LocationId=" + tier3List[t1].ID;
                                stmt2 += " AND Disabled = 0 ";

                                //FB 2089 - IPAD requirement ... start
                                if (mediatype >= 0)
                                    stmt2 += " AND VideoAvailable='" + mediatype + "' ";
                                //FB 2089 - IPAD requirement ... end
                                //FB 2411 Start
                                string serivetypein = "0";
                                if (serviceType == 3)
                                    serivetypein = "1,2,3";
                                else if (serviceType == 2)
                                    serivetypein = "1,2";
                                else if (serviceType == 1)
                                    serivetypein = "1";

                                if (serviceType > 0)//FB 2219
                                {
                                    if (selectedloc.Trim() == "")
                                        stmt2 += " AND ServiceType in (" + serivetypein + ") "; 
                                    else
                                        stmt2 += " AND (ServiceType in (" + serivetypein + ") OR roomId IN  (" + selectedloc.Trim() + "))";
                                }
                                //FB 2411 End

                                if (PublicRoom > 0)//FB 2392
                                    stmt2 += " AND isPublic = 1";
                                else if (PublicRoom < 0)
                                    stmt2 += " AND isPublic = 0";
                                
                                //ZD 101468 Starts
                                if (roomCatlist != "") 
                                    stmt2 += " AND RoomCategory in (" + roomCatlist + ")";
                                //ZD 101468 Ends

                                if (sqlStatement != "")
                                    stmt2 += sqlStatement;
                                stmt2 += " ORDER BY Name";
                            }
                        }
                        else
                        {
                            stmt2 = "select roomId, Name, Capacity, ProjectorAvailable, MaxPhoneCall, VideoAvailable, isTelepresence, DedicatedVideo, isPublic ";//FB 2170 FB 2334 2392
                            stmt2 += "from myVRM.DataLayer.vrmRoom where L2LocationId = " + tier2List[t2].ID + " and L3LocationId=" + tier3List[t1].ID; //ZD 102481 CHECK
                            stmt2 += " AND Disabled = 0 ";

                            //FB 2089 - IPAD requirement ... start
                            if (mediatype >= 0)
                                stmt2 += " AND VideoAvailable='" + mediatype + "' ";
                            //FB 2089 - IPAD requirement ... end

                            //FB 2411 Start
                            string serivetypein = "0";
                            if (serviceType == 3)
                                serivetypein = "1,2,3";
                            else if (serviceType == 2)
                                serivetypein = "1,2";
                            else if (serviceType == 1)
                                serivetypein = "1";

                            if (serviceType > 0)//FB 2219
                            {
                                if (selectedloc.Trim() == "")
                                    stmt2 += " AND ServiceType in (" + serivetypein + ") "; 
                                else
                                    stmt2 += " AND (ServiceType in (" + serivetypein + ") OR roomId IN  (" + selectedloc.Trim() + "))"; 
                            }
                            //FB 2411 End

                            if (PublicRoom > 0)//FB 2392
                                stmt2 += " AND isPublic = 1";
                            else if (PublicRoom < 0)
                                stmt2 += " AND isPublic = 0";

                            //ZD 101468 Starts
                            if (roomCatlist != "")
                                stmt2 += " AND RoomCategory in (" + roomCatlist + ")";
                            //ZD 101468 Ends

                            if (sqlStatement != "")
                                stmt2 += sqlStatement;
                        }

                        bSecond = true;
                        bfirst = true;
                        bfinish2 = false;
                        bfinish = false;

                        rooms = m_IconfRoom.execQuery(stmt2);
                        if (rooms.Count > 0)
                        {
                            foreach (object[] obj in rooms)
                            {
                                if (bfirst)
                                {
                                    outxml.Append("<level3>");
                                    outxml.Append(" <level3ID>" + tier3List[t1].ID + "</level3ID>");
                                    outxml.Append(" <level3Name>" + tier3List[t1].Name + "</level3Name>");
                                    outxml.Append(" <level2List>");

                                    bfirst = false;
                                    bfinish = true;
                                }
                                if (bSecond)
                                {
                                    outxml.Append(" <level2>");
                                    outxml.Append(" <level2ID>" + tier2List[t2].ID + "</level2ID>");
                                    outxml.Append(" <level2Name>" + tier2List[t2].Name + "</level2Name>");
                                    outxml.Append("<level1List>");

                                    bSecond = false;
                                    bfinish2 = true;
                                }
                                String istelepresence = ""; //FB 2170
                                String l1id = Convert.ToString(obj[0]);
                                String l1name = Convert.ToString(obj[1]);
                                String capacity = Convert.ToString(obj[2]);
                                String projector = Convert.ToString(obj[3]);
                                String maxNumConcurrent = Convert.ToString(obj[4]);
                                int iVideoAvailable = Convert.ToInt32(obj[5]);
                                int isPublic = Convert.ToInt32(obj[8]); //FB 2392- WhyGO
                                //FB 2170 Start
                                if (obj[6] == null)
                                    istelepresence = "0";
                                else
                                    istelepresence = obj[6].ToString();
                                //FB 2170 End
                                String Dedvideo = Convert.ToString(obj[7]); ;//FB 2334
                                roomAvailable = true;

                                if (orgInfo.TelepresenceFilter <= 0 && confType == vrmConfType.RooomOnly && istelepresence == "1")//FB 2170
                                    continue;
                                //FB 2334 Start
                                if (orgInfo.DedicatedVideo <= 0 && confType == vrmConfType.RooomOnly && Dedvideo == "1")
                                {
                                    if(l1id != selectedloc.Trim())
                                    {
                                        continue;
                                    }
                                }
                                //FB 2334 End
                                outxml.Append("<level1>");
                                outxml.Append("<level1ID>" + l1id + "</level1ID>");
                                outxml.Append("<level1Name>" + l1name + "</level1Name>");
                                outxml.Append("<capacity>" + capacity + "</capacity>");
                                outxml.Append("<projector>" + projector + "</projector>");
                                outxml.Append("<maxNumConcurrent>" + maxNumConcurrent + "</maxNumConcurrent>");

                                String videoAvailable = "";
                                if (iVideoAvailable == 2)
                                    videoAvailable = "1"; //video available
                                else
                                    videoAvailable = "0"; //no video available

                                outxml.Append("<videoAvailable>" + videoAvailable + "</videoAvailable>");
                                outxml.Append("<isPublic>" + isPublic + "</isPublic>");//FB 2392
                                //FB 2038 start
                                stmt2 = "select approverid from myVRM.DataLayer.vrmLocApprover where roomid = " + l1id;
                                IList appr = m_IconfRoom.execQuery(stmt2);
                                
                                if (appr.Count > 0)
                                    outxml.Append("<Approval>1</Approval>");
                                else
                                    outxml.Append("<Approval>0</Approval>");

                                outxml.Append("<Approvers>");
                                for (int j = 1; j < appr.Count + 1; j++)
                                {
                                    int apprid = 0;
                                    Int32.TryParse(appr[j - 1].ToString(), out apprid);

                                    vrmUser userInfo = m_IUserDAO.GetByUserId(apprid);
                                    outxml.Append("<Approver" + j + "ID>" + apprid + "</Approver" + j + "ID>");
                                    outxml.Append("<Approver" + j + "Name>" + userInfo.FirstName + " " + userInfo.LastName + "</Approver" + j + "Name>");
                                }
                                for (int j = appr.Count + 1; j <= 3; j++)
                                {
                                    outxml.Append("<Approver" + j + "ID></Approver" + j + "ID>");
                                    outxml.Append("<Approver" + j + "Name></Approver" + j + "Name>");
                                }
                                outxml.Append("</Approvers>");
                                //FB 2038 End
                                outxml.Append("</level1>");
                            }
                            if (bfinish2)
                            {
                                outxml.Append("</level1List>");
                                outxml.Append("</level2>");
                            }
                        }
                        if (bfinish)
                        {
                            outxml.Append("</level2List>");
                            outxml.Append("</level3>");
                        }
                    }
                }
                outxml.Append("</level3List>");
                outxml.Append("</locationList>");
            }
            catch (Exception ex)
            {
                m_log.Error("Fetch3LvAvailableLocations :" + ex.Message);
            }
        }

        #endregion

        #region Get Last Modified Rooms
        /// <summary>
        /// FB 2149
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetLastModifiedRoom(ref vrmDataObject obj)
        {
            IList rooms = null;
            String rmIDs = "";
            XmlDocument xd = null;
            XmlNode node = null;
            string userID = "";
            int userUID = 0;
            string orgid = "";
            string dateStr = "";
            DateTime modifiedDate = DateTime.Now;
            string timeZne = "";
            int timeZneId = -1;
            string stmt = "";
            vrmBaseUser vrmUser = null;
            string isUTC = "";
            StringBuilder str = null;
            try
            {
                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                

                node = xd.SelectSingleNode("//login/userID");
                userID = node.InnerXml.Trim();

                if (userID == "")
                    userID = "11";

                Int32.TryParse(userID,out userUID);

                node = xd.SelectSingleNode("//login/organizationID"); 
                if (node != null)
                    orgid = node.InnerXml.Trim();

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//login/modifiedDate"); 
                if (node != null)
                    dateStr = node.InnerXml.Trim();
                if (dateStr != "")
                    DateTime.TryParse(dateStr, out modifiedDate);

                node = xd.SelectSingleNode("//login/utcEnabled");
                if (node != null)
                    isUTC = node.InnerXml.Trim();

                if (isUTC != "1")
                {


                    node = xd.SelectSingleNode("//login/timeZone");
                    if (node != null)
                        timeZne = node.InnerXml.Trim();

                    Int32.TryParse(timeZne, out timeZneId);

                    if (timeZneId <= 0)
                    {
                        vrmUser = m_IUserDAO.GetByUserId(userUID);
                        timeZneId = vrmUser.TimeZone;
                    }

                    try
                    {
                        timeZone.changeToGMTTime(timeZneId, ref modifiedDate);

                    }
                    catch (Exception lclEX)
                    {
                        modifiedDate = DateTime.MinValue;
                       
                    }
                }
                if (modifiedDate == DateTime.MinValue)
                {
                    modifiedDate = DateTime.Now;
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref modifiedDate);
                }


                stmt = " select roomId ";
                stmt += " from myVRM.DataLayer.vrmRoom where Lastmodifieddate >= '" +modifiedDate.ToString() + "' and orgId = "+ organizationID.ToString();
                

                rooms = m_IRoomDAO.execQuery(stmt);
                if (str == null)
                    str = new StringBuilder();
                str.Append("<level1>");
                for (int i = 0; i < rooms.Count; i++)
                    str.Append("<level1ID>" + rooms[i].ToString() + "</level1ID>");

                str.Append("</level1>");


                

                obj.outXml = "<login>"+ str.ToString() +"</login>";
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        //FB 2027 DeleteRoom,ActiveRoom - Start
        #region DeleteRoom
        /// <summary>
        /// DeleteRoom (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteRoom(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0, roomid = 0, errid = 0;
            String sAddon = "_Del_";
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                List<vrmRoom> locRoom = new List<vrmRoom>();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/roomID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out roomid);

                if (CanDeleteRoom(roomid, ref errid))
                {
                    criterionList.Add(Expression.Eq("roomId", roomid));
                    locRoom = m_vrmRoomDAO.GetByCriteria(criterionList, true);
                    if (locRoom.Count > 0)
                    {
                        int IsCreateTemplate = locRoom[0].IsCreateTemplate;
                        int ConfTemplateId = locRoom[0].ConfTemplateId;
                        if (locRoom[0].OwnerID > 0)
                            locRoom[0].Name = locRoom[0].Name + sAddon + locRoom[0].roomId;

                        locRoom[0].Disabled = 1;
                        locRoom[0].Lastmodifieddate = DateTime.UtcNow; //ZD 101026
                        locRoom[0].ConfTemplateId = 0;
                        locRoom[0].IsCreateTemplate = 0;
                        m_vrmRoomDAO.Update(locRoom[0]);
                        //ZD 100522 Starts
                        if (IsCreateTemplate == 1)
                        {
                            if (ConfTemplateId > 0)
                            {
                                vrmDataObject tempObj = new vrmDataObject();
                                StringBuilder tempXML = new StringBuilder();
                                tempXML.Append("<login>");
                                tempXML.Append("<organizationID>" + organizationID + "</organizationID>");
                                tempXML.Append("<userID>" + userid + "</userID>");
                                tempXML.Append("<templates>");
                                tempXML.Append("<template>");
                                tempXML.Append("<templateID>" + ConfTemplateId + "</templateID>");
                                tempXML.Append("</template>");
                                tempXML.Append("</templates>");
                                tempXML.Append("</login>");
                                tempObj.inXml = tempXML.ToString();
                                m_ConferenceFactory = new Conference(ref obj);
                                m_ConferenceFactory.DeleteTemplate(ref tempObj);
                            }
                        }
                        //ZD 100522 End
                        m_Search.SetAuditRoom(false, roomid, locRoom[0].Lastmodifieddate, userid);  //ZD 100664
                    }

                    // ZD 102123 Starts
                    string hql = "select * from Loc_FloorPlan_D where roomids like '%" + roomid + "%'";
                    DataSet ds = m_roomlayer.ExecuteDataSet(hql);
                    string[] roomidarr = new string[1];
                    string[] roomidlist;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        roomidarr[0] = roomid.ToString();
                        if (ds.Tables[0].Rows.Count > 1) // This block removes duplicate entries if any
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                roomidlist = ds.Tables[0].Rows[i]["roomids"].ToString().Split(',');
                                var intersec = roomidlist.Intersect(roomidarr);
                                if (intersec.Count() < 1)
                                {
                                    ds.Tables[0].Rows[i].Delete();
                                    ds.AcceptChanges();
                                    i--;
                                }
                            }
                        }

                        roomidlist = ds.Tables[0].Rows[0]["roomids"].ToString().Split(',');
                        int flrPlanId;
                        vrmFloorPlan flrPlan = new vrmFloorPlan();
                        Int32.TryParse(ds.Tables[0].Rows[0]["Floorplanid"].ToString(), out flrPlanId);

                        if (roomidlist.Count() > 1) // This block delete roomid from plan
                        {
                            string[] locationlist = ds.Tables[0].Rows[0]["identifiedLocations"].ToString().Split(',');
                            string newRoomIds = "";
                            string newLocList = "";
                            for (int j = 0; j < roomidlist.Count(); j++)
                            {
                                if (roomid.ToString() == roomidlist[j])
                                    continue;

                                if (newRoomIds == "")
                                {
                                    newRoomIds = roomidlist[j];
                                    newLocList = locationlist[j];
                                }
                                else
                                {
                                    newRoomIds += "," + roomidlist[j];
                                    newLocList += "," + locationlist[j];
                                }
                            }

                            flrPlan = m_IFloorRoomDAO.GetByFloorRoomId(flrPlanId);
                            flrPlan.identifiedLocations = newLocList;
                            flrPlan.roomids = newRoomIds;
                            m_IFloorRoomDAO.Update(flrPlan);

                        }
                        else // This block delete entire plan
                        {
                            flrPlan = m_IFloorRoomDAO.GetByFloorRoomId(flrPlanId);
                            m_IFloorRoomDAO.Delete(flrPlan);
                        }

                    }
                    // ZD 102123 Ends
                }
                else
                {
                    myVRMEx = new myVRMException(errid);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
            }
            catch (myVRMException ex)
            {
                m_log.Error("vrmException", ex);
                obj.outXml = ex.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }

        private bool CanDeleteRoom(int roomid, ref int errid)
        {
            try
            {
                string hql;
                DataSet ds = null;

                if (m_roomlayer == null)
                    m_roomlayer = new ns_SqlHelper.SqlHelper(m_configPath);

                hql = "select * from conf_conference_d b inner join conf_room_d a on a.confid = b.confid";
                hql += " and a.roomid=" + roomid + " and b.deleted =0 and ((getutcdate() between b.confdate and";
                hql += " dateAdd(minute,b.duration,b.confdate)) or b.confdate > getutcdate() )"; //FB 2494 [getdate() as getutcdate(), and b.status not in (7)] 

                if (hql != "")
                    ds = m_roomlayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 550; //FB 2369
                    return false;
                }
                hql = " select * from tmp_list_d a inner join tmp_room_d b on a.tmpid=b.tmpid and b.roomid=" + roomid + " and a.deleted =0 ";
                if (hql != "")
                    ds = m_roomlayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 545; //FB 2164
                    return false;
                }
                hql = " select * from usr_List_d where PreferedRoom like '" + roomid + "'";
                hql += "or PreferedRoom like '" + roomid + ",%'or PreferedRoom like '%," + roomid + ",%'or PreferedRoom like '%," + roomid + "'";
                if (hql != "")
                    ds = m_roomlayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 538;
                    return false;
                }
                hql = " select * from usr_templates_D where deleted =0 and location= '" + roomid +"'"; //FB 2272
                if (hql != "")
                    ds = m_roomlayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 539;
                    return false;
                }
                hql = " select * from Inv_Category_D a inner join inv_room_d b";
                hql += " on a.id=b.categoryid and b.locationid=" + roomid + " and a.deleted =0 ";
                if (hql != "")
                    ds = m_roomlayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 540;
                    return false;
                }
                hql = "select * from usr_searchtemplates_d where template ";
                hql += " like '%<Selected>" + roomid + "</Selected>%' ";
                if (hql != "")
                    ds = m_roomlayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 541;
                    return false;
                }
                return true;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region ActiveRoom
        /// <summary>
        /// ActiveRoom (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool ActiveRoom(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0, roomid = 0, errid = 0;
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionList2 = new List<ICriterion>();
            List<ICriterion> criterionList3 = new List<ICriterion>();
            List<ICriterion> criterionList4 = new List<ICriterion>();//FB 2586
            List<ICriterion> criterionList5 = new List<ICriterion>();//FB 2694
            List<ICriterion> criterionList6 = new List<ICriterion>();//FB 2694
            List<ICriterion> criterionList7 = new List<ICriterion>();//ZD 101098
            List<ICriterion> Roomlist = new List<ICriterion>();
            try
            {
                List<vrmRoom> locRoom = new List<vrmRoom>();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/roomID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out roomid);

                if (roomid != 0)
                {
                    //ZD 100522 Starts
                    vrmRoom checkVMRCount = m_IRoomDAO.GetByRoomId(roomid);
                    if (checkVMRCount != null && checkVMRCount.IsCreateOnMCU >= 1)
                    {
                        myVRMEx = new myVRMException(732);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                    //ZD 100522 End
                    
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("Disabled", 0));
                    criterionList.Add(Expression.Eq("VideoAvailable", 2));
                    criterionList.Add(Expression.Eq("orgId", organizationID));
                    criterionList.Add(Expression.Eq("IsVMR", 0));//FB 2586
                    criterionList.Add(Expression.Eq("Extroom", 0));//FB 2586
                    criterionList.Add(Expression.Eq("isPublic", 0)); //FB 2594
                    criterionList.Add(Expression.Eq("RoomCategory", 1)); //ZD 101527
                    List<vrmRoom> checkVRoomCount = m_IRoomDAO.GetByCriteria(criterionList);
                    int roomVCount = checkVRoomCount.Count;
                    criterionList2.Add(Expression.Eq("Disabled", 0));
                    criterionList2.Add(Expression.Lt("VideoAvailable", 2));
					criterionList2.Add(Expression.Eq("orgId", organizationID));
                    criterionList2.Add(Expression.Eq("IsVMR", 0));//FB 2586
                    criterionList2.Add(Expression.Eq("isPublic", 0)); //FB 2594
                    criterionList2.Add(Expression.Not(Expression.Eq("RoomCategory", 4))); //ZD NON VIDEO LICENSE ISSUE
                    List<vrmRoom> checkNVRoomCount = m_IRoomDAO.GetByCriteria(criterionList2);
                    int roomNVCount = checkNVRoomCount.Count;
                    //FB 2586 Start
                    criterionList4.Add(Expression.Eq("Disabled", 0));
                    criterionList4.Add(Expression.Eq("VideoAvailable", 2));
                    criterionList4.Add(Expression.Eq("orgId", organizationID));
                    criterionList4.Add(Expression.Eq("IsVMR", 1));
                    List<vrmRoom> checkVMRRoomCount = m_IRoomDAO.GetByCriteria(criterionList4);
                    int roomVMRCount = checkVMRRoomCount.Count;
                    //FB 2586 End
                    //FB 2694 Start
                    
                    criterionList5.Add(Expression.Eq("Disabled", 0));
                    criterionList5.Add(Expression.Eq("VideoAvailable", 2));
                    criterionList5.Add(Expression.Eq("orgId", organizationID));
                    criterionList5.Add(Expression.Eq("RoomCategory", 4));
                    List<vrmRoom> checkVCHotdeskingRoomCount = m_IRoomDAO.GetByCriteria(criterionList5);
                    int roomHotdeskingVCCount = checkVCHotdeskingRoomCount.Count;

                    criterionList6.Add(Expression.Eq("Disabled", 0));
                    criterionList6.Add(Expression.Lt("VideoAvailable", 2));
                    criterionList6.Add(Expression.Eq("orgId", organizationID));
                    criterionList6.Add(Expression.Eq("RoomCategory", 4));
                    List<vrmRoom> checkNVHotdeskingCount = m_IRoomDAO.GetByCriteria(criterionList6);
                    int roomHotdeskingNVCount = checkNVHotdeskingCount.Count;

                    //ZD 101098 START
                    criterionList7.Add(Expression.Eq("Disabled", 0));
                    criterionList7.Add(Expression.Eq("VideoAvailable", 2));
                    criterionList7.Add(Expression.Eq("orgId", organizationID));
                    criterionList7.Add(Expression.Eq("iControl", 1));
                    List<vrmRoom> checkiControlRoomCount = m_IRoomDAO.GetByCriteria(criterionList7);
                    int roomiControlCount = checkiControlRoomCount.Count;
                    //ZD 101098 END

                    //FB 2694 End
                    int maxVRooms = orgInfo.MaxVideoRooms;
                    int maxNVRooms = orgInfo.MaxNonVideoRooms;
                    int maxVMRRms = orgInfo.MaxVMRRooms;//FB 2586
                    int maxiControlRms = orgInfo.MaxiControlRooms;//ZD 101098
                    int maxVCHotdeskingroom = orgInfo.MaxVCHotdesking;//FB 2694
                    int maxNVHotdeskingroom = orgInfo.MaxROHotdesking;//FB 2694
                    vrmRoom room = m_IRoomDAO.GetByRoomId(roomid);
                    int videoAvail = room.VideoAvailable;
                    int vmr = room.IsVMR;//FB 2586
                    int iControl = room.iControl;//ZD 101098
                    int guestroom = room.Extroom;//FB 2586
                    int roomcategory = room.RoomCategory;//FB 2694
                    //roomVCount += 1;
                    //roomNVCount += 1;
                    //roomVMRCount += 1;//FB 2586 
                    int canActivate = 0;
                    if (videoAvail == 2 && vmr == 0 && iControl == 0 && roomcategory != 4) //FB 2586 //FB 2694 //ZD 101098
                    {
                        if (roomVCount < maxVRooms)//FB 2694
                        {
                            canActivate = 1;
                        }
                        else
                        {
                            myVRMEx = new myVRMException(455);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2586 Start
                    else if (videoAvail == 2 && vmr == 1 && roomcategory != 4) //FB 2694
                    {
                        if (roomVMRCount < maxVMRRms)//FB 2694
                        {
                            canActivate = 1;
                        }
                        else
                        {
                            myVRMEx = new myVRMException(655);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2586 End

                    //ZD 101098 iControl START
                    else if (videoAvail == 2 && iControl == 1 && roomcategory != 4) //FB 2694
                    {
                        if (roomiControlCount < maxiControlRms)//FB 2694
                        {
                            canActivate = 1;
                        }
                        else
                        {
                            myVRMEx = new myVRMException(738);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //ZD 101098 END
                    
                    //FB 2694 start
                    else if (videoAvail == 2 && vmr == 0 && roomcategory == 4)
                    {
                        if (roomHotdeskingVCCount < maxVCHotdeskingroom)//FB 2694
                        {
                            canActivate = 1;
                        }
                        else
                        {
                            myVRMEx = new myVRMException(693);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    else if (videoAvail != 2 && vmr == 0 && roomcategory == 4)
                    {
                        if (roomHotdeskingNVCount < maxNVHotdeskingroom)//FB 2694
                        {
                            canActivate = 1;
                        }
                        else
                        {
                            myVRMEx = new myVRMException(694);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2694
                    else
                    {
                        if (roomNVCount < maxNVRooms) //FB 2694
                        {
                            canActivate = 1;
                        }
                        else
                        {
                            myVRMEx = new myVRMException(456);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    if (canActivate == 1)
                    {
                        Roomlist.Add(Expression.Eq("roomId", roomid));
                        List<vrmRoom> Actroom = m_IRoomDAO.GetByCriteria(Roomlist, true);
                        Actroom[0].disabled = 0;
                        Actroom[0].Lastmodifieddate = DateTime.UtcNow; //ZD 101026
                        m_IRoomDAO.Update(Actroom[0]);
                        criterionList3.Add(Expression.Eq("userid", Actroom[0].assistant));
                        List<vrmUser> usercnt = m_IUserDAO.GetByCriteria(criterionList3);
                        if (usercnt.Count == 0)
                        {
                            outXml.Append("<success><AssistantInchargeName>" + usercnt[0].FirstName + "</AssistantInchargeName>");
                            outXml.Append("<UserStatus>I</UserStatus></success>");
                        }
                        m_Search.SetAuditRoom(false, roomid, Actroom[0].Lastmodifieddate, userid);  //ZD 100664

                    }
                    obj.outXml = outXml.ToString();
                }
                else
                {
                    myVRMEx = new myVRMException(252);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
            }
            catch (myVRMException ex)
            {
                m_log.Error("vrmException", ex);
                obj.outXml = ex.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }

        #endregion
        //FB 2027 DeleteRoom,ActiveRoom - End
        #region GetServiceType
        /// <summary>
        /// GetServiceType - FB 2219
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetServiceType(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder OutXml = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetServiceType/UserID");
                string UserID = node.InnerXml.Trim();

                List<vrmServiceType> st_List = vrmGen.getServiceTypes();
                OutXml.Append("<GetServiceType>");
                foreach (vrmServiceType st in st_List)
                {
                    OutXml.Append("<ServiceType>");
                    OutXml.Append("<ID>" + st.Id.ToString() + "</ID>");
                    OutXml.Append("<Name>" + st.ServiceType + "</Name>");
                    OutXml.Append("</ServiceType>");
                }
                OutXml.Append("</GetServiceType>");
                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2361
        #region GetRoomDetails 
        /// <summary>
        /// GetRoomDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder OutXml = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetRoomDetails/UserID");
                string UserID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetRoomDetails/EndPointID");
                string eptID = node.InnerXml.Trim();

                List<ICriterion> criterionLst = new List<ICriterion>();
                if(eptID != "")
                    criterionLst.Add(Expression.Eq("endpointid", Convert.ToInt32(eptID)));

                List<vrmRoom> rmList = m_IRoomDAO.GetByCriteria(criterionLst);

                OutXml.Append("<GetRoomDetails>");
                foreach (vrmRoom rm in rmList)
                {
                    OutXml.Append("<Room>");
                    OutXml.Append("<ID>" + rm.roomId + "</ID>");
                    OutXml.Append("<Name>" + rm.Name + "</Name>");
                    OutXml.Append("</Room>");
                }
                OutXml.Append("</GetRoomDetails>");

                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetRoomQueue
        /// <summary>
        /// GetRoomQueue
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomQueue(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder OutXml = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetRoomQueue/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                List<ICriterion> criterionLst = new List<ICriterion>();
                if (orgid != "")
                    criterionLst.Add(Expression.Eq("Disabled", 0));
                    criterionLst.Add(Expression.Eq("orgId", organizationID));
                    criterionLst.Add(Expression.Not(Expression.Eq("RoomQueue", ""))); 
                    

                List<vrmRoom> rmList = m_IRoomDAO.GetByCriteria(criterionLst);

                OutXml.Append("<GetRoomQueue>");
                OutXml.Append("<Roomemails>");
                foreach (vrmRoom rm in rmList)
                {
                    OutXml.Append(rm.RoomQueue +";");
                }
                OutXml.Append("</Roomemails>");
                OutXml.Append("</GetRoomQueue>");

                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2426 Start

        #region SetGuesttoNormalRoom
        /// <summary>
        /// SetGuesttoNormalRoom
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetGuesttoNormalRoom(ref vrmDataObject obj)
        {
            int GuestRoomID = 0;
            int userid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                List<ICriterion> criterionList = new List<ICriterion>();
                List<ICriterion> Licencechk = new List<ICriterion>();
                List<ICriterion> confroom = new List<ICriterion>();
                List<ICriterion> criterionLst = new List<ICriterion>();
                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/roomID");
                string roomID = node.InnerXml.Trim();
                Int32.TryParse(roomID, out GuestRoomID);

                criterionList.Add(Expression.Eq("roomId", GuestRoomID));
                criterionList.Add(Expression.Eq("Disabled", 0));
                criterionList.Add(Expression.Eq("Extroom", 1));
                criterionList.Add(Expression.Eq("isPublic", 0)); //FB 2594
                criterionList.Add(Expression.Eq("orgId", organizationID));
                List<vrmRoom> checkRoom = m_IRoomDAO.GetByCriteria(criterionList,true);
                if (checkRoom.Count > 0)
                {
                    criterionLst = new List<ICriterion>();
                    criterionLst.Add(Expression.Eq("Name", checkRoom[0].Name));
                    criterionLst.Add(Expression.Not(Expression.Eq("RoomID", GuestRoomID)));
                    criterionLst.Add(Expression.Eq("Extroom", 0));
                    List<vrmRoom> roomChkList = m_IRoomDAO.GetByCriteria(criterionLst);
                    if (roomChkList.Count > 0)
                    {
                        myVRMException myVRMEx = new myVRMException(256);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                }

                Licencechk.Add(Expression.Eq("RoomCategory", 1)); //ZD 103773
                Licencechk.Add(Expression.Eq("Disabled", 0));
                Licencechk.Add(Expression.Eq("VideoAvailable", 2));
                Licencechk.Add(Expression.Eq("Extroom", 0));
                Licencechk.Add(Expression.Eq("IsVMR", 0));//FB 2586
                Licencechk.Add(Expression.Eq("isPublic", 0)); //FB 2594
                Licencechk.Add(Expression.Eq("orgId", organizationID));//FB 2510
                List<vrmRoom> checkguestRoomCount = m_IRoomDAO.GetByCriteria(Licencechk);
                //FB 2586 START
                int maxVideosRooms = orgInfo.MaxVideoRooms;
                vrmRoom room = m_IRoomDAO.GetByRoomId(GuestRoomID);

                if (room.VideoAvailable == 2 && room.IsVMR == 0 && room.Extroom == 1)
                {
                    if (checkguestRoomCount.Count >= maxVideosRooms)
                    {
                        myVRMEx = new myVRMException(455);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                }
                //FB 2586 END
                if (checkRoom.Count > 0)
                {
                    for (int i = 0; i < checkRoom.Count; i++)
                    {
                        checkRoom[i].Extroom = 0;
                        checkRoom[i].RoomCategory = 1; //ZD 103773
                        checkRoom[i].RoomIconTypeId = vrmAttributeType.GuestVideo; //FB 2065 //ZD 100619
                        if (!m_HardwareFac.SetGuesttoNormalEndpoint(checkRoom[i].endpointid, ref obj))
                        {
                            return false;
                        }
                        else
                        {
                            checkRoom[i].Lastmodifieddate = DateTime.UtcNow; //ZD 101026
                            m_IRoomDAO.Update(checkRoom[i]);
                            confroom.Add(Expression.Eq("roomId", GuestRoomID));
                            confroom.Add(Expression.Eq("Extroom", 1));
                            List<vrmConfRoom> confroomlist = m_IconfRoom.GetByCriteria(confroom);
                            if (confroomlist.Count > 0)
                            {
                                for (int r = 0; r < confroomlist.Count; r++)
                                {
                                    confroomlist[r].Extroom = 0;
                                    m_IconfRoom.Update(confroomlist[r]);
                                }
                            }

                            //ZD 100664 Starts
                            m_Search.SetAuditRoom(false, checkRoom[i].roomId, checkRoom[i].Lastmodifieddate, checkRoom[i].LastModifiedUser);
                            //ZD 100664 End
                        }
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        #region DeleteGuestRoom
        /// <summary>
        /// DeleteGuestRoom
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteGuestRoom(ref vrmDataObject obj)
        {
            int GuestRoomID = 0;
            int userid = 0,errid =0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                List<ICriterion> criterionList = new List<ICriterion>();
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/roomID");
                string roomID = node.InnerXml.Trim();
                Int32.TryParse(roomID, out GuestRoomID);

                criterionList.Add(Expression.Eq("roomId", GuestRoomID));
                criterionList.Add(Expression.Eq("Disabled", 0));
                criterionList.Add(Expression.Eq("orgId", organizationID));
                criterionList.Add(Expression.Eq("Extroom", 1));
                List<vrmRoom> checkRoom = m_IRoomDAO.GetByCriteria(criterionList, true);
                if (checkRoom.Count > 0)
                {
                    for (int i = 0; i < checkRoom.Count; i++)
                    {
                        if (CanDeleteRoom(checkRoom[i].roomId, ref errid))
                        {
                            m_HardwareFac.DeleteGuestEndpoint(checkRoom[i].endpointid);
                            checkRoom[i].disabled = 1;
                            m_IRoomDAO.Update(checkRoom[i]);
                        }
                        else
                        {
                            myVRMEx = new myVRMException(errid);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        //FB 2426 End

        //FB 2392 Starts FB 2532 

        

        #region FetchAvailableRooms

        public void FetchAvailableRooms(int outXMLType, Int32 organizationID, Int32 mode, string RoomNames,
            Int32 userid, Boolean bHasDepartment, ref StringBuilder outxml, ref Int32 confType, int mediatype,
            int serviceType, string selectedloc, int SearchBy, string SearchFor, int pageNo, int MaxRecords,
            string searchCountry, string searchState, int searchCountryId, int searchStateId) //FB 2532 //FB 2558 WhyGo //FB 2671
        {
            StringBuilder publicFields = null;
            StringBuilder strOutXML = new StringBuilder();
            //ESPublicRoom PublicRoomField = null; //FB 2671
            String stateName = "";
            string alphabet = "", a_Order = "Name";
            int sortBy = 3;
            string BrokerRoomNum = ""; //FB 3001
            ArrayList stateIDs = new ArrayList();
            ArrayList countryIDs = new ArrayList(); //FB 2671
            int iPageNo = 0, numrows = 0, numrecs = 0, totpages = 0;
            Int32 u = 0;
            try
            {

                List<ICriterion> LocSiteList = new List<ICriterion>();
                List<ICriterion> LocStateList = new List<ICriterion>();
                List<ICriterion> LocCountryList = new List<ICriterion>(); //FB 2671
                
                List<ICriterion> criterionListorg = new List<ICriterion>();
                criterionListorg.Add(Expression.Eq("orgId", organizationID));

                alphabet = SearchFor;
                if (RoomNames.Trim() != "")
                    criterionListorg.Add(Expression.Sql("this_.Name Not in" + RoomNames));

                string serivetypein = "0";
                if (serviceType == 3)
                    serivetypein = "1,2,3";
                else if (serviceType == 2)
                    serivetypein = "1,2";
                else if (serviceType == 1)
                    serivetypein = "1";
                
                string stmtServiceType = "";

                if (serviceType > 0)
                {
                    if (selectedloc.Trim() == "")
                        stmtServiceType += " AND ServiceType in ('" + serivetypein + "') ";
                    else
                        stmtServiceType += " AND (ServiceType in ('" + serivetypein + "') OR roomId IN ('" + selectedloc.Trim() + "'))";

                    criterionListorg.Add(Expression.Sql(stmtServiceType));
                }

                if (PublicRoom > 0)
                    criterionListorg.Add(Expression.Eq("isPublic", 1));
                else if (PublicRoom < 0)
                    criterionListorg.Add(Expression.Eq("isPublic", 0));

                if (mediatype >= 0)
                {
                    criterionListorg.Add(Expression.Eq("VideoAvailable", mediatype));
                }

                criterionListorg.Add(Expression.Eq("Disabled", 0));
                if (selectedloc.Trim() != "")
                    criterionListorg.Add(Expression.Sql("RoomID Not in( " + selectedloc + " )"));

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo.MultipleDepartments == 0)
                    userid = 11;

                #region OutXML
                strOutXML.Append("<GetConfAvailableRoom>");
				//FB 2558 WhyGo
                List<int> tier3IDs = null;
                String tier2IDs = "";
                //FB 2671 Starts
                if (PublicRoom > 0)
                {
                    string exprSearchPublic = null;
                    if (searchStateId > 0)
                    {
                        exprSearchPublic = String.Format(
                            "RoomId in (select RoomId from ES_PublicRoom_d where StateName={0} and Country={1})",
                            searchStateId, searchCountryId);
                    }
                    else
                    {
                        if (searchCountryId > 0)
                        {
                            exprSearchPublic = String.Format("RoomId in (select RoomId from ES_PublicRoom_d where Country={0})",
                                searchCountryId);
                        }
                    }
                    if (!String.IsNullOrEmpty(exprSearchPublic))
                        criterionListorg.Add(Expression.Sql(exprSearchPublic));
                }
                else
                {
                    // search by state (if not null or empty)
                    if (!String.IsNullOrEmpty(searchState))
                    {
                        LocStateList.Add(Expression.Eq("State", searchState));
                        List<vrmState> states = m_IStateDAO.GetByCriteria(LocStateList);
                        for (int i = 0; i < states.Count; i++)
                        {
                            stateIDs.Add(states[i].Id);
                        }
                        criterionListorg.Add(Expression.In("State", stateIDs));
                    }
                    // search by country
                    if (!String.IsNullOrEmpty(searchCountry))
                    {
                        LocCountryList.Add(Expression.Eq("CountryName", searchCountry));
                        List<vrmCountry> countries = m_ICountryDAO.GetByCriteria(LocCountryList);
                        for (int i = 0; i < countries.Count; i++)
                        {
                            countryIDs.Add(countries[i].CountryID);
                        }
                        criterionListorg.Add(Expression.In("Country", countryIDs));
                    }
                }
                //FB 2671 Ends
                string checkAlpha = alphabet.ToLower();
                int capacity = 0, strLength = 0;
                strLength = alphabet.Length;
                sortBy = SearchBy;
                switch (sortBy)
                {
                    case 1:
                        a_Order = "Capacity";
                        Int32.TryParse(checkAlpha, out capacity);
                        criterionListorg.Add(Expression.Ge("Capacity", capacity));
                        break;
                    case 2:
                        a_Order = "City";
                        if (strLength > 0)
                        {
                            criterionListorg.Add(Expression.Like("City", alphabet + "%"));
                        }
                        break;
                    case 4:
                        a_Order = "RoomPhone";
                        if (strLength > 0)
                        {
                            criterionListorg.Add(Expression.Like("RoomPhone", alphabet + "%"));
                        }
                        break;
                    case 5:
                        a_Order = "L3LocationId"; //CountryName //Tier2 //FB 2558 WhyGo - Starts
                        if (strLength > 0) //FB 2671
                        {
                            LocSiteList.Add(Expression.Like("Name", alphabet + "%"));
                            LocSiteList.Add(Expression.Eq("disabled", 0));
                            LocSiteList.Add(Expression.Eq("orgId", organizationID));
                            List<vrmTier2> tier2s = m_IT2DAO.GetByCriteria(LocSiteList);
                            tier3IDs = new List<int>();

                            for (int j = 0; j < tier2s.Count; j++)
                            {
                                if (!tier3IDs.Contains(tier2s[j].L3LocationId))
                                {
                                    tier3IDs.Add(tier2s[j].L3LocationId);
                                    if (tier2IDs == "")
                                        tier2IDs = tier2s[j].ID.ToString();
                                    else
                                        tier2IDs += "," + tier2s[j].ID.ToString();
                                }
                            }
                            criterionListorg.Add(Expression.In("L3LocationId", tier3IDs));
                            criterionListorg.Add(Expression.Sql("this_.L2LocationId in ( " + tier2IDs + ")"));//FB 2558 WhyGo - End
                        }
                        break;
                    case 6:
                        a_Order = "State";
                        if (PublicRoom < 1 && strLength > 0) //FB 2671
                        {
                            LocStateList.Add(Expression.Or(Expression.Like("State", alphabet + "%"), Expression.Like("StateCode", alphabet + "%")));
                            List<vrmState> states = m_IStateDAO.GetByCriteria(LocStateList);
                            stateIDs.Clear();
                            for (int i = 0; i < states.Count; i++)
                            {
                                stateIDs.Add(states[i].Id);
                            }

                            criterionListorg.Add(Expression.In("State", stateIDs));
                        }
                        break;
                    case 7:
                        a_Order = "Zipcode";
                        if (strLength > 0)
                        {
                            criterionListorg.Add(Expression.Like("Zipcode", alphabet + "%"));
                        }
                        break;
                    default:
                        a_Order = "Name";
                        if (strLength > 0)
                        {
                            criterionListorg.Add(Expression.Like("Name", alphabet + "%"));
                        }
                        break;
                }
				//FB 2558 WhyGo - End
                iPageNo = pageNo;

                if (iPageNo == 0)
                    iPageNo = 1;
                long ttlRecords = 0;
                IList roomList = new List<vrmRoom>();

                if (pageNo > 0)
                {
                    m_IRoomDAO.addProjection(Projections.RowCount());
                    IList list = m_IRoomDAO.GetObjectByCriteria(new List<ICriterion>());
                    Int32.TryParse((list[0].ToString()), out numrows);
                    list = m_IRoomDAO.GetObjectByCriteria(criterionListorg);

                    Int32.TryParse((list[0].ToString()), out numrecs);

                    m_IRoomDAO.clearProjection();

                    m_IRoomDAO.pageSize(MaxRecords);
                    m_IRoomDAO.pageNo(iPageNo);
                    ttlRecords =
                        m_IRoomDAO.CountByCriteria(criterionListorg);
                    m_IRoomDAO.addOrderBy(Order.Asc(a_Order));
                }
                roomList = m_IRoomDAO.GetByCriteria(criterionListorg);

                totpages = numrows / MaxRecords;
                if ((numrows % MaxRecords) > 0)
                    totpages++;

                int ttlPages = (int)(ttlRecords / MaxRecords);

                if (ttlRecords % MaxRecords > 0)
                    ttlPages++;


                publicFields = new StringBuilder();
                vrmUser userInfo = null;
                
                userInfo = m_IUserDAO.GetByUserId(userid); //FB 3001

                if (roomList != null && roomList.Count > 0) //FB 2671
                {
                    //Doubt
                    //FB 2594-Starts
                    //Dictionary<int, ESPublicRoom> dictionary;
                    vrmRoom locRoom = null;

                    //List<ESPublicRoom> bymyVRMRoomId = m_IESPublicRoomDAO.GetBymyVRMRoomId(roomList.Cast<vrmRoom>().Select<vrmRoom, int>(delegate(vrmRoom room)
                    //{
                    //    return room.roomId;
                    //}).ToList<int>());
                    //if ((bymyVRMRoomId != null) && (bymyVRMRoomId.Count > 0))
                    //{
                    //    dictionary = bymyVRMRoomId.ToDictionary<ESPublicRoom, int>(delegate(ESPublicRoom publicRoom)
                    //    {
                    //        return publicRoom.RoomID;
                    //    });
                    //}
                    //else
                    //{
                    //    dictionary = new Dictionary<int, ESPublicRoom>();
                    //}
                    //FB 2594-End
                    
                    //FB XXXX Starts
                    List<ESPublicRoom> publicRoomFields = m_IESPublicRoomDAO.GetBymyVRMRoomId((from vrmRoom room in roomList select room.roomId).ToList());
                    Dictionary<int, ESPublicRoom> publicRooms;
                    if (publicRoomFields != null && publicRoomFields.Count > 0)
                        publicRooms = publicRoomFields.ToDictionary(publicRoom => publicRoom.RoomID);
                    else
                        publicRooms = new Dictionary<int, ESPublicRoom>();
                    //FB 2671 Ends


                    for (int count = 0; count < roomList.Count; count++)
                    {
                        locRoom = (vrmRoom)roomList[count];

                        /* FB 2558 - WhyGo
                        if (orgInfo.TelepresenceFilter <= 0 && confType == vrmConfType.RooomOnly && locRoom.isTelepresence.ToString() == "1")//FB 2170
                            continue;

                        if (orgInfo.DedicatedVideo <= 0 && confType == vrmConfType.RooomOnly && locRoom.DedicatedVideo.ToString() == "1")
                        {
                            if (locRoom.roomId.ToString() != selectedloc.Trim())
                            {
                                continue;
                            }
                        } Removed for whygo as there only public rooms
                        */
                        if (locRoom.Name != "Phantom Room")
                        {
                            strOutXML.Append("<Room>");
                            strOutXML.Append("<RoomID>" + locRoom.roomId.ToString() + "</RoomID>");

                            //FB 2671 Starts
                            ESPublicRoom PublicRoomField;
                            publicRooms.TryGetValue(locRoom.roomId, out PublicRoomField);
                            //FB 2671
                            strOutXML.Append("<LastModifiedDate>" + locRoom.Lastmodifieddate.ToString() + "</LastModifiedDate>"); //FB 2532

                            if (outXMLType == 1) //FB 2532 Starts
                            {
                                strOutXML.Append("<RoomName>" + locRoom.Name + "</RoomName>");
                                if (locRoom.isPublic > 0) //FB 3001
                                {
                                    if (userInfo != null)
                                        BrokerRoomNum = userInfo.BrokerRoomNum;
                                    strOutXML.Append("<RoomPhoneNumber>" + BrokerRoomNum + "</RoomPhoneNumber>");
                                }
                                else
                                    strOutXML.Append("<RoomPhoneNumber>" + locRoom.RoomPhone + "</RoomPhoneNumber>");

                                strOutXML.Append("<MaximumCapacity>" + locRoom.Capacity.ToString() + "</MaximumCapacity>");
                                if (PublicRoomField == null) //FB 2558 WhyGo
                                {
                                    //ZD 100619 Starts
                                    if (locRoom.assistant > 0)
                                    {
                                        strOutXML.Append("<AssistantInchargeID>" + locRoom.assistant.ToString() + "</AssistantInchargeID>");
                                        userInfo = m_IUserDAO.GetByUserId(locRoom.assistant);
                                        if (userInfo != null)
                                        {
                                            strOutXML.Append("<AssistantInchargeName>" + userInfo.FirstName + " " + userInfo.LastName + "</AssistantInchargeName>");
                                            strOutXML.Append("<AssistantInchargeEmail>" + userInfo.Email + "</AssistantInchargeEmail>");
                                        }
                                        else
                                        {
                                            strOutXML.Append("<AssistantInchargeName></AssistantInchargeName>");
                                            strOutXML.Append("<AssistantInchargeEmail></AssistantInchargeEmail>");
                                        }
                                      
                                    }
                                    else
                                    {
                                        strOutXML.Append("<AssistantInchargeID>0</AssistantInchargeID>");
                                        strOutXML.Append("<AssistantInchargeName>" + locRoom.GuestContactName + "</AssistantInchargeName>");
                                        strOutXML.Append("<AssistantInchargeEmail>" + locRoom.GuestContactEmail + "</AssistantInchargeEmail>");

                                    }
                                    strOutXML.Append("<GuestContactPhone>" + locRoom.GuestContactPhone + "</GuestContactPhone>");
                                    //ZD 100619 Ends
                                }
                                if (locRoom.tier2 != null)
                                {
                                    if (locRoom.tier2.L3LocationId > 0)  //ZD 102481
                                    {
                                        strOutXML.Append("<Tier1ID>" + locRoom.tier2.L3LocationId.ToString() + "</Tier1ID>");  //ZD 102481
                                        strOutXML.Append("<Tier1Name>" + locRoom.tier2.TopTierName + "</Tier1Name>");  //ZD 102481
                                    }
                                    else
                                    {
                                        strOutXML.Append("<Tier1ID></Tier1ID>");
                                        strOutXML.Append("<Tier1Name></Tier1Name>");
                                    }
                                    strOutXML.Append("<Tier2ID>" + locRoom.tier2.ID.ToString() + "</Tier2ID>");
                                    strOutXML.Append("<Tier2Name>" + locRoom.tier2.Name + "</Tier2Name>");
                                }
                                else
                                {
                                    strOutXML.Append("<Tier1ID></Tier1ID>");
                                    strOutXML.Append("<Tier1Name></Tier1Name>");
                                    strOutXML.Append("<Tier2ID></Tier2ID>");
                                    strOutXML.Append("<Tier2Name></Tier2Name>");
                                }
                                if (PublicRoomField == null)//FB 2558 WhyGo
                                {
                                    strOutXML.Append("<Floor>" + locRoom.RoomFloor + "</Floor>");
                                    strOutXML.Append("<RoomNumber>" + locRoom.RoomNumber + "</RoomNumber>");
                                    strOutXML.Append("<StreetAddress1>" + locRoom.Address1 + "</StreetAddress1>");
                                    strOutXML.Append("<StreetAddress2>" + locRoom.Address2 + "</StreetAddress2>");
                                    strOutXML.Append("<Handicappedaccess>" + locRoom.HandiCappedAccess.ToString() + "</Handicappedaccess>");
                                    strOutXML.Append("<Disabled>" + locRoom.disabled.ToString() + "</Disabled>");
                                    strOutXML.Append("<isTelePresence>" + locRoom.isTelepresence.ToString() + "</isTelePresence>");//FB 2400
                                    strOutXML.Append("<State>" + locRoom.State.ToString() + "</State>");
                                    if (locRoom.State > 0)
                                    {
                                        vrmState objState = m_IStateDAO.GetById(locRoom.State);
                                        strOutXML.Append("<StateName>" + objState.StateCode.ToString() + "</StateName>");
                                    }
                                    else
                                    {
                                        stateName = "";
                                        if (PublicRoomField != null)
                                            stateName = PublicRoomField.StateName;

                                        strOutXML.Append("<StateName>" + stateName + "</StateName>");
                                    }
                                    strOutXML.Append("<Country>" + locRoom.Country.ToString() + "</Country>");
                                    if (locRoom.Country > 0)
                                    {
                                        vrmCountry objCountry = m_ICountryDAO.GetById(locRoom.Country);
                                        strOutXML.Append("<CountryName>" + objCountry.CountryName + "</CountryName>");
                                    }
                                    else
                                    {
                                        strOutXML.Append("<CountryName></CountryName>");
                                    }
                                    string stmt2 = "";
                                    stmt2 = "select approverid from myVRM.DataLayer.vrmLocApprover where roomid = " + locRoom.roomId.ToString();
                                    IList appr = m_IconfRoom.execQuery(stmt2);

                                    if (appr.Count > 0)
                                        strOutXML.Append("<Approval>1</Approval>");
                                    else
                                        strOutXML.Append("<Approval>0</Approval>");

                                    strOutXML.Append("<Approvers>");
                                    for (int j = 1; j < appr.Count + 1; j++)
                                    {
                                        int apprid = 0;
                                        Int32.TryParse(appr[j - 1].ToString(), out apprid);

                                        userInfo = m_IUserDAO.GetByUserId(apprid);
                                        strOutXML.Append("<Approver" + j + "ID>" + apprid + "</Approver" + j + "ID>");
                                        strOutXML.Append("<Approver" + j + "Name>" + userInfo.FirstName + " " + userInfo.LastName + "</Approver" + j + "Name>");
                                    }
                                    for (int j = appr.Count + 1; j <= 3; j++)
                                    {
                                        strOutXML.Append("<Approver" + j + "ID></Approver" + j + "ID>");
                                        strOutXML.Append("<Approver" + j + "Name></Approver" + j + "Name>");
                                    }
                                    strOutXML.Append("</Approvers>");
                                }

                                strOutXML.Append("<Video>" + locRoom.VideoAvailable.ToString() + "</Video>");
                                strOutXML.Append("<City>" + locRoom.City + "</City>");
                                strOutXML.Append("<ZipCode>" + locRoom.Zipcode + "</ZipCode>");
                                strOutXML.Append("<MapLink>" + locRoom.Maplink + "</MapLink>");
                                strOutXML.Append("<TimezoneID>" + locRoom.TimezoneID.ToString() + "</TimezoneID>");
                                if (locRoom.TimezoneID > 0)
                                {
                                    timeZoneData tz = new timeZoneData();
                                    timeZone.GetTimeZone(locRoom.TimezoneID, ref tz);
                                    strOutXML.Append("<TimezoneName>" + tz.TimeZone + "</TimezoneName>");
                                }
                                else
                                {
                                    strOutXML.Append("<TimezoneName></TimezoneName>");
                                }
                                strOutXML.Append("<Longitude>" + locRoom.Longitude + "</Longitude>");
                                strOutXML.Append("<Latitude>" + locRoom.Latitude + "</Latitude>");
                                List<vrmEndPoint> eptList = new List<vrmEndPoint>();
                                List<ICriterion> criterionListept = new List<ICriterion>();
                                criterionListept.Add(Expression.Eq("endpointid", locRoom.endpointid));
                                criterionListept.Add(Expression.Eq("deleted", 0));
                                eptList = m_vrmEpt.GetByCriteria(criterionListept);

                                strOutXML.Append("<EndpointID>" + locRoom.endpointid.ToString() + "</EndpointID>");

                                if (eptList.Count > 0)
                                {
                                    foreach (vrmEndPoint ept in eptList)
                                    {
                                        strOutXML.Append("<EndpointName>" + ept.name + "</EndpointName>");
                                        strOutXML.Append("<EndpointIP>" + ept.address + "</EndpointIP>");
                                        break;
                                    }
                                }
                                else
                                {
                                    strOutXML.Append("<EndpointName></EndpointName>");
                                    strOutXML.Append("<EndpointIP></EndpointIP>");
                                }
                                if (PublicRoomField == null) //FB 2558 WhyGo
                                {
                                    //Image Project codelines start...
                                    string roomImagesids = locRoom.RoomImageId;
                                    string roomImagesnames = locRoom.RoomImage;

                                    string imagename = "";

                                    string fileext = "";

                                    if (roomImagesids != null && roomImagesnames != null)
                                    {
                                        if (roomImagesids != "" && roomImagesnames != "")
                                        {
                                            string[] idArr = roomImagesids.Split(',');
                                            string[] nameArr = roomImagesnames.Split(',');

                                            if (idArr.Length > 0 && nameArr.Length > 0)
                                            {
                                                fileext = "jpg";

                                                imagename = nameArr[0].ToString();
                                                if (imagename != "")
                                                    fileext = imagename.Substring(imagename.LastIndexOf(".") + 1);
                                            }
                                        }
                                    }
                                    strOutXML.Append("<ImageName>" + imagename + "</ImageName>");
                                    strOutXML.Append("<Imagetype>" + fileext + "</Imagetype>");
                                    strOutXML.Append("<ServiceType>" + locRoom.ServiceType.ToString() + "</ServiceType>");
                                    strOutXML.Append("<DedicatedVideo>" + locRoom.DedicatedVideo + "</DedicatedVideo>");
                                    strOutXML.Append("<DedicatedCodec>" + locRoom.DedicatedCodec + "</DedicatedCodec>");
                                    strOutXML.Append("<Extroom>" + locRoom.Extroom + "</Extroom>");
                                    strOutXML.Append("<LoginUserId>" + locRoom.adminId + "</LoginUserId>");
                                    strOutXML.Append("<isVMR>" + locRoom.IsVMR + "</isVMR>");
                                    strOutXML.Append("<DefaultEquipmentID>" + locRoom.DefaultEquipmentid.ToString() + "</DefaultEquipmentID>");
                                }
                                strOutXML.Append("<isPublic>" + locRoom.isPublic + "</isPublic>");
                                strOutXML.Append("<PublicFields>");

                                int Speed = 384;
                                publicFields = new StringBuilder();

                                if (PublicRoomField != null)
                                {
                                    if (eptList.Count > 0)
                                        Speed = eptList[0].linerateid;

                                    publicFields.Append("<WhygoRoomID>" + PublicRoomField.WhygoRoomId + "</WhygoRoomID>");
                                    publicFields.Append("<Address>" + PublicRoomField.address + "</Address>");
                                    publicFields.Append("<State>" + PublicRoomField.StateName + "</State>");
                                    publicFields.Append("<Country>" + PublicRoomField.Country + "</Country>");
                                    publicFields.Append("<AUXEquipment>" + PublicRoomField.AUXEquipment + "</AUXEquipment>");
                                    publicFields.Append("<Speed>" + Speed + "</Speed>");
                                    publicFields.Append("<Type>" + PublicRoomField.Type + "</Type>");
                                    publicFields.Append("<Description>" + PublicRoomField.RoomDescription + "</Description>");
                                    publicFields.Append("<ExtraNotes>" + PublicRoomField.ExtraNotes + "</ExtraNotes>");
                                    publicFields.Append("<GeoCodeAddress>" + PublicRoomField.geoCodeAddress + "</GeoCodeAddress>");
                                    publicFields.Append("<IsAutomatic>" + PublicRoomField.isAutomatic + "</IsAutomatic>");
                                    publicFields.Append("<IsHDCapable>" + PublicRoomField.isHDCapable + "</IsHDCapable>");
                                    publicFields.Append("<IsIPCapable>" + PublicRoomField.isIPCapable + "</IsIPCapable>");
                                    publicFields.Append("<IsISDNCapable>" + PublicRoomField.isISDNCapable + "</IsISDNCapable>");
                                    publicFields.Append("<ImgLink>" + PublicRoomField.MapLink + "</ImgLink>");
                                    publicFields.Append("<IsTP>" + PublicRoomField.isTP + "</IsTP>");
                                    publicFields.Append("<isVCCapable>" + PublicRoomField.isVCCapable + "</isVCCapable>");
                                    publicFields.Append("<CurrencyType>" + PublicRoomField.CurrencyType + "</CurrencyType>");
                                    publicFields.Append("<IsEarlyHoursEnabled>" + PublicRoomField.IsEarlyHoursEnabled + "</IsEarlyHoursEnabled>");
                                    publicFields.Append("<EarlyHoursStart>" + PublicRoomField.EHStartTime + "</EarlyHoursStart>");
                                    publicFields.Append("<EarlyHoursEnd>" + PublicRoomField.EHEndTime + "</EarlyHoursEnd>");
                                    publicFields.Append("<EarlyHoursCost>" + PublicRoomField.EHCost + "</EarlyHoursCost>");
                                    publicFields.Append("<IsEarlyHoursFullyAuto>" + PublicRoomField.AHFullyAuto + "</IsEarlyHoursFullyAuto>"); //FB 2543
                                    publicFields.Append("<OpenHour>" + PublicRoomField.openHours + "</OpenHour>");
                                    publicFields.Append("<OfficeHoursStart>" + PublicRoomField.OHStartTime + "</OfficeHoursStart>");
                                    publicFields.Append("<OfficeHoursEnd>" + PublicRoomField.OHEndTime + "</OfficeHoursEnd>");
                                    publicFields.Append("<OfficeHoursCost>" + PublicRoomField.OHCost + "</OfficeHoursCost>");
                                    publicFields.Append("<IsAfterHourEnabled>" + PublicRoomField.IsAfterHourEnabled + "</IsAfterHourEnabled>");
                                    publicFields.Append("<AfterHoursStart>" + PublicRoomField.AHStartTime + "</AfterHoursStart>");
                                    publicFields.Append("<AfterHoursEnd>" + PublicRoomField.AHEndTime + "</AfterHoursEnd>");
                                    publicFields.Append("<AfterHoursCost>" + PublicRoomField.AHCost + "</AfterHoursCost>");
                                    publicFields.Append("<IsAfterHoursFullyAuto>" + PublicRoomField.AHFullyAuto + "</IsAfterHoursFullyAuto>"); //FB 2543
                                    publicFields.Append("<IsCrazyHoursSupported>" + PublicRoomField.isCrazyHoursSupported + "</IsCrazyHoursSupported>");
                                    publicFields.Append("<CrazyHoursStart>" + PublicRoomField.CHtartTime + "</CrazyHoursStart>");
                                    publicFields.Append("<CrazyHoursEnd>" + PublicRoomField.CHEndTime + "</CrazyHoursEnd>");
                                    publicFields.Append("<CrazyHoursCost>" + PublicRoomField.CHCost + "</CrazyHoursCost>");
                                    publicFields.Append("<IsCrazyHoursFullyAuto>" + PublicRoomField.CHFullyAuto + "</IsCrazyHoursFullyAuto>"); //FB 2543
                                    publicFields.Append("<Is24HoursEnabled>" + PublicRoomField.Is24HoursEnabled + "</Is24HoursEnabled>");
                                    publicFields.Append("<DefaultEquipment>" + PublicRoomField.DefaultEquipment + "</DefaultEquipment>");
                                    publicFields.Append("<PublicRoomLastModified>" + PublicRoomField.PublicRoomLastModified.ToString() + "</PublicRoomLastModified>");
                                }
                                strOutXML.Append(publicFields.ToString());
                                strOutXML.Append("</PublicFields>");
                            } //FB 2532 Ends
                            strOutXML.Append("</Room>");
                        }
                    }
                    if (pageNo > 0)
                    {
                        strOutXML.Append("<pageNo>" + pageNo.ToString() + "</pageNo>");
                        strOutXML.Append("<totalPages>" + ttlPages.ToString() + "</totalPages>");
                        strOutXML.Append("<searchBy>" + sortBy + "</searchBy>");
                        strOutXML.Append("<searchFor>" + alphabet + "</searchFor>");
                        //FB 2671 Starts
                        if (!String.IsNullOrEmpty(searchCountry))
                            strOutXML.AppendFormat("<searchCountry>{0}</searchCountry>", searchCountry);
                        if (!String.IsNullOrEmpty(searchState))
                            strOutXML.AppendFormat("<searchState>{0}</searchState>", searchState);
                        //FB 2671 Ends
                        strOutXML.Append("<totalNumber>" + numrecs + "</totalNumber>");
                    }
                }
                outxml.Append(strOutXML.ToString());

                outxml.Append("</GetConfAvailableRoom>");

                #endregion
               
            }
            catch (Exception ex)
            {
                m_log.Error("FetchAvailableRooms :" + ex.Message);
            }
        }

        #endregion

        //FB 2392 Ends

        //FB 2724 Start

        #region RoomValidation
        /// <summary>
        /// RoomValidation 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool RoomValidation(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0, LoginUserId = 0;
            StringBuilder tempXML = new StringBuilder();
            StringBuilder outXml = new StringBuilder();
            string roomUID = "", roomToken = "", weburl = "", companyLogo = "", Password = ""; //ZD 100196
            List<vrmRoom> locRoom = new List<vrmRoom>();
            OrgData orgInfo = null;
            sysMailData sysMailData = null;
            vrmBaseUser vrmUser = null;
            List<ICriterion> criterionList = new List<ICriterion>();
            cryptography.Crypto crypto = null;
            String mailExtnsn = "";//ZD 102618
            timeZoneData tz = new timeZoneData(); //ZD 103807
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);

                node = xd.SelectSingleNode("//login/LoginUserId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out LoginUserId);

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    throw myVRMEx;
                }

                if (userid == 0)
                    userid = LoginUserId;               
                
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                node = xd.SelectSingleNode("//login/roomUID"); //ZD 100196
                if (node != null)
                    roomUID = node.InnerText.Trim();

                node = xd.SelectSingleNode("//login/roomToken");
                if (node != null)
                    roomToken = node.InnerText.Trim();

                criterionList.Add(Expression.Eq("RoomUID", roomUID));//ZD 100196
                locRoom = m_IRoomDAO.GetByCriteria(criterionList, true);

                if (locRoom.Count > 0)
                {
                    locRoom[0].RoomToken = roomToken;
                    if (userid > 0)
                    {
                        locRoom[0].Lastmodifieddate = DateTime.Now;
                        locRoom[0].LastModifiedUser = userid;
                    }
                    m_IRoomDAO.clearFetch();
                    m_IRoomDAO.Update(locRoom[0]);

                    //FB 3061 Start
                    if (locRoom[0].tier2 != null)
                    {
                        if (locRoom[0].tier2.L3LocationId > 0)  //ZD 102481
                        {
                            outXml.Append("<Tier1>");
                            outXml.Append("<Tier1ID>" + locRoom[0].tier2.L3LocationId.ToString() + "</Tier1ID>");  //ZD 102481
                            outXml.Append("<Tier1Name>" + locRoom[0].tier2.TopTierName + "</Tier1Name>");  //ZD 102481
                            outXml.Append("</Tier1>");
                        }
                        else
                        {
                            outXml.Append("<Tier1>");
                            outXml.Append("<Tier1ID></Tier1ID>");
                            outXml.Append("<Tier1Name></Tier1Name>");
                            outXml.Append("</Tier1>");
                        }
                        outXml.Append("<Tier2>");
                        outXml.Append("<Tier2ID>" + locRoom[0].tier2.ID.ToString() + "</Tier2ID>");
                        outXml.Append("<Tier2Name>" + locRoom[0].tier2.Name + "</Tier2Name>");
                        outXml.Append("</Tier2>");
                    }
                    else
                    {
                        outXml.Append("<Tier1>");
                        outXml.Append("<Tier1ID></Tier1ID>");
                        outXml.Append("<Tier1Name></Tier1Name>");
                        outXml.Append("</Tier1>");
                        outXml.Append("<Tier2>");
                        outXml.Append("<Tier2ID></Tier2ID>");
                        outXml.Append("<Tier2Name></Tier2Name>");
                        outXml.Append("</Tier2>");
                    }
                    //FB 3061 End

                    outXml.Append("<roomID>" + locRoom[0].RoomID.ToString() + "</roomID>");
                    outXml.Append("<roomName>" + locRoom[0].Name + "</roomName>");
                    vrmUser = m_vrmUserDAO.GetByUserId(locRoom[0].assistant);

                    if (vrmUser != null)
                    {
                        outXml.Append("<login>" + vrmUser.Email + "</login>");

                        crypto = new cryptography.Crypto();
                        Password = crypto.decrypt(vrmUser.Password);

                        outXml.Append("<password>" + Password + "</password>");
                        //ZD 103807 Starts
                        timeZone.GetTimeZone(vrmUser.TimeZone, ref tz);
                        outXml.Append("<timezone>");
                        outXml.Append("<timezoneID>" + tz.TimeZoneID.ToString() + "</timezoneID>");
                        outXml.Append("<timezoneName>" + tz.TimeZoneDiff.ToString() + " ");
                        outXml.Append(tz.TimeZone + "</timezoneName>");
                        outXml.Append("<StandardName>" + tz.StandardName + "</StandardName>");
                        outXml.Append("</timezone>");
                        outXml.Append("<dateFormat>" + vrmUser.DateFormat + "</dateFormat>");
                        //ZD 103807  Ends
                    }
                    else
                    {
                        outXml.Append("<login></login>");
                        outXml.Append("<password></password>");

                        //ZD 103807 Starts
                        timeZone.GetTimeZone(sysSettings.TimeZone, ref tz);
                        outXml.Append("<timezone>");
                        outXml.Append("<timezoneID>" + tz.TimeZoneID.ToString() + "</timezoneID>");
                        outXml.Append("<timezoneName>" + tz.TimeZoneDiff.ToString() + " ");
                        outXml.Append(tz.TimeZone + "</timezoneName>");
                        outXml.Append("<StandardName>" + tz.StandardName + "</StandardName>");
                        outXml.Append("</timezone>");
                        outXml.Append("<dateFormat>MM/dd/yyyy</dateFormat>");
                        //ZD 103807  Ends
                    }
                    outXml.Append("<timeout>" + orgInfo.iControlTimeout + "</timeout>");

                    //ZD 101019 START
                    outXml.Append("<VideoStreamingDetails>");
                    outXml.Append("<VideoSourceURL1>" + orgInfo.VideoSourceURL1 + "</VideoSourceURL1>");
                    outXml.Append("<ScreenPosition1>" + orgInfo.ScreenPosition1 + "</ScreenPosition1>");
                    outXml.Append("<VideoSourceURL2>" + orgInfo.VideoSourceURL2+ "</VideoSourceURL2>");
                    outXml.Append("<ScreenPosition2>" + orgInfo.ScreenPosition2 + "</ScreenPosition2>");
                    outXml.Append("<VideoSourceURL3>" + orgInfo.VideoSourceURL3 + "</VideoSourceURL3>");
                    outXml.Append("<ScreenPosition3>" + orgInfo.ScreenPosition3 + "</ScreenPosition3>");
                    outXml.Append("<VideoSourceURL4>" + orgInfo.VideoSourceURL4 + "</VideoSourceURL4>");
                    outXml.Append("<ScreenPosition4>" + orgInfo.ScreenPosition4 + "</ScreenPosition4>");
                    outXml.Append("</VideoStreamingDetails>");
                    //ZD 101019 END

                    sysMailData = new sysMailData();
                    sysMailData = m_ISysMailDAO.GetById(1);
                    //ZD 102618 Start
                    if (sysMailData.CompanyMail.IndexOf("@") > 0)
                        if (sysMailData.CompanyMail.Split('@').Length > 0)
                            mailExtnsn = sysMailData.CompanyMail.Split('@')[1];

                    outXml.Append("<CompanyMailExtension>" + mailExtnsn + "</CompanyMailExtension>");

                    //ZD 102618 end //ZD 104387 - Start
                    OrgData orgdata = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    //weburl = sysMailData.websiteURL;                    
                    //companyLogo = weburl + "/en/Organizations/Org_" + orgInfo.OrgId.ToString().Trim() + "/CSS/Mirror/Image/lobbytop1600.jpg";
                    outXml.Append("<companyLogo>" + vrmFact.GetImage(orgdata.LobytopImageId) + "</companyLogo>");
                    outXml.Append("<companyBackground>" + vrmFact.GetImage(orgdata.LogoImageId) + "</companyBackground>");
                    //ZD 104387 - End
                    outXml.Append("<VideoRefreshTimer>" + orgInfo.VideoRefreshTimer + "</VideoRefreshTimer>");//ZD 103398
                }
                else
                    outXml.Append("<Error>No Room Found</Error>");

                obj.outXml = outXml.ToString();
            }
            catch (myVRMException ex)
            {
                m_log.Error("vrmException", ex);
                obj.outXml = ex.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetRoomConferenceMonthlyView
        /// <summary>
        /// GetRoomConferenceMonthlyView
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomConferenceMonthlyView(ref vrmDataObject obj)
        {
            StringBuilder outXML = new StringBuilder();
            try
            {
                DateTime calDate = DateTime.Now;
                XmlDocument xd = new XmlDocument();

                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//calendarView/date");
                if (node != null)
                    DateTime.TryParse(node.InnerText.Trim(), out calDate);

                DateTime fromDate = new DateTime(calDate.Year, calDate.Month, 1, 0, 0, 0);
                DateTime NextMonth = calDate.AddMonths(1); 
                DateTime toDate = new DateTime(calDate.Year, calDate.Month, calDate.AddMonths(1).AddDays(-NextMonth.Day).Day, 23, 59, 59);
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                //ZD 100151
                using (xWriter = XmlWriter.Create(outXML, xSettings))
                {
                    xWriter.WriteStartElement("monthlyView");
                    GetCalendarView(obj, fromDate, toDate, 0, ref xWriter);
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.Flush();
                    obj.outXml = outXML.ToString();
                }
                //ZD 100151
                              
                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetRoomConferenceMonthlyView :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("GetRoomConferenceMonthlyView :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetModifiedConference
        /// <summary>
        /// GetModifiedConference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetModifiedConference(ref vrmDataObject obj)
        {
            DateTime calDate = DateTime.Now;
            XmlDocument xd = new XmlDocument();
            int userID = 11, utcEnabled = 0, LoginUserId = 0;
            //int RoomID = 0,  Cfrist = 1;
            List<ICriterion> CritList = new List<ICriterion>();
            List<vrmConference> confs = new List<vrmConference>();
            timeZoneData tzData = new timeZoneData();
            StringBuilder outXML = new StringBuilder();
            //String temp = "", roomURL = "";
            //List<vrmConfRoom> confRooms = null;
            //ICriterion criterium = null;
            //vrmRoom Loc = null;
            vrmConference conf = null;
            try
            {
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//calendarView/date");
                if (node != null)
                    DateTime.TryParse(node.InnerText.Trim(), out calDate);

                node = xd.SelectSingleNode("//calendarView/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    throw myVRMEx;
                }

                node = xd.SelectSingleNode("//calendarView/utcEnabled");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out utcEnabled);

                node = xd.SelectSingleNode("//calendarView/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userID);

                node = xd.SelectSingleNode("//calendarView/LoginUserId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out LoginUserId);

                if (userID == 0)
                    userID = LoginUserId;


                //node = xd.SelectSingleNode("//calendarView/roomURL");
                //if (node != null)
                //    roomURL = node.InnerText.Trim();

                //if (roomURL != "")
                //{
                //    Loc = m_vrmRoomDAO.GetByRoomURL(roomURL);
                //    if (Loc != null)
                //        RoomID = Loc.RoomID;
                //    else
                //    {
                //        obj.outXml = "No Room Found";
                //        return false;
                //    }
                //}
             

                vrmBaseUser vrmUser = m_IUserDAO.GetByUserId(userID);

                timeZone.changeToGMTTime(vrmUser.TimeZone, ref calDate);

                CritList.Add(Expression.Ge("settingtime", calDate));
                CritList.Add(Expression.Eq("orgId", organizationID));
                confs.AddRange(m_IconfDAO.GetByCriteria(CritList));
                //criterium = (Expression.Ge("settingtime", calDate));
                //criterium = Expression.And(Expression.Eq("orgId", organizationID), criterium);
        
                vrmUser user = m_IUserDAO.GetByUserId(userID);

                //if (RoomID != 0)
                //{
                //    CritList.Add(Expression.Eq("roomId", RoomID));
                //    m_IconfRoom.addOrderBy(Order.Asc("StartDate"));

                //    confRooms = m_IconfRoom.GetByCriteria(CritList);
                //    for (int i = 0; i < confRooms.Count; i++)
                //    {
                //        CritList = new List<ICriterion>();
                //        CritList.Add(criterium);
                //        CritList.Add(Expression.Eq("confid", confRooms[i].confid));
                //        CritList.Add(Expression.Eq("instanceid", confRooms[i].instanceid));
                //        confs.AddRange(m_IconfDAO.GetByCriteria(CritList));
                //    }
                //}
       
                timeZone.GetTimeZone(user.TimeZone, ref tzData);

                if (confs.Count <= 0)
                    outXML.Append("<conferences></conferences>");
                else
                {
                    outXML.Append("<Conferences>");
                    for (int i = 0; i < confs.Count; i++)
                    {
                        conf = confs[i];
                        DateTime confDateTime = conf.confdate;
                        DateTime todayNow = DateTime.Now; 

                        if (utcEnabled == 0)
                            timeZone.userPreferedTime(user.TimeZone, ref confDateTime);

                        vrmOrganization vrmOrg = m_IOrgDAO.GetById(conf.orgId);
                        if (vrmOrg == null)
                        {
                            obj.outXml = "<error>Invalid Organization ID</error>";
                            return false;
                        }
                        if (orgInfo == null)
                            orgInfo = m_IOrgSettingsDAO.GetByOrgId(conf.orgId);
                        
                            outXML.Append("<Conference>");
                            outXML.Append("<organizationName>" + vrmOrg.orgname.ToString() + "</organizationName>");
                            outXML.Append("<ConferenceID>" + conf.confid.ToString() + "," +
                                    conf.instanceid.ToString() + "</ConferenceID>");
                            outXML.Append("<ConferenceUniqueID>" + conf.confnumname.ToString() + "</ConferenceUniqueID>");
                            outXML.Append("<ConferenceName>" + conf.externalname.ToString() + "</ConferenceName>");
                            outXML.Append("<ConferenceType>" + conf.conftype.ToString() + "</ConferenceType>");
                            outXML.Append("<ConferenceDateTime>" + confDateTime.ToString("g") + "</ConferenceDateTime>");
                            outXML.Append("<ConferenceDuration>" + conf.duration.ToString() + "</ConferenceDuration>");
                            outXML.Append("<isVMR>" + conf.isVMR + "</isVMR>");
                            outXML.Append("<VMRType>" + orgInfo.EnableVMR.ToString() + "</VMRType>");
                            outXML.Append("<PublicVMRCount>" + orgInfo.MaxPublicVMRParty.ToString() + "</PublicVMRCount>");
                            outXML.Append("<StartMode>" + conf.StartMode.ToString() + "</StartMode>");
                            outXML.Append("<ConferenceStatus>" + conf.status.ToString() + "</ConferenceStatus>");
                            outXML.Append("<OpenForRegistration>" + conf.dynamicinvite.ToString() + "</OpenForRegistration>");
                        
                            /* *** Buffer Zone Fixes - start *** */

                            DateTime setupTime;
                            DateTime tearDownTime;

                            if (conf.SetupTime <= DateTime.MinValue)
                                setupTime = conf.confdate;
                            else
                                setupTime = conf.SetupTime;

                            if (conf.TearDownTime <= DateTime.MinValue)
                                tearDownTime = conf.confdate.AddMinutes(conf.duration);
                            else
                                tearDownTime = conf.TearDownTime;

                            if (utcEnabled == 0)//FB 2014
                            {
                                timeZone.userPreferedTime(user.TimeZone, ref setupTime);
                                timeZone.userPreferedTime(user.TimeZone, ref tearDownTime);
                            }

                            outXML.Append("<SetupTime>" + setupTime.ToString("g") + "</SetupTime>");
                            outXML.Append("<TearDownTime>" + tearDownTime.ToString("g") + "</TearDownTime>");

                            /* *** Buffer Zone Fixes - end *** */

                            vrmUser host = m_IUserDAO.GetByUserId(conf.owner);

                            if (host != null) // Diagnostics
                            {
                                outXML.Append("<ConferenceHost>" + host.FirstName + " " + host.LastName + "</ConferenceHost>");
                                outXML.Append("<ConferenceHostEmail>" + host.Email + "</ConferenceHostEmail>"); //FB 2617 (user email)
                            }

                            // FB 2694
                            vrmUser requestor = m_IUserDAO.GetByUserId(conf.userid);
                            if (requestor != null)
                            {
                                outXML.Append("<ConferenceRequestor>" + requestor.FirstName + " " + requestor.LastName + "</ConferenceRequestor>");
                                outXML.Append("<ConferenceRequestorEmail>" + requestor.Email + "</ConferenceRequestorEmail>");
                            }

                            sysMailData sysMailData = new sysMailData();
                            sysMailData = m_ISysMailDAO.GetById(1);
                            outXML.Append("<ConfDescription>" + m_utilFactory.ReplaceOutXMLSpecialCharacters(conf.description) + "</ConfDescription>"); //FB 2236

                            outXML.Append("<IsRecur>" + conf.recuring.ToString() + "</IsRecur>");

                            outXML.Append("<Location>");
                            String locList = "";

                            for (int r = 0; r < conf.ConfRoom.Count; r++)
                            {
                                vrmConfRoom room = conf.ConfRoom[r];
                                if (room.disabled == 1) //ZD 104243
                                    continue;
                                outXML.Append("<Selected>");
                                outXML.Append("<ID>" + room.roomId.ToString() + "</ID>");
                                outXML.Append("<Name>" + room.Room.Name + "</Name>");
                                outXML.Append("<UID>" + room.Room.RoomUID + "</UID>"); //ZD 100196
                                outXML.Append("</Selected>");
                                if (locList == "")
                                    locList = room.Room.tier2.TopTierName + ">" + room.Room.tier2.Name + ">" + room.Room.Name;  //ZD 102481
                                else
                                    locList = locList + "~ " + room.Room.tier2.TopTierName + ">" + room.Room.tier2.Name + ">" + room.Room.Name;  //ZD 102481
                            }
                            outXML.Append("</Location>");
                         
                            if (locList != "")
                                outXML.Append("<LocationList>" + locList.ToString() + " </LocationList>");
                            else
                                outXML.Append("<LocationList>No Rooms</LocationList>");

                            if (conf.LastRunDateTime == DateTime.MinValue)
                                outXML.Append("<LastRunDate></LastRunDate>");
                            else
                                outXML.Append("<LastRunDate>" + conf.LastRunDateTime.ToString() + "</LastRunDate>");
                            outXML.Append("</Conference>");
                    }
                    outXML.Append("</Conferences>");
                    obj.outXml = outXML.ToString();
                }

                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetModifiedRoomConferenceMonthlyView :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("GetModifiedRoomConferenceMonthlyView :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        //FB 2724 End
        //FB 2593 Starts
        public bool GetAllRoomsList(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                StringBuilder outXml = new StringBuilder();

                int userId = 0;
                node = xd.SelectSingleNode("//login/userID");
                int.TryParse(node.InnerXml.Trim(), out userId);

                vrmUser user = m_IUserDAO.GetByUserId(userId);

                if (user == null)
                    return false;

                outXml.Append("<GetAllRoomsList>");
                List<ICriterion> CriterionList = new List<ICriterion>();
                CriterionList.Add(Expression.Eq("Disabled", 0));
                CriterionList.Add(Expression.Eq("Extroom", 0));
                List<vrmRoom> selectedRooms = m_IRoomDAO.GetByCriteria(CriterionList);
                m_IRoomDAO.clearOrderBy();

                outXml.Append("<Rooms>");
                foreach (vrmRoom room in selectedRooms)
                {
                    outXml.Append("<Room>");
                    outXml.Append("<id>" + room.RoomID.ToString() + "</id>");
                    outXml.Append("<name>" + room.Name + "</name>");
                    outXml.Append("<orgId>" + room.orgId + "</orgId>");
                    outXml.Append("</Room>");
                }
                outXml.Append("</Rooms>");
                outXml.Append("</GetAllRoomsList>");

                obj.outXml = outXml.ToString();

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; 
                return false;
            }
        }
        //FB 2593 End

        //FB 2639 Start

        #region GetAvailableLocations

        public void GetAvailableLocations(int organizationID, int mode, int userid, bool bHasDepartment, ref int confType, int mediatype, int serviceType, string selectedloc, ref List<int> RoomList)
        {
            string stmt2 = "", serivetypein = "0";
            vrmUser usr = new vrmUser();
            int roomid = 0;
            try
            {
                if (mode == 1)
                {
                    usr = m_IUserDAO.GetByUserId(userid);

                    if (!usr.isSuperAdmin())
                    {
                        stmt2 = " select roomId from myVRM.DataLayer.vrmRoom where Disabled = 0 ";

                        if (mediatype >= 0)
                            stmt2 += " AND VideoAvailable='" + mediatype + "' ";

                        if (serviceType == 3)
                            serivetypein = "1,2,3";
                        else if (serviceType == 2)
                            serivetypein = "1,2";
                        else if (serviceType == 1)
                            serivetypein = "1";

                        if (serviceType > 0)
                        {
                            if (selectedloc.Trim() == "")
                                stmt2 += " AND ServiceType in (" + serivetypein + ") ";
                            else
                                stmt2 += " AND (ServiceType in (" + serivetypein + ") OR roomId IN  (" + selectedloc.Trim() + "))";
                        }

                        if (PublicRoom > 0)
                            stmt2 += " AND isPublic = 1";
                        else if (PublicRoom < 0)
                            stmt2 += " AND isPublic = 0";

                        if (bHasDepartment)
                        {
                            stmt2 += " and roomId in (select crd.roomId from myVRM.DataLayer.vrmLocDepartment  crd, myVRM.DataLayer.vrmUserDepartment UD";
                            stmt2 += " where crd.departmentId = UD.departmentId and UD.userId = " + userid + " )";
                        }
                        else
                        {
                            stmt2 += " and orgId='" + organizationID + "' and ";
                            stmt2 += " roomId not in (select roomId from myVRM.DataLayer.vrmLocDepartment) and roomId <> 11";
                        }
                        stmt2 += "	ORDER BY Name ";
                    }
                    else
                    {
                        stmt2 = " select roomId from myVRM.DataLayer.vrmRoom where Disabled = 0 ";

                        if (mediatype >= 0)
                            stmt2 += " AND VideoAvailable='" + mediatype + "' ";

                        if (serviceType == 3)
                            serivetypein = "1,2,3";
                        else if (serviceType == 2)
                            serivetypein = "1,2";
                        else if (serviceType == 1)
                            serivetypein = "1";

                        if (serviceType > 0)
                        {
                            if (selectedloc.Trim() == "")
                                stmt2 += " AND ServiceType in (" + serivetypein + ") ";
                            else
                                stmt2 += " AND (ServiceType in (" + serivetypein + ") OR roomId IN  (" + selectedloc.Trim() + "))";
                        }

                        if (PublicRoom > 0)
                            stmt2 += " AND isPublic = 1";
                        else if (PublicRoom < 0)
                            stmt2 += " AND isPublic = 0";

                        stmt2 += " ORDER BY Name";
                    }
                }
                else
                {
                    stmt2 = "select roomId from myVRM.DataLayer.vrmRoom where Disabled = 0 ";

                    if (mediatype >= 0)
                        stmt2 += " AND VideoAvailable='" + mediatype + "' ";

                    if (serviceType == 3)
                        serivetypein = "1,2,3";
                    else if (serviceType == 2)
                        serivetypein = "1,2";
                    else if (serviceType == 1)
                        serivetypein = "1";

                    if (serviceType > 0)
                    {
                        if (selectedloc.Trim() == "")
                            stmt2 += " AND ServiceType in (" + serivetypein + ") ";
                        else
                            stmt2 += " AND (ServiceType in (" + serivetypein + ") OR roomId IN  (" + selectedloc.Trim() + "))";
                    }

                    if (PublicRoom > 0)
                        stmt2 += " AND isPublic = 1";
                    else if (PublicRoom < 0)
                        stmt2 += " AND isPublic = 0";
                }

                IList rooms = m_IconfRoom.execQuery(stmt2);

                for (int r = 0; r < rooms.Count; r++)
                {
                    int.TryParse(rooms[r].ToString(), out roomid);
                    RoomList.Add(roomid);
                }
            }
            catch (Exception ex)
            {
                m_log.Error("GetAvailableLocations :" + ex.Message);
            }
        }

        #endregion

        //FB 2639 End

        //ZD 100196 Start

        #region ChkRoomAuthentication
        /// <summary>
        /// ChkRoomAuthentication 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool ChkRoomAuthentication(ref vrmDataObject obj)
        {
            bool bRet = true;
            string roomUID = "";
            List<vrmRoom> locRoom = new List<vrmRoom>();
            OrgData orgInfo = null;
            myVRMException myvrmEx;
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//myVRM/command/login/roomUID");
                if (node.InnerText != "")
                    roomUID = node.InnerXml.Trim();

                //ZD 103239 starts
                int clientId = 0;
                node = xd.SelectSingleNode("//myVRM/client");
                if (node.InnerText != "")
                    int.TryParse(node.InnerXml.Trim(), out clientId);
                //ZD 103239 End

                if (roomUID != "")
                {
                    criterionList.Add(Expression.Eq("RoomUID", roomUID));
                    locRoom = m_IRoomDAO.GetByCriteria(criterionList,true);
                    if (locRoom != null && locRoom.Count > 0)
                    {
                        //ZD 102826 Starts
                        if (locRoom[0].iControl == 0)
                        {
                            myvrmEx = new myVRMException(750);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        //ZD 102826 End

                        orgInfo = m_IOrgSettingsDAO.GetByOrgId(locRoom[0].orgId);
                        if (orgInfo == null)
                        {
                            myvrmEx = new myVRMException(207);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }

                        //ZD 103239 starts
                        int EnablePIMServiceType = orgInfo.EnablePIMServiceType;
                        if (clientId == (int)clientFrom.AppleDevices)
                            EnablePIMServiceType = 1;

                        obj.outXml = "<success><organizationID>" + locRoom[0].orgId + "</organizationID><PIMServiceType>" + EnablePIMServiceType + "</PIMServiceType></success>";
                        //ZD 103239 End

                        return true;
                    }
                    else
                    {
                        obj.outXml = "<error>No Room Found</error>";
                        return false;
                    }
                }
                else
                {
                    obj.outXml = "<error>No Room Found</error>";
                    return false;
                }

            }
            catch (myVRMException ex)
            {
                m_log.Error("vrmException", ex);
                obj.outXml = ex.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //ZD 100196 End


        //ZD 101443 Start

        #region FetchUserAssignedRooms
        /// <summary>
        /// FetchUserAssignedRooms
        /// </summary>
        /// <param name="organizationID"></param>
        /// <param name="userid"></param>
        /// <param name="RoomList"></param>
        public void FetchUserAssignedRooms(int organizationID, int userid, ref List<vrmRoom> RoomList)
        {
            List<ICriterion> Icrit = new List<ICriterion>();
            try
            {
                Icrit.Add(Expression.Not(Expression.Eq("RoomID", 11)));
                Icrit.Add(Expression.Eq("orgId", organizationID));
                RoomList = m_IRoomDAO.GetByCriteria(Icrit, true);
                RoomList = RoomList.Where(r => r.assistant == userid || r.locationApprover.Count(l => l.approverid == userid) > 0).ToList();
            }
            catch (Exception ex)
            {
                m_log.Error("FetchUserAssignedRooms :" + ex.Message);
            }
        }

        #endregion

        #region UpdateUserAssignedRooms
        /// <summary>
        /// UpdateUserAssignedRooms
        /// </summary>
        /// <param name="organizationID"></param>
        /// <param name="Extuserid"></param>
        /// <param name="SelectAll"></param>
        /// <param name="NewUserID"></param>
        /// <param name="SelectedID"></param>
        /// <returns></returns>
        public bool UpdateUserAssignedRooms(int organizationID, int Extuserid, int SelectAll, int NewUserID, XPathNavigator xNode, string inXml,ref int ErrorNo)
        {
            List<ICriterion> Icrit = new List<ICriterion>();
            List<vrmRoom> RoomList = null;
            List<vrmLocApprover> Applist = null;
            try
            {
                if (xNode != null)
                {
                    var idlist = (XDocument.Parse(inXml).Descendants("SelectedID").Elements("ID").Select(element => element.Value).ToList());

                    idlist.Remove("");

                    #region Assisant Update
                    Icrit.Add(Expression.Not(Expression.Eq("RoomID", 11)));
                    Icrit.Add(Expression.Eq("orgId", organizationID));
                    Icrit.Add(Expression.Eq("assistant", Extuserid));
                    Icrit.Add(Expression.In("RoomID", idlist));
                    RoomList = m_IRoomDAO.GetByCriteria(Icrit, true);
                    if (RoomList != null && RoomList.Count > 0)
                    {
                        RoomList = RoomList.Select((data, i) => { data.assistant = NewUserID; return data; }).ToList();
                        m_IRoomDAO.SaveOrUpdateList(RoomList);
                    }
                    #endregion

                    #region Approver Update

                    Icrit = new List<ICriterion>();
                    Icrit.Add(Expression.Not(Expression.Eq("roomid", 11)));
                    Icrit.Add(Expression.Eq("approverid", Extuserid));
                    Icrit.Add(Expression.In("roomid", idlist));
                    Applist = m_ILocApprovDAO.GetByCriteria(Icrit);
                    if (Applist != null && Applist.Count > 0)
                    {
                        if (!m_Search.CheckApproverRights(NewUserID.ToString()))
                        {
                            ErrorNo = 632;
                            return false;
                        }
                        Applist = Applist.Select((data, i) => { data.approverid = NewUserID; return data; }).ToList();
                        m_ILocApprovDAO.SaveOrUpdateList(Applist);
                    }
                    #endregion
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("UpdateUserAssignedRooms :" + ex.Message);
                return false;
            }
        }
        #endregion

        //ZD 101443 End


        //ZD 101736 Start

        #region GetRoomEWSDetails
        /// <summary>
        /// GetRoomEWSDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomEWSDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder OutXml = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetRoomEWSDetails/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                List<ICriterion> criterionLst = new List<ICriterion>();
                if (orgid != "")
                    criterionLst.Add(Expression.Eq("Disabled", 0));
                criterionLst.Add(Expression.Eq("orgId", organizationID));
                criterionLst.Add(Expression.Not(Expression.Eq("RoomQueue", "")));


                List<vrmRoom> rmList = m_IRoomDAO.GetByCriteria(criterionLst);

                OutXml.Append("<GetRoomEWSDetails>");
                for (int r = 0; r < rmList.Count; r++)
                {
                    OutXml.Append("<RoomDetails>");
                    OutXml.Append("<RoomId>" + rmList[r].roomId + "</RoomId>");
                    OutXml.Append("<RoomEmail>" + rmList[r].RoomQueue + "</RoomEmail>");
                    OutXml.Append("<RoomDomain>" + rmList[r].Domain + "</RoomDomain>");
                    OutXml.Append("<RoomEWSURL>" + rmList[r].ExchangeURL + "</RoomEWSURL>");
                    OutXml.Append("</RoomDetails>");
                }
                OutXml.Append("</GetRoomEWSDetails>");

                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region UpdateRoomEWSDetails
        /// <summary>
        /// UpdateRoomEWSDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool UpdateRoomEWSDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder OutXml = new StringBuilder();
            string URL = "";
            List<int> RoomIds = new List<int>();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<vrmRoom> locRoomList = new List<vrmRoom>();
            List<RoomEWS> REWS = new List<RoomEWS>();
            XDocument xdoc = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                
                //node = xd.SelectSingleNode("//UpdateRoomEWSDetails/organizationID");
                //string orgid = "";
                //if (node != null)
                //    orgid = node.InnerText.Trim();
                //organizationID = defaultOrgId;
                //Int32.TryParse(orgid, out organizationID);

                //if (orgInfo == null)
                //    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                //if (organizationID < defaultOrgId)
                //{
                //    myVRMEx = new myVRMException(423);
                //    obj.outXml = myVRMEx.FetchErrorMsg();
                //    return false;
                //}

                xdoc = XDocument.Parse(obj.inXml);
                REWS = (from temp in xdoc.Descendants("RoomDetails").OrderBy(x => x.Element("RoomEWSURL").Value)
                                   select new RoomEWS
                               {
                                   Roomid = Convert.ToInt32(temp.Element("RoomId").Value),
                                   RoomURL = ((string.IsNullOrEmpty(temp.Element("RoomEWSURL").Value) == true) ? "" : Convert.ToString(temp.Element("RoomEWSURL").Value)),
                               }).ToList();


                var queryLastNames = from room in REWS
                                     group room by room.RoomURL into newGroup
                                     orderby newGroup.Key
                                     select newGroup;

                foreach (var nameGroup in queryLastNames)
                {
                    URL = nameGroup.Key;
                    RoomIds = new List<int>();
                    foreach (var room in nameGroup)
                    {
                        RoomIds.Add(room.Roomid);
                    }
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.In("RoomID", RoomIds));
                    m_IRoomDAO.clearFetch();
                    locRoomList = m_IRoomDAO.GetByCriteria(criterionList, true);
                    locRoomList = locRoomList.Select((data, i) => { data.ExchangeURL = URL; return data; }).ToList();
                    m_IRoomDAO.SaveOrUpdateList(locRoomList);
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        public class RoomEWS
        {
            internal int Roomid { get; set; }
            internal string RoomURL { get; set; }
        }

        //ZD 101736 End
		//ZD 101527 Starts
        #region GetSyncRooms
        /// <summary>
        /// GetSyncRooms
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSyncRooms(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            List<vrmSyncLoc> loclist = new List<vrmSyncLoc>();
            vrmSyncLoc synRooms = null;
            StringBuilder OutXML = new StringBuilder();
            xSettings = null;
            obj.outXml = "";

            string EndpointID = "", EndpointName = "", Name = "",RoomID = "", OrgID = "";
            long ttlRecords = 0;
            int PageNo = 0, ttlPages = 0;
            int i = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);


                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//GetSyncRooms/organizationID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out organizationID);

                        if (organizationID < 11)
                        {
                            obj.outXml = "<error>Invalid Organization ID</error>";
                            return false;
                        }
                    }
                }

                m_ISyncLocDAO.clearFetch();
                loclist = m_ISyncLocDAO.GetByOrgId(organizationID);
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OutXML, xSettings))
                {
                    xWriter.WriteStartElement("GetSyncRooms");

                    xNode = xNavigator.SelectSingleNode("//GetSyncRooms/PageNo");
                    if (xNode != null)
                        Int32.TryParse(xNode.Value.Trim(), out PageNo);

                    if (loclist != null && loclist.Count > 0)
                    {
                        ttlRecords = loclist.Count;
                        loclist = loclist.Skip(m_iMaxRecords * (PageNo - 1)).Take(m_iMaxRecords).ToList();

                        ttlPages = (int)(ttlRecords / m_iMaxRecords);

                        if (ttlRecords % m_iMaxRecords > 0)
                            ttlPages++;

                        for (i = 0; i < loclist.Count; i++)
                        {
                            synRooms = new vrmSyncLoc();
                            synRooms = loclist[i];

                            EndpointID = Convert.ToString(synRooms.endpointid);
                            EndpointName = Convert.ToString(synRooms.endPointName);
                            Name = Convert.ToString(synRooms.Name);
                            OrgID = Convert.ToString(synRooms.orgId);
                            RoomID = Convert.ToString(synRooms.RoomID);

                            xWriter.WriteStartElement("SyncRoom");
                            xWriter.WriteElementString("EndpointID", EndpointID);
                            xWriter.WriteElementString("EndpointName", EndpointName);
                            xWriter.WriteElementString("RoomName", Name);
                            xWriter.WriteElementString("RoomID", RoomID);
                            xWriter.WriteElementString("OrgID", OrgID);
                            xWriter.WriteFullEndElement();
                        }
                    }
                    xWriter.WriteElementString("TotalPages", ttlPages.ToString());
                    xWriter.WriteElementString("PageNo", PageNo.ToString());

                    xWriter.WriteFullEndElement();
                }

                obj.outXml = OutXML.ToString();
            }
            catch (Exception ex)
            {
                m_log.Error("GetSyncRooms Failed: " + ex.Message);
                obj.outXml = "";
                return false;
            }

            return true;
        }
        #endregion

        #region SetSyncRooms
        public bool SetSyncRooms(ref vrmDataObject obj)
        {
            bool bRet = true;
            bool isNewEndpoint = false;
            int pId = 0, eId = 0, userID = 0;
            IList maxId;
            List<vrmSyncLoc> Rooms = new List<vrmSyncLoc>();
            List<ICriterion> selcnt = new List<ICriterion>();
            List<ICriterion> criterionLst;
            XmlElement itemElement;
            vrmSyncLoc SyncRoom = new vrmSyncLoc();
            List<ICriterion> criterionList = new List<ICriterion>();
            int SyncRoomID = 0, rmLimit = 0, errID = 0, roomID = 0;
            vrmRoom locRoom = null;
            List<ICriterion> criterionListept = new List<ICriterion>();
            List<vrmEndPoint> eptList = new List<vrmEndPoint>();
            List<ICriterion> tcriterionList = new List<ICriterion>();
            List<vrmTier2> t2RoomList = null;
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                XmlNodeList RoomIds;

                node = xd.SelectSingleNode("//SetSyncRooms/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = 11;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                node = xd.SelectSingleNode("//SetSyncRooms/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);

                if (userID < defaultOrgId)
                    userID = 11;

                RoomIds = xd.SelectNodes("//SetSyncRooms/RoomIDs/RoomID");

                if (RoomIds != null && RoomIds.Count > 0)
                {
                    orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    for (int rmtCnt = 0; rmtCnt < RoomIds.Count; rmtCnt++)
                    {
                        eptList = new List<vrmEndPoint>();
                        itemElement = (XmlElement)RoomIds[rmtCnt];

                        if (itemElement != null)
                            int.TryParse(itemElement.InnerXml.Trim(), out SyncRoomID);

                        criterionList = new List<ICriterion>();
                        SyncRoom = new vrmSyncLoc();
                        selcnt = new List<ICriterion>();

                        //Normal Video/Audio/None rooms
                        criterionLst = new List<ICriterion>();
                        criterionLst.Add(Expression.Eq("VideoAvailable", 2));
                        criterionLst.Add(Expression.Eq("RoomCategory", 1));
                        criterionLst.Add(Expression.Eq("isPublic", 0));
                        criterionLst.Add(Expression.Eq("Extroom", 0));
                        criterionLst.Add(Expression.Eq("Disabled", 0));
                        criterionLst.Add(Expression.Eq("orgId", organizationID));


                        rmLimit = orgdt.MaxVideoRooms;
                        errID = 455;
                        List<vrmRoom> checkRoomCount = m_IRoomDAO.GetByCriteria(criterionLst);

                        if (checkRoomCount.Count >= rmLimit)
                        {
                            myVRMException myvrmEx = new myVRMException(errID);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }

                        Rooms = new List<vrmSyncLoc>();

                        SyncRoom = m_ISyncLocDAO.GetBymyVRMRoomId(SyncRoomID);

                        //Check whether enpoint is imported or not

                        criterionListept = new List<ICriterion>();
                        criterionListept.Add(Expression.Eq("identifierValue", SyncRoom.identifierValue));
                        eptList = m_vrmEpt.GetByCriteria(criterionListept);

                        if (!(eptList != null && eptList.Count > 0))
                        {
                            myVRMException myvrmEx = new myVRMException(745);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }

                        locRoom = new vrmRoom();

                        locRoom.Name = SyncRoom.Name;
                        locRoom.orgId = SyncRoom.orgId;
                        locRoom.endpointid = SyncRoom.endpointid;
                        //Default Values
                        locRoom.RoomBuilding = "";
                        locRoom.RoomFloor = "";
                        locRoom.RoomNumber = "";
                        locRoom.RoomPhone = "";
                        locRoom.Capacity = 1;
                        locRoom.assistant = userID;
                        locRoom.AssistantPhone = "";
                        locRoom.ProjectorAvailable = 0;
                        locRoom.MaxPhoneCall = 0;
                        locRoom.VideoAvailable = 2;
                        locRoom.adminId = userID;
                        locRoom.AssistantPhone = "";
                        locRoom.DefaultEquipmentid = 0;
                        locRoom.Caterer = "";
                        locRoom.DynamicRoomLayout = "";
                        
                        tcriterionList = new List<ICriterion>();
                        tcriterionList.Add(Expression.Eq("ID", orgdt.OnflyMiddleTierID));
                        tcriterionList.Add(Expression.Eq("disabled", 0));
                        t2RoomList = m_IT2DAO.GetByCriteria(tcriterionList);
                        if (t2RoomList.Count > 0)
                        {
                            locRoom.tier2 = (vrmTier2)t2RoomList[0];
                            locRoom.MiddleTier = locRoom.tier2.Name;
                            locRoom.TopTier = locRoom.tier2.TopTierName;  //ZD 102481
                        }
                        locRoom.L3LocationId = orgdt.OnflyTopTierID;
                        locRoom.Disabled = 0;
                        locRoom.ResponseTime = 60;
                        locRoom.ResponseMessage = "";
                        locRoom.RoomImage = "";
                        locRoom.SetupTime = 0;
                        locRoom.TeardownTime = 0;
                        locRoom.AuxAttachments = "";
                        locRoom.CostCenterid = 0;
                        locRoom.notifyemails = "";
                        locRoom.Address1 = "";
                        locRoom.Address2 = "";
                        locRoom.City = "";
                        locRoom.State = 0;
                        locRoom.Zipcode = "";
                        locRoom.Country = 0;
                        locRoom.Maplink = "";
                        locRoom.ParkingDirections = "";
                        locRoom.AdditionalComments = "";
                        locRoom.TimezoneID = sysSettings.TimeZone;
                        locRoom.Longitude = "";
                        locRoom.Latitude = "";
                        locRoom.MapImage1 = "";
                        locRoom.MapImage2 = "";
                        locRoom.SecurityImage1 = "";
                        locRoom.SecurityImage2 = "";
                        locRoom.MiscImage1 = "";
                        locRoom.MiscImage2 = "";
                        locRoom.Custom1 = "";
                        locRoom.Custom2 = "";
                        locRoom.Custom3 = "";
                        locRoom.Custom4 = "";
                        locRoom.Custom5 = "";
                        locRoom.Custom6 = "";
                        locRoom.Custom7 = "";
                        locRoom.Custom8 = "";
                        locRoom.Custom9 = "";
                        locRoom.Custom10 = "";
                        locRoom.HandiCappedAccess = 0;
                        locRoom.MapImage1Id = ""; //ZD 103569
                        locRoom.MapImage2Id = "";//ZD 103569
                        locRoom.SecurityImage1Id = 0;
                        locRoom.SecurityImage2Id = 0;
                        locRoom.MiscImage1Id = "";//ZD 103569
                        locRoom.MiscImage2Id = ""; //ZD 103569
                        locRoom.RoomImageId = "";
                        locRoom.isVIP = 0;
                        locRoom.isTelepresence = 0;
                        locRoom.ServiceType = -1;
                        locRoom.DedicatedVideo = "";
                        locRoom.RoomQueue = "";
                        locRoom.DedicatedCodec = "";
                        locRoom.AVOnsiteSupportEmail = "";
                        locRoom.Extroom = 0;
                        locRoom.IsVMR = 0;
                        locRoom.InternalNumber = "";
                        locRoom.ExternalNumber = "";
                        locRoom.EntityID = 0;
                        locRoom.Type = "";
                        locRoom.OwnerID = 0;
                        locRoom.Extension = "";
                        locRoom.VidyoURL = "";
                        locRoom.Pin = "";
                        locRoom.isLocked = 0;
                        locRoom.allowCallDirect = 0;
                        locRoom.MemberID = 0;
                        locRoom.isPublic = 0;
                        locRoom.RoomCategory = 1;
                        locRoom.RoomToken = "";
                        locRoom.LastModifiedUser = userID;
                        locRoom.Lastmodifieddate = DateTime.UtcNow;
                        locRoom.VMRLink = "";
                        locRoom.RoomIconTypeId = vrmAttributeType.Video; //ZD 103569
                        locRoom.RoomUID = "";
                        locRoom.Password = "";
                        locRoom.GuestContactName = "";
                        locRoom.GuestContactEmail = "";
                        locRoom.GuestContactPhone = "";
                        locRoom.PermanentConfId = "";
                        locRoom.VMRConfId = "";
                        locRoom.BridgeId = 0;
                        locRoom.IsCreateOnMCU = 0;
                        locRoom.IsDialOutLoc = 0;
                        locRoom.DialOutLocationIds = "";
                        locRoom.PermanentconfName = "";
                        locRoom.ConfTemplateId = 0;
                        locRoom.iControl = 0;
                        locRoom.TopTier = orgdt.TopTier;
                        locRoom.MiddleTier = orgdt.MiddleTier;
                        locRoom.Secure = 0;
                        locRoom.UsersSecurityemail = "";
                        locRoom.Securityemail = "";
                        locRoom.CreateType = 2;

                        m_IRoomDAO.Save(locRoom);
                        roomID = getMaxRoomID();
                        m_Search.SetAuditRoom(true, locRoom.roomId, locRoom.Lastmodifieddate, locRoom.LastModifiedUser);
                        m_ISyncLocDAO.Delete(SyncRoom);
                    }
                }
                //obj.outXml = "<SetEndpoint><EndpointID>" + ((vrmEndPoint)endPoints[0]).endpointid.ToString() +
                //   "</EndpointID></SetEndpoint>";

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;

        }
        #region getMaxRoomID
        /// <summary>
        /// getMaxRoomID
        /// </summary>
        /// <returns></returns>
        private int getMaxRoomID()
        {
            try
            {
                string qString = "select max(LA.roomId) from myVRM.DataLayer.vrmRoom LA";

                IList list = m_IRoomDAO.execQuery(qString);

                string sMax;
                if (list[0] != null)
                    sMax = list[0].ToString();
                else
                    sMax = "0";

                int id = Int32.Parse(sMax);
                return id;
            }
            catch (Exception e)
            {
                m_log.Error("getMaxRoomID", e);
                throw e;
            }
        }
        #endregion
        #endregion
        //ZD 101527 Ends
		//ZD 102358
        #region GetTopTiers
        /// <summary>
        /// GetTopTiers
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetTopTiers(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder OutXML = new StringBuilder();
            xSettings = null;
            obj.outXml = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                
                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//GetTopTiers/organizationID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out organizationID);

                        if (organizationID < 11)
                        {
                            obj.outXml = "<error>Invalid Organization ID</error>";
                            return false;
                        }
                    }
                }

                String stmt;
                stmt = "select distinct l3.ID, l3.Name from myVRM.DataLayer.vrmTier3 l3 ";
                stmt += " where l3.disabled = 0 and l3.orgId = '" + organizationID.ToString() + "'";
                stmt += " order by l3.Name ";

                IList tier3 = m_IT3DAO.execQuery(stmt);
                if (tier3.Count > 0)
                {
                    xSettings = new XmlWriterSettings();
                    xSettings.OmitXmlDeclaration = true;
                    using (xWriter = XmlWriter.Create(OutXML, xSettings))
                    {
                        xWriter.WriteStartElement("GetTopTiers");
                        foreach (object[] t3 in tier3)
                        {
                            xWriter.WriteStartElement("TopTier");
                            xWriter.WriteElementString("ID", t3[0].ToString());
                            xWriter.WriteElementString("Name", t3[1].ToString());
                            xWriter.WriteFullEndElement();
                        }
                        xWriter.WriteFullEndElement();
                    }
                }

                obj.outXml = OutXML.ToString();
            }
            catch (Exception ex)
            {
                m_log.Error("GetTopTiers Failed: " + ex.Message);
                obj.outXml = "";
                return false;
            }

            return true;
        }
        #endregion

        #region GetMiddleTiersByTopTier
        /// <summary>
        /// GetMiddleTiersByTopTier
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetMiddleTiersByTopTier(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder OutXML = new StringBuilder();
            xSettings = null;
            obj.outXml = "";
            Int32 topTierID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//GetMiddleTiers/organizationID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out organizationID);

                        if (organizationID < 11)
                        {
                            obj.outXml = "<error>Invalid Organization ID</error>";
                            return false;
                        }

                        xNode = xNavigator.SelectSingleNode("//GetMiddleTiers/TopTierID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out topTierID);
                    }
                }
                
                String stmt;
                stmt = "select distinct l3.ID, l3.Name from myVRM.DataLayer.vrmTier2 l3 ";
                stmt += " where l3.disabled = 0 and l3.orgId = '" + organizationID.ToString() + "' and l3.L3LocationId  ='" + topTierID + "'";
                stmt += " order by l3.Name ";

                IList tier2 = m_IT2DAO.execQuery(stmt);
                if (tier2.Count > 0)
                {
                    xSettings = new XmlWriterSettings();
                    xSettings.OmitXmlDeclaration = true;
                    using (xWriter = XmlWriter.Create(OutXML, xSettings))
                    {
                        xWriter.WriteStartElement("GetMiddleTiers");
                        foreach (object[] t2 in tier2)
                        {
                            xWriter.WriteStartElement("MiddleTier");
                            xWriter.WriteElementString("ID", t2[0].ToString());
                            xWriter.WriteElementString("Name", t2[1].ToString());
                            xWriter.WriteFullEndElement();
                        }
                        xWriter.WriteFullEndElement();
                    }
                }

                obj.outXml = OutXML.ToString();
            }
            catch (Exception ex)
            {
                m_log.Error("GetMiddleTiersByTopTier Failed: " + ex.Message);
                obj.outXml = "";
                return false;
            }

            return true;
        }
        #endregion

        #region GetRoomsByTiers
        /// <summary>
        /// GetRoomsByTiers
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomsByTiers(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder OutXML = new StringBuilder();
            xSettings = null;
            obj.outXml = "";
            Int32 userID = 11, topTierID = 0, middleTierID = 0; // ZD 103303
            int deptBased = -1; // ZD 103303
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//GetRoomsByTiers/userID"); // ZD 103303
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out userID);

                        xNode = xNavigator.SelectSingleNode("//GetRoomsByTiers/organizationID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out organizationID);

                        if (organizationID < 11)
                        {
                            obj.outXml = "<error>Invalid Organization ID</error>";
                            return false;
                        }

                        xNode = xNavigator.SelectSingleNode("//GetRoomsByTiers/TopTierID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out topTierID);

                        xNode = xNavigator.SelectSingleNode("//GetRoomsByTiers/MiddleTierID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out middleTierID);
                    }
                }

                // ZD 103303 Start
                string stmt = "select roleName from myVRM.DataLayer.vrmUserRoles ";
                stmt += " where (level = 2 or crossaccess = 1) ";
                stmt += " and roleID = (select roleID from myVRM.DataLayer.vrmUser where userid = " + userID + ") ";

                IList adminRights = m_IT2DAO.execQuery(stmt);

                if (adminRights != null && adminRights.Count > 0)
                {
                    deptBased = -1; // Admin
                }
                else
                {
                    stmt = "select departmentId from myVRM.DataLayer.vrmUserDepartment where userId = " + userID;
                    IList hasDept = m_IT2DAO.execQuery(stmt);
                    if (hasDept != null && hasDept.Count > 0)
                        deptBased = 1; // Dept
                    else
                        deptBased = 0; // Non-Dept
                }

                if (deptBased == -1) // Admin
                {
                    stmt = "select distinct R.RoomID, R.Name from myVRM.DataLayer.vrmRoom R ";
                    stmt += " where R.roomId <> 11 and R.Disabled = 0 and Extroom=0 and R.orgId = '" + organizationID + "' and R.tier2  = '" + middleTierID + "' and R.L3LocationId  ='" + topTierID + "'";
                    stmt += " order by R.Name ";

                }
                else if (deptBased == 1) // Dept
                {
                    stmt = "select distinct R.RoomID, R.Name from myVRM.DataLayer.vrmRoom R, myVRM.DataLayer.vrmLocDepartment rd, myVRM.DataLayer.vrmUserDepartment ud ";
                    stmt += " where R.roomId <> 11 and R.Disabled = 0 and Extroom=0 and R.orgId = '" + organizationID + "' and R.tier2  = '" + middleTierID + "' and R.L3LocationId  ='" + topTierID + "'";
                    stmt += " and R.RoomID = rd.roomId and ud.userId = " + userID + " and rd.departmentId in ";
                    stmt += " (select departmentId from myVRM.DataLayer.vrmUserDepartment where userId = " + userID + ") ";
                    stmt += " order by R.Name ";
                }
                else if(deptBased == 0) // Non-Dept
                {
                    stmt = "select distinct R.RoomID, R.Name from myVRM.DataLayer.vrmRoom R ";
                    stmt += " where R.roomId <> 11 and R.Disabled = 0 and Extroom=0 and R.orgId = '" + organizationID + "' and R.tier2  = '" + middleTierID + "' and R.L3LocationId  ='" + topTierID + "'";
                    stmt += " and R.roomId not in (select roomId from myVRM.DataLayer.vrmLocDepartment) ";
                    stmt += " order by R.Name ";
                }
                // ZD 103303 End

                IList tier2 = m_IT2DAO.execQuery(stmt);
                if (tier2 != null && tier2.Count > 0)
                {
                    xSettings = new XmlWriterSettings();
                    xSettings.OmitXmlDeclaration = true;
                    using (xWriter = XmlWriter.Create(OutXML, xSettings))
                    {
                        xWriter.WriteStartElement("GetRoomsByTiers");
                        foreach (object[] t2 in tier2)
                        {
                            xWriter.WriteStartElement("Room");
                            xWriter.WriteElementString("ID", t2[0].ToString());
                            xWriter.WriteElementString("Name", t2[1].ToString());
                            xWriter.WriteFullEndElement();
                        }
                        xWriter.WriteFullEndElement();
                    }
                }

                obj.outXml = OutXML.ToString();
            }
            catch (Exception ex)
            {
                m_log.Error("GetRoomsByTiers Failed: " + ex.Message);
                obj.outXml = "";
                return false;
            }

            return true;
        }
        #endregion

        //ZD 102123 Starts
        #region GetFloorPlans
        public bool GetFloorPlans(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            List<vrmFloorPlan> FloorPlanlist = new List<vrmFloorPlan>();
            vrmFloorPlan floorPlan = null;
            StringBuilder OutXML = new StringBuilder();
            List<string> BusyRoomID = new List<string>();
            xSettings = null;
            obj.outXml = "";

            string PlanID = "", PlanName = "", PlanImage = "", RoomID = "", OrgID = "", IdentifiedLoc = "";
            long ttlRecords = 0;
            int PageNo = 0, ttlPages = 0;
            int i = 0, isEnableAVItem = 0;
            string Tier1ID = "", Tier2ID = "";
            string Tier1Name = "", Tier2Name = "", RoomName = "", Handicappedaccess = "", PhotosOnly = "", MediaNone = "", MediaAudio = "", MediaVideo = "", mediaQry = "", ConfType= "";
            string hdnZipCode = "", ZipCode = "", Country = "", State = "", State2 = "", State3 = "", stateQuery = "", ListAVItem = "", avItemQuery = "", AVItemNone = "";
            List<int> locationIds = new List<int>();
            List<int> NonelocationIds = new List<int>();
            string qString = "";
            string stDate = "", confDuration = "";
            int isAdmin = 0, userID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);


                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//GetFloorPlans/organizationID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out organizationID);

                        if (organizationID < 11)
                        {
                            obj.outXml = "<error>Invalid Organization ID</error>";
                            return false;
                        }

                        xNode = xNavigator.SelectSingleNode("//GetFloorPlans/UserID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out userID);

                        xNode = xNavigator.SelectSingleNode("//GetFloorPlans/isAdmin");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out isAdmin);
                    }
                }

                m_IFloorRoomDAO.clearFetch();
                //FloorPlanlist = m_IFloorRoomDAO.GetByOrgId(organizationID);
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OutXML, xSettings))
                {
                    xWriter.WriteStartElement("GetFloorPlans");

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/PageNo");
                    if (xNode != null)
                        Int32.TryParse(xNode.Value.Trim(), out PageNo);

                   

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/StartDate");
                    if (xNode != null)
                        stDate = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/ConfDuration");
                    if (xNode != null)
                        confDuration = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/TimeZone");



                    string tzone = "26";
                    if (xNode != null)
                    {
                        tzone = xNode.InnerXml.Trim();
                    }

                    int tzoneID = 33;

                    Int32.TryParse(tzone, out tzoneID);

                    DateTime confDate = DateTime.Now;
                    DateTime.TryParse(stDate, out confDate);
                    timeZone.changeToGMTTime(tzoneID, ref confDate);



                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/Tier1Name");
                    if (xNode != null)
                        Tier1Name = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/Tier2Name");
                    if (xNode != null)
                        Tier2Name = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/RoomName");
                    if (xNode != null)
                        RoomName = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/Handicappedaccess");
                    if (xNode != null)
                        Handicappedaccess = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/PhotosOnly");
                    if (xNode != null)
                        PhotosOnly = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/ConfType");
                    if (xNode != null)
                        ConfType = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/MediaNone");
                    if (xNode != null)
                        MediaNone = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/MediaAudio");
                    if (xNode != null)
                        MediaAudio = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/MediaVideo");
                    if (xNode != null)
                        MediaVideo = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/hdnZipCode");
                    if (xNode != null)
                        hdnZipCode = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/ZipCode");
                    if (xNode != null)
                        ZipCode = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/Country");
                    if (xNode != null)
                        Country = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/State");
                    if (xNode != null)
                        State = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/State2");
                    if (xNode != null)
                        State2 = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/State3");
                    if (xNode != null)
                        State3 = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/isEnableAVItem");
                    if (xNode != null)
                        int.TryParse(xNode.Value, out isEnableAVItem);

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/ListAVItem");
                    if (xNode != null)
                        ListAVItem = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/AVItemNone");
                    if (xNode != null)
                        AVItemNone = xNode.Value;

                    xNode = xNavigator.SelectSingleNode("//GetFloorPlans/SearchCriteria/avItemQuery");
                    if (xNode != null)
                        avItemQuery = xNode.Value;
                                  

                    string qry = "";                    
                    string hql2;
                    DataSet ds2 = null;

                    if (m_roomlayer == null)
                        m_roomlayer = new ns_SqlHelper.SqlHelper(m_configPath);

                    hql2 = "select roomid from loc_room_D where roomcategory = 4";
                        
                    if(RoomName != "")
                        hql2 += " and Name like '%" + RoomName + "%'";

                    if (Handicappedaccess == "1")
                        hql2 += " and Handicappedaccess = " + Handicappedaccess;

                    if (PhotosOnly == "1")
                        hql2 += " and RoomImage != '' ";
                        

                    if (ConfType != "" && ConfType != "8")
                    {
                        mediaQry = " VideoAvailable = 2 ";
                    }
                    else
                    {
                        if (MediaNone == "1")
                            mediaQry += " VideoAvailable = 0 ";

                        mediaQry = ((mediaQry == "" || MediaAudio != "1") ? mediaQry : mediaQry += " or ");

                        if (MediaAudio == "1")
                            mediaQry += " VideoAvailable = 1 ";

                        mediaQry = ((mediaQry == "" || MediaVideo != "1") ? mediaQry : mediaQry += " or ");

                        if (MediaVideo == "1")
                            mediaQry += " VideoAvailable = 2 ";

                    }
                    if (mediaQry != "")
                        mediaQry = "( " + mediaQry + " )";

                    if (mediaQry != "")
                        hql2 = ((hql2 == "") ? mediaQry : hql2 + " and " + mediaQry);

                    if (hdnZipCode == "1")
                    {
                        hql2 += " and ZipCode Like  '%" + ZipCode + "%' ";

                    }
                    else
                    {

                        if (Country != "-1" && Country != "")
                            hql2 += " and Country = " + Country;

                        if (State != "-1" && State != "")
                            stateQuery += " State = " + State;

                        stateQuery = ((stateQuery == "" || State2 == "-1") ? stateQuery : stateQuery += " or ");

                        if (State2 != "-1" && State2 != "")
                            stateQuery += " State = " + State2;

                        stateQuery = ((stateQuery == "" || State3 == "-1") ? stateQuery : stateQuery += " or ");

                        if (State3 != "-1" && State3 != "")
                            stateQuery += " State = " + State3;

                        stateQuery = ((stateQuery == "") ? stateQuery : "( " + stateQuery + " )");

                        hql2 = ((stateQuery == "") ? hql2 : hql2 + " and " + stateQuery);
                    }

                        

                    if (hql2 != "")
                        ds2 = m_roomlayer.ExecuteDataSet(hql2);

                        
                    DataSet ds = null;

                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                            

                        string hql;

                        if (m_roomlayer == null)
                            m_roomlayer = new ns_SqlHelper.SqlHelper(m_configPath);

                        hql = "select * from loc_floorplan_d where orgid = " + organizationID + " ";


                        if (Tier1Name != "")
                            hql += " and (L3Locationid  in (select id from Loc_Tier3_D where Name Like '%" + Tier1Name + "%'))";

                        if (Tier2Name != "")
                            hql += " and (L2Locationid  in (select id from Loc_Tier2_D where Name Like '%" + Tier2Name + "%'))";

                            

                        string[] filt = new string[ds2.Tables[0].Rows.Count];
                        for (int j = 0; j < ds2.Tables[0].Rows.Count; j++)
                        {
                            filt[j] = (ds2.Tables[0].Rows[j]["roomid"]).ToString();
                        }


                        if (hql != "")
                            ds = m_roomlayer.ExecuteDataSet(hql);


                        if (ds != null)
                        {
                            m_IFloorRoomDAO.clearFetch();

                            int rowCnt = ds.Tables[0].Rows.Count;
                            string[] arrRoomIds;
                            for (int k = 0; k < rowCnt; k++)
                            {
                                arrRoomIds = (ds.Tables[0].Rows[k]["roomids"]).ToString().Split(',');
                                var intersec = arrRoomIds.Intersect(filt);
                                if (intersec.Count() < 1)
                                {
                                    ds.Tables[0].Rows[k].Delete();
                                    ds.AcceptChanges();
                                    k--;
                                    rowCnt--;
                                }
                            }




                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                FloorPlanlist = (from DataRow row in ds.Tables[0].Rows
                                                    select new vrmFloorPlan
                                                    {
                                                        Floorplanid = int.Parse(row["Floorplanid"].ToString()),
                                                        FloorPlaneName = row["FloorPlaneName"].ToString(),
                                                        loginUserid = int.Parse(row["loginUserid"].ToString()),
                                                        orgId = int.Parse(row["orgId"].ToString()),
                                                        FloorImage = (byte[])row["FloorImage"],
                                                        identifiedLocations = row["identifiedLocations"].ToString(),
                                                        roomids = row["roomids"].ToString(),
                                                        createddate = Convert.ToDateTime(row["createddate"].ToString()),
                                                        modifieddate = Convert.ToDateTime(row["modifieddate"].ToString()),
                                                        L2LocationId = int.Parse(row["L2LocationId"].ToString()),
                                                        L3LocationId = int.Parse(row["L3LocationId"].ToString()),
                                                        TopTier = row["L2LocationId"].ToString(),
                                                        MiddleTier = row["L3LocationId"].ToString()

                                                    }).ToList();
                            }
                        }
                    }

                    #region AVItems Search

                    if (isEnableAVItem == 1)
                    {
                        if (AVItemNone == "1" && ListAVItem != "" && avItemQuery == "and")
                            locationIds = new List<int>();
                        else
                        {
                            if (m_roomlayer == null)
                                m_roomlayer = new ns_SqlHelper.SqlHelper(m_configPath);

                            m_roomlayer.OpenConnection();

                            if (ListAVItem != "")
                            {
                                qString = " select distinct locationID from Inv_Room_D where categoryID in (select ID from Inv_Category_D where ID in ";
                                qString += "(select categoryID from Inv_ItemList_AV_D where " + ListAVItem + ") and Type = 1 and deleted = 0)";

                                System.Data.DataSet ds3 = m_roomlayer.ExecuteDataSet(qString);
                                if (ds3 != null)
                                {
                                    if (ds3.Tables.Count > 0)
                                    {
                                        if (ds3.Tables[0].Rows.Count > 0)
                                        {
                                            for (int j = 0; j < ds3.Tables[0].Rows.Count; j++)
                                            {
                                                locationIds = (from r in ds3.Tables[0].AsEnumerable()
                                                                select r.Field<int>("locationID")).Distinct().ToList();
                                            }
                                        }
                                    }
                                }
                            }

                            if (AVItemNone == "1")
                            {
                                qString = " select Roomid from Loc_room_D where Roomid not in ( select distinct locationID from Inv_Room_D where categoryID in (select ID from Inv_Category_D where Type = 1 and deleted = 0)) and roomid in ( " + hql2 + " )";

                                System.Data.DataSet ds1 = m_roomlayer.ExecuteDataSet(qString);
                                if (ds1 != null)
                                {
                                    if (ds1.Tables.Count > 0)
                                    {
                                        if (ds1.Tables[0].Rows.Count > 0)
                                        {

                                            NonelocationIds = (from r in ds1.Tables[0].AsEnumerable()
                                                                select r.Field<int>("Roomid")).Distinct().ToList();
                                        }
                                    }
                                }
                            }

                            locationIds = locationIds.Union(NonelocationIds).Distinct().ToList();

                            string[] filter = new string[locationIds.Count];
                            for (int j = 0; j < locationIds.Count; j++)
                            {
                                filter[j] = locationIds[j].ToString();
                            }


                            string[] arrRoomId;
                            int cnt = FloorPlanlist.Count;
                            for (int j = 0; j < cnt; j++)
                            {
                                arrRoomId = FloorPlanlist[j].roomids.ToString().Split(',');
                                var inter = arrRoomId.Intersect(filter);
                                if (inter.Count() < 1)
                                {
                                    FloorPlanlist.RemoveAt(j);
                                    j--;
                                    cnt--;
                                }
                            }
                        }
                            
                    }
                    #endregion

                    string sqlStat = "";
                    if (isAdmin == 1)
                        sqlStat = "select RoomID, * from Loc_Room_D where roomcategory = 4 and Disabled = 0 and orgId = " + organizationID + "";
                    else
                    {
                        sqlStat = "select departmentId from Usr_Dept_D where userid = " + userID.ToString();
                        DataSet ds5 = m_roomlayer.ExecuteDataSet(sqlStat);

                        if (ds5.Tables[0].Rows.Count > 0)
                            sqlStat = "select distinct lr.roomid from Usr_Dept_D ud, Loc_Department_D ld, loc_room_d lr where lr.roomcategory = 4 and ud.departmentId = ld.DepartmentId and lr.RoomID = ld.RoomId and ud.userid = " + userID.ToString();
                        else
                            sqlStat = "select RoomID from loc_room_d where RoomCategory=4 and [Disabled]=0";
                    }
                    DataSet ds4 = m_roomlayer.ExecuteDataSet(sqlStat);

                    List<string> lstRoomAccess = null;
                    if (ds4.Tables[0].Rows.Count > 0)
                    {
                        string[] roomAccess = new string[ds4.Tables[0].Rows.Count];
                        for (int k = 0; k < ds4.Tables[0].Rows.Count; k++)
                        {
                            roomAccess[k] = (ds4.Tables[0].Rows[k]["roomid"]).ToString();
                        }
                        lstRoomAccess = roomAccess.ToList();

                        string[] arrRooms;
                        int len = FloorPlanlist.Count;
                        for (int j = 0; j < len; j++)
                        {
                            arrRooms = FloorPlanlist[j].roomids.ToString().Split(',');
                            var inter = arrRooms.Intersect(roomAccess);
                            if (inter.Count() < 1)
                            {
                                FloorPlanlist.RemoveAt(j);
                                j--;
                                len--;
                            }
                        }
                    }
                    else
                        FloorPlanlist.Clear();

                    if (FloorPlanlist != null && FloorPlanlist.Count > 0)
                    {

                        for (int j = 0; j < FloorPlanlist.Count; j++)
                        {
                            vrmTier3 tier1obj = null;
                            tier1obj = m_IT3DAO.GetById(FloorPlanlist[j].L3LocationId);
                            FloorPlanlist[j].TopTier = tier1obj.Name;

                            vrmTier2 tier2obj = null;
                            tier2obj = m_IT2DAO.GetById(FloorPlanlist[j].L2LocationId);
                            FloorPlanlist[j].MiddleTier = tier2obj.Name;

                        }

                        List<vrmFloorPlan> SortedList = FloorPlanlist.OrderBy(o => o.TopTier).ToList();
                        FloorPlanlist.Clear();
                        FloorPlanlist = new List<vrmFloorPlan> (SortedList);

                        ttlRecords = FloorPlanlist.Count;
                        FloorPlanlist = FloorPlanlist.Skip(m_iMaxRecords * (PageNo - 1)).Take(m_iMaxRecords).ToList();

                        ttlPages = (int)(ttlRecords / m_iMaxRecords);

                        if (ttlRecords % m_iMaxRecords > 0)
                            ttlPages++;


                        CheckHotdeskingAvailableRooms(ref BusyRoomID, confDate.ToString(), confDuration, organizationID);

                        for (i = 0; i < FloorPlanlist.Count; i++)
                        {
                            floorPlan = new vrmFloorPlan();
                            floorPlan = FloorPlanlist[i];

                            PlanID = Convert.ToString(floorPlan.Floorplanid);
                            PlanName = Convert.ToString(floorPlan.FloorPlaneName);
                            PlanImage = Convert.ToString(vrmImg.ConvertByteArrToBase64(floorPlan.FloorImage));
                            OrgID = Convert.ToString(floorPlan.orgId);
                            RoomID = Convert.ToString(floorPlan.roomids);
                            IdentifiedLoc = Convert.ToString(floorPlan.identifiedLocations);
                            Tier1ID = Convert.ToString(floorPlan.L3LocationId);
                            Tier2ID = Convert.ToString(floorPlan.L2LocationId);
                            
							string tier1 = "", tier2 = "";
                            tier1 = floorPlan.TopTier;
                            tier2 = floorPlan.MiddleTier;

                            xWriter.WriteStartElement("FloorPlan");
                            xWriter.WriteElementString("FloorPlanID", PlanID);
                            xWriter.WriteElementString("FloorPlanName", PlanName);
                            xWriter.WriteElementString("FloorPlanImage", PlanImage);
                            xWriter.WriteElementString("IdentifiedLocations", IdentifiedLoc);
                            xWriter.WriteElementString("PlanRoomIDs", RoomID);
                            
                            List<string> result = RoomID.Split(',').ToList();                            

                            string BusyRmID = ""; 
                            for (int j = 0; j < result.Count; j++)
                            {
                                vrmRoom Loc = m_vrmRoomDAO.GetByRoomId(Convert.ToInt32(result[j]));
                                if ((lstRoomAccess != null) && (!lstRoomAccess.Contains(result[j])))
                                {
                                    if (BusyRmID == "")
                                        BusyRmID = result[j] + "~~" + Loc.Name + "~~" + -1;
                                    else
                                        BusyRmID += "||" + result[j] + "~~" + Loc.Name + "~~" + -1;

                                }
                                else if (BusyRoomID.Contains(result[j]))
                                {
                                    if (BusyRmID == "")
                                        BusyRmID = result[j] + "~~" + Loc.Name + "~~" + 0;
                                    else
                                        BusyRmID += "||" + result[j] + "~~" + Loc.Name + "~~" + 0;
                                }
                                else
                                {
                                    if (BusyRmID == "")
                                        BusyRmID = result[j] + "~~" + Loc.Name + "~~" + 1;
                                    else
                                        BusyRmID += "||" + result[j] + "~~" + Loc.Name + "~~" + 1;
                                }
                            }
                            xWriter.WriteElementString("PlanRoomIDStatus", BusyRmID);


                            xWriter.WriteElementString("Tier1ID", Tier1ID);
                            xWriter.WriteElementString("Tier2ID", Tier2ID);
                            xWriter.WriteElementString("Tier1Name", tier1);
                            xWriter.WriteElementString("Tier2Name", tier2);

                            xWriter.WriteFullEndElement();
                        }
                    }
                    xWriter.WriteElementString("TotalPages", ttlPages.ToString());
                    xWriter.WriteElementString("PageNo", PageNo.ToString());
                    xWriter.WriteElementString("TotalRecords", ttlRecords.ToString());

                    xWriter.WriteFullEndElement();
                }

                obj.outXml = OutXML.ToString();
            }
            catch (Exception ex)
            {
                m_log.Error("GetFloorPlans Failed: " + ex.Message);
                obj.outXml = "";
                return false;
            }

            return true;
        }
        #endregion

        #region SetFloorPlan
        public bool SetFloorPlan(ref vrmDataObject obj)
        {
            bool bRet = true;
            int FloorPlanID = 0, userID = 0;
            string PlaneName = "", IDentifiedLoc = "", RoomIDS = "", RoomID = "", PImage = "", orgid = "";
            int Tier1ID = 0, Tier2ID = 0;
            byte[] PlanImage = null;

            vrmFloorPlan FloorPlan = new vrmFloorPlan();

            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//SetFloorPlan/organizationID");
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = 11;

                int.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                node = xd.SelectSingleNode("//SetFloorPlan/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);

                if (userID < defaultOrgId)
                    userID = 11;

                
                node = xd.SelectSingleNode("//SetFloorPlan/FloorPlan/FloorPlanID");
                if (node != null)
                     RoomID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetFloorPlan/FloorPlan/FloorPlanName");
                if (node != null)
                    PlaneName = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetFloorPlan/FloorPlan/FloorPlanImage");
                if (node != null)
                    PImage = node.InnerXml.Trim();
                PlanImage = vrmImg.ConvertBase64ToByteArray(PImage);

                node = xd.SelectSingleNode("//SetFloorPlan/FloorPlan/IdentifiedLocations");
                if (node != null)
                    IDentifiedLoc = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetFloorPlan/FloorPlan/PlanRoomIDs");
                if (node != null)
                    RoomIDS = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetFloorPlan/FloorPlan/Tier1ID");
                if (node != null)
                    Tier1ID = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//SetFloorPlan/FloorPlan/Tier2ID");
                if (node != null)
                    Tier2ID = Int32.Parse(node.InnerXml.Trim());


                FloorPlan = new vrmFloorPlan();
                
                
                bool isNewRoom = false;
                if (RoomID.ToLower().Contains("new"))
                {
                    isNewRoom = true;
                    FloorPlan.createddate = DateTime.UtcNow;
                    FloorPlan.Floorplanid = getMaxPalnID() + 1;
                }
                if (!isNewRoom)
                {
                    int.TryParse(RoomID, out FloorPlanID);
                    FloorPlan = m_IFloorRoomDAO.GetByFloorRoomId(FloorPlanID);
                }
                FloorPlan.modifieddate = DateTime.UtcNow;
                FloorPlan.FloorPlaneName = PlaneName;
                FloorPlan.loginUserid = userID;
                FloorPlan.orgId = organizationID;
                FloorPlan.FloorImage = PlanImage;
                FloorPlan.identifiedLocations = IDentifiedLoc;
                FloorPlan.roomids = RoomIDS;
                FloorPlan.L3LocationId = Tier1ID;
                FloorPlan.L2LocationId = Tier2ID;

                if (isNewRoom)
                    m_IFloorRoomDAO.Save(FloorPlan);
                else
                    m_IFloorRoomDAO.SaveOrUpdate(FloorPlan);
                

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region getMaxRoomID
        /// <summary>
        /// getMaxRoomID
        /// </summary>
        /// <returns></returns>
        private int getMaxPalnID()
        {
            try
            {
                string qString = "select max(FP.Floorplanid) from myVRM.DataLayer.vrmFloorPlan FP";

                IList list = m_IFloorRoomDAO.execQuery(qString);

                string sMax;
                if (list[0] != null)
                    sMax = list[0].ToString();
                else
                    sMax = "0";

                int id = 0;
                int.TryParse(sMax, out id);
                return id;
            }
            catch (Exception e)
            {
                m_log.Error("getMaxRoomID", e);
                throw e;
            }
        }
        #endregion

        #region DeleteFloorPlan
        public bool DeleteFloorPlan(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            List<vrmFloorPlan> FloorPlanlist = new List<vrmFloorPlan>();
            StringBuilder OutXML = new StringBuilder();
            xSettings = null;
            obj.outXml = "";

            int i = 0, userid = 11, floorPlanID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//DeleteFloorPlan/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//DeleteFloorPlan/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);
                //organizationID = defaultOrgId;//ZD 104452

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//DeleteFloorPlan/FloorPlanID");
                int.TryParse(node.InnerText.Trim(), out floorPlanID);

                criterionList.Add(Expression.Eq("Floorplanid", floorPlanID));
                criterionList.Add(Expression.Eq("orgId", organizationID));
                List<vrmFloorPlan> checkRoom = m_IFloorRoomDAO.GetByCriteria(criterionList, true);
                if (checkRoom.Count > 0)
                {
                    m_IFloorRoomDAO.Delete(checkRoom[0]);
                }

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("DeleteFloorPlan Failed: " + ex.Message);
                obj.outXml = "";
                return false;
            }

            return true;
        }
        #endregion

        #region CheckHotdeskingAvailableRooms
        /// <summary>
        /// CheckHotdeskingAvailableRooms
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="orgId"></param>
        /// <param name="confsResultList"></param>
        private void CheckHotdeskingAvailableRooms(ref List<string> BusyRoomID, string confdate, string confDuration, int organizationID)
        {
            try
            {
                StringBuilder stmt = new StringBuilder();

                stmt.Append(" select cf.roomId, cf.confid from myVRM.DataLayer.vrmConfRoom cf, myVRM.DataLayer.vrmConference c, myVRM.DataLayer.vrmRoom l ");
                stmt.Append(" where (('" + confdate + "' >= cf.StartDate and '" + confdate + "' < dateadd(minute, cf.Duration,cf.StartDate)) ");
                stmt.Append(" or ( cf.StartDate >= '" + confdate + "' and cf.StartDate < dateadd(minute, " + confDuration + " , '" + confdate + "'))) ");
                stmt.Append(" and c.confid = cf.confid and c.instanceid=cf.instanceid ");
                stmt.Append(" and c.orgId =" + organizationID + " and c.deleted = 0 and l.roomId = cf.roomId ");

                //stmt.Append(" select cf.roomId, cf.confid from myVRM.DataLayer.vrmConfRoom cf, myVRM.DataLayer.vrmConference c, myVRM.DataLayer.vrmRoom l ");
                //stmt.Append(" where ((getutcdate() >= cf.StartDate and getutcdate() < dateadd(minute, cf.Duration,cf.StartDate)) ");
                //stmt.Append(" or ( cf.StartDate >= getutcdate() and cf.StartDate < dateadd(minute, 60 , getutcdate()))) ");
                //stmt.Append(" and c.confid = cf.confid and c.instanceid=cf.instanceid ");
                //stmt.Append(" and c.orgId =11 and c.deleted = 0 and l.roomId = cf.roomId ");

                IList IRoomList = m_IconfDAO.execQuery(stmt.ToString());

                foreach (object[] tdObj in IRoomList)
                {
                    if (!BusyRoomID.Contains(tdObj[0]))
                        BusyRoomID.Add(tdObj[0].ToString());
                }

            }
            catch (myVRMException e)
            {
                m_log.Error("CheckHotdeskingAvailableRooms vrmException: ", e);
            }
            catch (Exception e)
            {
                m_log.Error("CheckHotdeskingAvailableRooms sytemException: ", e);
            }

        }
        #endregion
        //ZD 102123 Ends

        //ZD 103460 Start
        #region GetIcontrolconference
        /// <summary>
        /// GetIcontrolconference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetIcontrolconference(ref vrmDataObject obj)
        {
            StringBuilder outXML = new StringBuilder();
            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            DateTime calDate = DateTime.Now;
            DateTime setUpTime = DateTime.Now;
            DateTime tearDownTime = DateTime.Now;
            DateTime confdate = DateTime.Now;
            DateTime gmtConfDate = DateTime.Now;
            DateTime MCUPreStart = DateTime.Now;
            DateTime MCUPreEnd = DateTime.Now;
            DateTime endDatetime = DateTime.Now;
            XmlDocument xd = new XmlDocument();
            int RoomID = 0;
            List<ICriterion> CritList = new List<ICriterion>();
            List<vrmConference> confs = new List<vrmConference>();
            List<vrmConference> RoomConfList = new List<vrmConference>();
            String ConfType = "", Hostname = "", RoomUID = "", temp = "";
            vrmRoom Loc = null;
            List<int> confStatus = new List<int>();
            String todayDate = ""; //ALLBUGS-86
            DateTime toDateTime = DateTime.Now; //ALLBUGS-86
            try
            {
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetIcontrolconference/RoomUID");
                if (node != null)
                    RoomUID = node.InnerText.Trim();

                if (RoomUID != "")
                {
                    Loc = m_vrmRoomDAO.GetByRoomUID(RoomUID);
                    if (Loc != null)
                        RoomID = Loc.RoomID;
                    else
                    {
                        obj.outXml = "No Room Found";
                        return false;
                    }
                }

                //ALLBUGS-86 Starts
                node = xd.SelectSingleNode("//GetIcontrolconference/Date");
                if (node != null)
                    todayDate = node.InnerText.Trim();
                toDateTime = Convert.ToDateTime(todayDate);
                //ALLBUGS-86 End

                toDate = toDateTime.AddDays(1); // Return next 1 Days conference from Today in UTC format
                fromDate = toDateTime.AddDays(-1); // Return previous day conference
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref fromDate);
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref toDate);
                fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
                toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);

                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(outXML, xSettings))
                {
                    confStatus.Add(0); // Future
                    confStatus.Add(5); // Ongoing
                    confStatus.Add(6); // On MCU

                    CritList = new List<ICriterion>();

                    CritList.Add(Expression.Ge("MCUPreStart", fromDate));
                    CritList.Add(Expression.Le("TearDownTimeDateTime", toDate));
                    CritList.Add(Expression.Eq("deleted", 0));
                    CritList.Add(Expression.In("status", confStatus));
                    CritList.Add(Expression.Eq("Permanent", 0));
                    m_IconfDAO.addOrderBy(Order.Asc("confdate"));
                    confs = m_IconfDAO.GetByCriteria(CritList);
                    RoomConfList = confs.Where(s => s.ConfRoom.Count(r => r.roomId == RoomID) > 0).ToList();


                    xWriter.WriteStartElement("GetIcontrolconference");
                    for (int i = 0; i < RoomConfList.Count; ++i)
                    {
                        vrmConference vrmconff = RoomConfList[i];
                        double duration = 0.0;

                        confdate = vrmconff.confdate;
                        setUpTime = vrmconff.SetupTime;
                        tearDownTime = vrmconff.TearDownTime;
                        gmtConfDate = vrmconff.confdate;
                        MCUPreStart = vrmconff.MCUPreStart;
                        MCUPreEnd = vrmconff.MCUPreEnd;

                        xWriter.WriteStartElement("conferences"); //ALLBUGS-86
                        xWriter.WriteStartElement("conference");
                        xWriter.WriteElementString("date", confdate.ToString("MM/dd/yyyy")); //ALLBUGS-86
                        xWriter.WriteElementString("confID", vrmconff.confid + "," + vrmconff.instanceid);
                        xWriter.WriteElementString("uniqueID", vrmconff.confnumname.ToString());
                        xWriter.WriteElementString("confName", vrmconff.externalname);
                        xWriter.WriteElementString("ConferenceType", vrmconff.conftype.ToString());

                        ConfType = "";
                        switch (vrmconff.conftype)
                        {
                            case 7:
                                ConfType = "Room Conference";
                                break;
                            case 6:
                                ConfType = "Audio Only Conference";
                                break;
                            case 2:
                                ConfType = "Audio/Video Conference";
                                break;
                            case 4:
                                ConfType = "Point-To-Point Conference";
                                break;
                            case 8:
                                ConfType = "Hotdesking Conference";
                                break;
                        }

                        xWriter.WriteElementString("ConferenceTypeName", ConfType);
                        xWriter.WriteElementString("deleted", vrmconff.deleted.ToString());
                        xWriter.WriteElementString("isVIP", vrmconff.isVIP.ToString());
                        xWriter.WriteElementString("isVMR", vrmconff.isVMR.ToString());
                        xWriter.WriteElementString("confPassword", vrmconff.password);

                        if ((vrmconff.duration >= 1440 || vrmconff.confEnd.Day > confdate.Day) && vrmconff.confEnd <= toDate && (toDate - fromDate).TotalMinutes < 1440)
                            Double.TryParse((vrmconff.confEnd - fromDate).TotalMinutes.ToString(), out duration);
                        else
                            Double.TryParse(vrmconff.duration.ToString(), out duration);

                        duration = Math.Round(duration, 0);

                        xWriter.WriteElementString("confDate", confdate.ToString("MM/dd/yyyy"));
                        temp = confdate.ToString("hh:mm tt");
                        if (confdate < fromDate)
                            temp = "00:00 AM";

                        xWriter.WriteElementString("confTime", temp);

                        temp = setUpTime.ToString("hh:mm tt");
                        if (setUpTime < fromDate)
                            temp = "00:00 AM";

                        xWriter.WriteElementString("setupTime", temp);

                        temp = tearDownTime.ToString("hh:mm tt");
                        if (tearDownTime < fromDate)
                            temp = "00:00 AM";

                        xWriter.WriteElementString("teardownTime", temp);
                        xWriter.WriteElementString("setupDur", vrmconff.SetupTimeinMin.ToString());
                        xWriter.WriteElementString("teardownDur", vrmconff.TearDownTimeinMin.ToString());
                        xWriter.WriteElementString("MCUPreStartDur", vrmconff.McuSetupTime.ToString());
                        xWriter.WriteElementString("MCUPreEndDur", vrmconff.MCUTeardonwnTime.ToString());
                        xWriter.WriteElementString("durationMin", duration.ToString());

                        endDatetime = confdate.AddMinutes(duration);

                        xWriter.WriteElementString("endDateTime", endDatetime.ToString("MM/dd/yyyy hh:mm tt"));
                        xWriter.WriteElementString("owner", vrmconff.owner.ToString());
                        xWriter.WriteElementString("ConferenceStatus", vrmconff.status.ToString());
                        Hostname = "";
                        if (vrmconff.HostName == "")
                        {
                            vrmUser owner = m_IUserDAO.GetByUserId(vrmconff.owner);
                            Hostname = owner.FirstName + " " + owner.LastName;
                        }
                        else
                        {
                            Hostname = vrmconff.HostName;
                        }
                        xWriter.WriteElementString("Hostname", Hostname);
                        xWriter.WriteStartElement("mainLocation");
                        for (int j = 0; j < vrmconff.ConfRoom.Count; j++)
                        {
                            xWriter.WriteStartElement("location");
                            xWriter.WriteElementString("locationID", vrmconff.ConfRoom[j].roomId.ToString());
                            xWriter.WriteElementString("locationName", vrmconff.ConfRoom[j].Room.Name);
                            xWriter.WriteFullEndElement();
                        }
                        xWriter.WriteFullEndElement();
                        xWriter.WriteFullEndElement();
                        xWriter.WriteFullEndElement(); //ALLBUGS-86
                    }
                    xWriter.WriteFullEndElement();
                    xWriter.Flush();
                    obj.outXml = outXML.ToString();
                }

                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetIcontrolconference :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("GetIcontrolconference :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        //ZD 103460 End

    }
}
